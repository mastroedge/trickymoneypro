<?php
if (! defined('BASEPATH'))
    exit('No direct script access allowed');

if (! function_exists('timezone_list')) {

    function timezone_list()
    {
        $zones_array = array();
        $timestamp = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            $zones_array[$key]['ZONE'] = $zone;
            $zones_array[$key]['GMT'] = 'UTC/GMT ' . date('P', $timestamp);
        }
        return $zones_array;
    }
}

if (! function_exists('get_current_setting')) {

    function get_current_setting($setting)
    {
        $CI = &get_instance();
        // echo '<pre>';print_r($CI->session);echo '</pre>';
        switch ($setting) {
            case 'currency_code_session':
                return SHOW_CURRENCY ? $CI->session->userdata('settings')['currency_code'] : '';
                break;
            default:
                $CI->load->database();
                $CI->db->select('value');
                $CI->db->from('settings');
                $CI->db->where('settings', $setting);
                $query = $CI->db->get();
                $result = $query->row();
                return $result->value;
                break;
        }
    }
}

if (! function_exists('decimalPlace')) {

    function decimalPlace($number, $decimal = '.')
    {
        if (empty($number)) {
            $number = 0;
        }
        $broken_number = explode($decimal, $number);
        if (array_key_exists("1", $broken_number)) {
            $decimalPart = round($broken_number[1] / pow(10, strlen($broken_number[1]) - 2), 2); // Rounds to two decimal places
            $decimalPart = str_pad($decimalPart, 2, '0', STR_PAD_RIGHT); // Ensures two digits
            return numberFormat($broken_number[0]) . $decimal . '<small>' . $decimalPart . '</small>';
        } else {
            return numberFormat($broken_number[0]) . $decimal . '<small>00</small>';
        }
        // return number_format((float) $number, 2);
    }
}

if (! function_exists('numberFormat')) {
    function numberFormat($number, $decimals = 0)
    {

        // $number = 555;
        // $decimals=0;
        // $number = 555.000;
        // $number = 555.123456;

        if (strpos($number, '.') != null) {
            $decimalNumbers = substr($number, strpos($number, '.'));
            $decimalNumbers = substr($decimalNumbers, 1, $decimals);
        } else {
            $decimalNumbers = 0;
            for ($i = 2; $i <= $decimals; $i++) {
                $decimalNumbers = $decimalNumbers . '0';
            }
        }
        // return $decimalNumbers;

        $number = (int) $number;
        $negative = false;
        if ($number < 0) {
            $negative = true;
            $number = str_replace("-", "", $number);
        }
        // reverse
        $number = strrev($number);

        $n = '';
        $stringlength = strlen($number);

        for ($i = 0; $i < $stringlength; $i++) {
            if ($i % 2 == 0 && $i != $stringlength - 1 && $i > 1) {
                $n = $n . $number[$i] . ',';
            } else {
                $n = $n . $number[$i];
            }
        }

        $number = $n;
        // reverse
        $number = strrev($number);
        if ($negative) {
            $number = "-" . $number;
        }

        ($decimals != 0) ? $number = $number . '.<small>' . $decimalNumbers . '</small>' : $number;

        return $number;
    }
}

if (! function_exists('getDaysfromDate')) {

    function getDaysfromDate($idate)
    {
        if ($idate && $idate > '1988-05-01') {
            $now = time();
            $your_date = strtotime($idate);
            $datediff = $now - $your_date;

            return "<small> (" . round($datediff / (60 * 60 * 24)) . " days)</small>";
        }
    }
}

if (! function_exists('getOld')) {

    function getOld($id, $id_value, $table)
    {
        $CI = &get_instance();
        $CI->load->database();
        $CI->db->select('*');
        $CI->db->from($table);
        $CI->db->where($id, $id_value);
        $query = $CI->db->get();
        $result = $query->row();
        return $result;
    }
}

if (! function_exists('checkPermission')) {

    function checkPermission($type)
    {
        $CI = &get_instance();
        if (! isset($_SERVER['HTTP_REFERER']) && $CI->session->userdata('user_type') == $type) {
            echo "<h2>Sorry, You have No Permission To Access This Page !</h2>";
            echo "<a href=" . site_url('Admin/dashboard') . ">Go Dashboard</a>";
            exit();
        }
    }
}

if (! function_exists('displayMoney')) {

    function displayMoney($money, $highlight = true)
    {
        $price = get_current_setting('currency_code_session') . " " . decimalPlace($money);
        if ($highlight) {
            if (MIN_BALANCE > $money && $money > 0) {
                $highlight = 'text-info';
            } else if ($money < 0) {
                $highlight = 'text-danger';
            } else {
                $highlight = 'text-success';
            }
        }
        return '<span class="rupee '.$highlight.'">' . $price . '</span>';
    }
}

if (! function_exists('displayDate')) {

    function displayDate($d = 'today', $f = DATETIME_FORMAT_PHP, $date = '')
    {
        $rdate = '';
        switch ($d) {
            case 'today':
                $rdate = date($f);  // Use the provided format for today’s date
                break;
            case 'thisMonthFirstDay':
                $rdate = date($f, strtotime(date('Y-m-01')));  // First day of the current month with provided format
                break;
            case 'date':
                $rdate = date($f, strtotime($date));
                break;
            default:
                $rdate = date($f);  // Default to today’s date if no matching case
                break;
        }
        return $rdate;
    }
}

if (! function_exists('changeDateFormat')) {

    function changeDateFormat($dateInput, $f = DATETIME_FORMAT_DB)
    {
        return date($f, strtotime($dateInput));
    }
}

if (! function_exists('changeDate')) {

    function changeDate($date, $rotation, $f = DATETIME_FORMAT_DB)
    {
        $updatedDate = new DateTime($date);
        $updatedDate->modify($rotation);
        return $updatedDate->format($f);
    }
}

if (! function_exists('displayDateCalendar')) {

    function displayDateCalendar($trans_date, $size_class = 'size1x')
    {
        $timestamp = strtotime($trans_date);
        // $time_only_24hr = date('H:i:s', $timestamp);
        $time_only_12hr = date('h:i:s A', $timestamp);
        $date_trans = '<time datetime="' . $trans_date . '" class="date-as-calendar position-em ' . $size_class . '">
        <span class="weekday">' . date('l', $timestamp) . '</span>
        <span class="day">' . date('d', $timestamp) . '</span>
        <span class="month">' . date('F', $timestamp) . '</span>
        <span class="year">' . date('Y', $timestamp) . '</span>
        <span class="time">' . $time_only_12hr . '</span>
        </time>';
        return $date_trans;
    }
}

if (! function_exists('getDeviceType')) {

    function getDeviceType()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/mobile/i', $userAgent)) {
            return 'Mobile';
        } elseif (preg_match('/tablet|ipad/i', $userAgent)) {
            return 'Tablet';
        } else {
            return 'Desktop';
        }
    }
}

if (! function_exists('displayDescription')) {

    function displayDescription($t)
    {
        $result = $t->ref . '<span class="note">' . $t->note . '</span>';
        switch ($t->type) {
            case TYPE_INCOME:
                $result .= '<span>' . $t->payer . '</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>' . $t->accounts_name . '</span><span class="category-info">' . $t->category . '</span>';
                break;
            case TYPE_EXPENSE:
                $result .= '<span>' . $t->accounts_name . '</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>' . $t->payee . '</span><span class="category-info">' . $t->category . '</span>';
                break;
            case TYPE_TRANSFER:
                if (strpos($t->accounts_name, '||') !== false) {
                    $acc = split_account_by_delimiter($t->accounts_name);
                    $result .= '<span>' . $acc[0] . '</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>' . $acc[1] . '</span></span>';
                } else if (!empty($t->accounts_name)) {
                    if ($t->cr > 0) {
                        $result .= '<span>-</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>' . $t->accounts_name . '</span>';
                    } else if ($t->dr > 0) {
                        $result .= '<span>' . $t->accounts_name . '</span> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span>-</span>';
                    }
                }
                break;
        }
        return $result;
    }
}

if (! function_exists('group_result_by_key')) {

    function group_result_by_key($key, $data)
    {
        $result = array();
        // echo '<pre>'; print_r($data);echo '</pre>';
        foreach ($data as $val) {
            // echo '<pre>'; print_r($val);echo '</pre>';
            if (property_exists($val, $key)) {
                $result[$val->$key][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        // echo '<pre>'; print_r($result);echo '</pre>';

        return $result;
    }
}

if (! function_exists('split_account_by_delimiter')) {

    function split_account_by_delimiter($key)
    {
        return explode("||", $key);
    }
}

/**
 * $key = ['one', 'two'];
 */
if (! function_exists('merge_account_by_delimiter')) {

    function merge_account_by_delimiter($key)
    {
        return implode("||", $key);
    }
}

if (! function_exists('objectToArray')) {
    function objectToArray($obj)
    {
        if (is_object($obj)) {
            // Convert object properties to array
            $obj = get_object_vars($obj);
        }
        if (is_array($obj)) {
            // Recursively process each element
            return array_map('objectToArray', $obj);
        } else {
            // Return the value if it's neither an object nor an array
            return $obj;
        }
    }
}

if (! function_exists('convertArrayKey')) {

    function convertArrayKey($key)
    {
        // Trim the key to remove leading/trailing spaces
        $trimmedKey = trim($key);

        // Replace spaces with underscores and convert to lowercase
        return strtolower(str_replace(' ', '_', $trimmedKey));
    }
}

if (! function_exists('setDefaultValue')) {

    function setDefaultValue($value)
    {
        return !empty($value) ? $value : NULL;
    }
}

if (! function_exists('checkOrderOfArray')) {

    function checkOrderOfArray($transactions, $key)
    {
        // Extract the values based on the provided key
        $values = array_column($transactions, $key);

        // Check for ascending order
        $ascending = $values;
        sort($ascending); // Sort in ascending order
        if ($values === $ascending) {
            return "ASC";
        }

        // Check for descending order
        $descending = $values;
        rsort($descending); // Sort in descending order
        if ($values === $descending) {
            return "DESC";
        }

        return "Unordered";
    }
}
