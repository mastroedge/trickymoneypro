<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect('User');
        }
        $this->load->database();
        $this->load->model('Reportmodel');
        $this->load->model('Adminmodel');
        $this->load->model('Displaymodel');
        $this->load->model('Transaction_model');
    }

    //View Account Statement Report// 
    public function accountStatement($action = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(0);
        // Add a default option to the beginning of the $data['accounts'] array
        $default_account = (object) ['accounts_name' => 'ALL'];
        array_unshift($data['accounts'], $default_account);

        if ($action == 'asyn') {
            $this->load->view('reports/accountStatement', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/accountStatement', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $account = $this->input->post('accounts_name', true);
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);
            $trans_type = $this->input->post('trans_type', true);
            $order = $this->input->post('order', true);

            $check_balance = $this->input->post('check-balance', true);
            $check_amount = $this->input->post('check-amount', true);
            $check_calculation = $this->input->post('check-calculation', true);

            $reportData = $this->Reportmodel->getAccountStatement($account, $from_date, $to_date, $trans_type, $order);

            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_type" => $trans_type,
                    "show_amount" => $check_amount ? $check_amount : false,
                    "show_balance" => $check_balance ? $check_balance : false,
                    "show_calculations" => $check_calculation ? $check_calculation : false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
            }
        }
    }

    //Date Wise Income Report
    public function datewiseIncomeReport($action = '')
    {

        if ($action == 'asyn') {
            $this->load->view('reports/datewise_income_report');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/datewise_income_report');
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);

            $tdata = array(
                'type' => TYPE_INCOME,
                'from_date' => $from_date,
                'to_date' => $to_date
            );
            $reportData = $this->Transaction_model->getIncomeExpenseData($tdata);
            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_amount" => false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
            }
        }
    }


    //Day Wise Income Report
    public function daywiseIncomeReport($action = '')
    {

        if ($action == 'asyn') {
            $this->load->view('reports/daywise_income_report');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/daywise_income_report');
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);

            $tdata = array(
                'type' => TYPE_INCOME,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'group' => 'group'
            );
            $reportData = $this->Transaction_model->getIncomeExpenseData($tdata);
            if (empty($reportData)) {
                echo "false";
            } else {

                $sum = 0;
                foreach ($reportData as $report) {
                    $sum = $sum + $report->amount;
                ?>
                    <tr>
                        <td><?php echo displayDateCalendar($report->trans_date) ?></td>
                        <td class="text-end"><?php echo displayMoney($report->amount) ?></td>

                    <?php
                }
                echo "<tr><td><b>Total Income</b></td>";
                echo "<td class='text-end'><b>" . displayMoney($sum) . "</b></td></tr>";
            }
        }
    }

    //Date Wise Expense Report
    public function datewiseExpenseReport($action = '')
    {

        if ($action == 'asyn') {
            $this->load->view('reports/datewise_expense_report');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/datewise_expense_report');
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);

            $tdata = array(
                'type' => TYPE_EXPENSE,
                'from_date' => $from_date,
                'to_date' => $to_date
            );
            $reportData = $this->Transaction_model->getIncomeExpenseData($tdata);
            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_amount" => false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
            }
        }
    }

    //Day Wise Expense Report
    public function daywiseExpenseReport($action = '')
    {

        if ($action == 'asyn') {
            $this->load->view('reports/daywise_expense_report');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/daywise_expense_report');
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);

            $tdata = array(
                'type' => TYPE_EXPENSE,
                'from_date' => $from_date,
                'to_date' => $to_date,
                'group' => 'group'
            );
            $reportData = $this->Transaction_model->getIncomeExpenseData($tdata);
            if (empty($reportData)) {
                echo "false";
            } else {
                $sum = 0;
                foreach ($reportData as $report) {
                    $sum = $sum + $report->amount;
                ?>

                    <tr>
                        <td><?php echo displayDateCalendar($report->trans_date) ?></td>
                        <td class="text-end"><?php echo displayMoney($report->amount) ?></td>

                    <?php
                }
                echo "<tr><td><b>Total Expense</b></td>";
                echo "<td class='text-end'><b>" . displayMoney($sum) . "</b></td></tr>";
            }
        }
    }

    //Transfer Report
    public function transferReport($action = '')
    {

        if ($action == 'asyn') {
            $this->load->view('reports/transfer_report');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/transfer_report');
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);

            $reportData = $this->Reportmodel->getTransferReport($from_date, $to_date);
            if (empty($reportData)) {
                echo "false";
            } else {
                $dr = 0;
                $cr = 0;
                foreach ($reportData as $report) {
                    $dr = $dr + $report->dr;
                    $cr = $cr + $report->cr;
                    ?>

                    <tr>
                        <td><?php echo displayDateCalendar($report->trans_date) ?></td>
                        <td><?php echo $report->accounts_name ?></td>
                        <td><?php echo $report->ref ?></td>
                        <td><?php echo $report->payer ?></td>
                        <td><?php echo $report->note ?></td>
                        <td class="text-end"><?php echo displayMoney($report->dr) ?></td>
                        <td class="text-end"><?php echo displayMoney($report->cr) ?></td>
                    </tr>

            <?php
                }
                echo "<tr><td colspan='5'><b>Total</b>";
                echo "<td class='text-end'><b>" . displayMoney($dr) . "</b></td>";
                echo "<td class='text-end'><b>" . displayMoney($cr) . "</b></td></tr>";
            }
        }
    }

    //Transfer Report
    public function transactReport($action = '')
    {
        $data = array();
        if ($action == 'asyn') {
            $data['expense_data'] = $this->Reportmodel->getTransactReport();
            $data['income_data'] = $this->Reportmodel->getTransactReport(TYPE_INCOME);
            $data['income_expense'] = $this->Reportmodel->getCommonTransactReport($data['expense_data'], $data['income_data']);
            $this->load->view('reports/transact_report', $data);
        } else if ($action == '') {
            $data['expense_data'] = $this->Reportmodel->getTransactReport();
            $data['income_data'] = $this->Reportmodel->getTransactReport(TYPE_INCOME);
            $data['income_expense'] = $this->Reportmodel->getCommonTransactReport($data['expense_data'], $data['income_data']);
            $this->load->view('theme/include/header');
            $this->load->view('reports/transact_report', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {

            $isMobile = false;
            $deviceType = getDeviceType();
            if ($deviceType === 'Mobile') {
                $isMobile = true;
            }

            $type = $this->input->post('type', true);
            $payer = $this->input->post('name', true);

            $this->db->select('t.trans_id');
            $this->db->from('lending_transaction lt');
            $this->db->join('transaction t', 't.trans_id = lt.trans_id', 'left'); // Join with transaction table
            $this->db->where("t.type", $type);
            if ($type == TYPE_EXPENSE) {
                $this->db->where("t.payee", $payer);
            } else {
                $this->db->where("t.payer", $payer);
            }
            $trans = $this->db->get()->result();
            $trans_ids = array();
            foreach ($trans as $key => $t) {
                $trans_ids[$key] = $t->trans_id;
            }
            if (count($trans_ids) == 0) {
                echo 'Already Deleted';
                return;
            }
            $this->db->select('*');
            $this->db->from('transaction');
            $this->db->where_in("trans_id", $trans_ids);
            $this->db->order_by("trans_date", "DESC");
            $this->db->order_by("trans_id", "DESC");
            $t_data = $this->db->get()->result();
            ?>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed expense-table transaction-table">
                    <?= (!empty($payer) ? "<tr><th colspan='4'>" . $payer . "</th></tr>" : "") ?>
                    <tr>
                        <th class="col-date">Date</th>
                        <th class="col-description">Description</th>
                        <th class="col-amount">Amount</th>
                        <th class="text-end">Action</th>
                    </tr>
                    <?php foreach ($t_data as $t) { ?>
                        <tr>
                            <td><?php echo displayDateCalendar($t->trans_date, ($isMobile ? 'size0_75x' : 'size1x')) ?></td>
                            <td class="col-description"><?php echo displayDescription($t) ?></td>
                            <td class="text-end"><?php echo displayMoney($t->amount) ?></td>
                            <td><a href="" onclick="addToTransact(event, '<?= $t->trans_id ?>', '<?= site_url() ?>')"
                                    class="myicon aicon remove"><i class="fa fa-user-times" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>

                </table>
            </div>
            <?php
        }
    }

    //Income Vs Expense Report
    public function incomeVsExpense($action = '')
    {
        $data = array();
        if ($action == 'asyn' || $action == '') {
            $data['payerList'] = $this->Adminmodel->getPayerAndPayee('Payer');
            $data['payeeList'] = $this->Adminmodel->getPayerAndPayee('Payee');
            $data['accountList'] = $this->Adminmodel->getAllChartOfAccountsSplit();
            $data['accounts'] = $this->Adminmodel->getAllAccounts(0);

            $accounts = $this->Adminmodel->resultsObjectToArray($this->Adminmodel->getAllAccounts(1), 'accounts_name', 'accounts_name');
            $selmt = array(
                'name' => 'account_to',
                'options' => $accounts,
                'slected' => '',
                'js' => 'class="select2sel account_to form-control"',
                'isLabel' => true,
                'label' => 'Account Name',
                'divClass' => 'row',
                'labelClass' => 'col-md-12 col-lg-12 col-sm-12 col-4',
                'optionClass' => 'col-md-12 col-lg-12 col-sm-12 col-8'
            );
            $data['account_to'] = $this->Displaymodel->selectBox($selmt);
        }
        if ($action == 'asyn') {
            $this->load->view('reports/income_vs_expense_report', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/income_vs_expense_report', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);
            $order = $this->input->post('order', true);
            $payer = $this->input->post('payer', true);
            $payee = $this->input->post('payee', true);
            $income_account = $this->input->post('income_account', true);
            $expense_account = $this->input->post('expense_account', true);
            $cat_income = $this->input->post('cat-income[]', true);
            $cat_expense = $this->input->post('cat-expense[]', true);

            $check_balance = $this->input->post('check-balance', true);
            $check_income = $this->input->post('check-income', true);
            $check_expense = $this->input->post('check-expense', true);

            $dis_col = ($check_income && $check_expense) ? 6 : 12;

            $trans_table_input = array(
                "show_class" => true,
                "show_action" => true,
                "show_total" => true,
                "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                "show_checkbox" => true,
                "show_credit_debit" => true,
                "show_balance" => $check_balance,
                "show_amount" => false,
            );

            $disp_in_exp_table = '<div class="row m-0">';
            if ($check_income) {
                $incomes = array(
                    'type' => array(TYPE_INCOME, TYPE_TRANSFER),
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'payer' => $payer,
                    'category' => $cat_income,
                    'accounts_name' => $income_account,
                    'order' => $order
                );
                $income = $this->Transaction_model->getIncomeExpenseData($incomes);
                $income_total = $this->Adminmodel->calculateTotalFromTransactionsByType($income);

                $disp_in_exp_table .= '<div class="col-md-12 col-lg-' . $dis_col . ' col-sm-12 table-responsive">';
                $disp_in_exp_table .= '<h4 class="text-center">INCOME</h4>';
                $disp_in_exp_table .= (!empty($payer) ? "" . $payer . "" : "");
                $trans_table_input["t_data"] = $income;
                $trans_table_input['show_type'] = 'cr';
                $disp_in_exp_table .= $this->Displaymodel->getTransactionTable($trans_table_input);
                $disp_in_exp_table .= '</div>';
            }
            if ($check_expense) {
                $expenses = array(
                    'type' => array(TYPE_EXPENSE, TYPE_TRANSFER),
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'payee' => $payee,
                    'category' => $cat_expense,
                    'accounts_name' => $expense_account,
                    'order' => $order
                );
                $expense = $this->Transaction_model->getIncomeExpenseData($expenses);
                $expense_total = $this->Adminmodel->calculateTotalFromTransactionsByType($expense);

                $disp_in_exp_table .= '<div class="col-md-12 col-lg-' . $dis_col . ' col-sm-12 table-responsive">';
                $disp_in_exp_table .= '<h4 class="text-center">EXPENSE</h4>';
                $disp_in_exp_table .= (!empty($payee) ? "" . $payee . "" : "");
                $trans_table_input["t_data"] = $expense;
                $trans_table_input['show_type'] = 'dr';
                $disp_in_exp_table .= $this->Displaymodel->getTransactionTable($trans_table_input);
                $disp_in_exp_table .= '</div>';
            }
            $disp_in_exp_table .= '</div>';

            if ($check_income || $check_expense) {
                $disp_in_exp_table .= '<div class="col-md-12 col-lg-12 col-sm-12">';
                $disp_in_exp_table .= '<table class="table table-bordered">';
                $disp_in_exp_table .= '<tr><th class="text-center"></th><th class="text-center">Without Transfer</th><th class="text-center">With Transfer</th></tr>';
                if ($check_income) {
                    $disp_in_exp_table .= '<tr><td class="text-end">Total Income</td><td class="text-end">' . displayMoney($income_total[TYPE_INCOME]) . '</td><td class="text-end">' . displayMoney($income_total[TYPE_INCOME . AMOUNT_CR]) . '</td></tr>';
                }
                if ($check_expense) {
                    $disp_in_exp_table .= '<tr><td class="text-end">Total Expense</td><td class="text-end">' . displayMoney($expense_total[TYPE_EXPENSE]) . '</td><td class="text-end">' . displayMoney($expense_total[TYPE_EXPENSE . AMOUNT_DR]) . '</td></tr>';
                }
                if ($check_income && $check_expense) {
                    $disp_in_exp_table .= '<tr><td class="text-end">Total (Inc - Exp)</td><td class="text-end">' . displayMoney($income_total[TYPE_INCOME] - $expense_total[TYPE_EXPENSE]) . '</td><td class="text-end">' . displayMoney($income_total[TYPE_INCOME . AMOUNT_CR] - $expense_total[TYPE_EXPENSE . AMOUNT_DR]) . '</td></tr>';
                }
                $disp_in_exp_table .= '</table>';
                $disp_in_exp_table .= '</div>';
            }

            echo $disp_in_exp_table;
        }
    }

    // Report By Chart of Accounts
    public function incomeCategoryReport($action = '')
    {
        $data = array();
        $data['accountList'] = $this->Adminmodel->getAllChartOfAccountsSplit();
        if ($action == 'asyn') {
            $this->load->view('reports/income_cat_report', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/income_cat_report', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);
            $category = $this->input->post('category[]', true);

            $reportData = $this->Reportmodel->getCategoryReport($from_date, $to_date, $category);
            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_amount" => false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
             }
        }
    }


    //Report By Payer
    public function reportByPayer($action = '')
    {
        $data = array();
        $data['payerList'] = $this->Adminmodel->getPayerAndPayee('Payer');
        if ($action == 'asyn') {
            $this->load->view('reports/report_by_payer', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/report_by_payer', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);
            $payer = $this->input->post('payer', true);

            $reportData = $this->Reportmodel->getPayerReport($from_date, $to_date, $payer);
            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_amount" => false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
            }
        }
    }

    //Report By Payee
    public function reportByPayee($action = '')
    {
        $data = array();
        $data['payeeList'] = $this->Adminmodel->getPayerAndPayee('Payee');
        if ($action == 'asyn') {
            $this->load->view('reports/report_by_payee', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('reports/report_by_payee', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'view') {
            $from_date = $this->input->post('from-date', true);
            $to_date = $this->input->post('to-date', true);
            $to_date = changeDate($to_date, '+1 day', DATE_ONLY_FORMAT);
            $payee = $this->input->post('payee', true);

            $reportData = $this->Reportmodel->getPayeeReport($from_date, $to_date, $payee);
            if (empty($reportData)) {
                echo "false";
            } else {
                $trans_table_input = array(
                    "t_data" => $reportData,
                    "show_class" => true,
                    "show_action" => true,
                    "show_total" => true,
                    "lend_array" => $this->Adminmodel->getLendingTransactionArray(),
                    "show_checkbox" => true,
                    "show_credit_debit" => true,
                    "show_amount" => false
                );
                echo $this->Displaymodel->getTransactionTable($trans_table_input);
            }
        }
    }
}
