<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect('User');
        }
        $this->load->database();
        $this->load->model('Adminmodel');
        $this->load->model('Displaymodel');
        $this->load->model('Transaction_model');
        $this->load->model('Accounts_model');
        $this->load->model('Payment_method_model');
        $this->load->model('Chart_of_accounts_model');
        $this->load->model('Payee_payers_model');
        $this->load->model('Bulk_transaction_model');
        $this->load->model('Repeat_transaction_model');
        $this->load->model('Vehicle_model');
        $this->load->model('Vehicle_trip_model');
        $this->load->model('Vehicle_service_model');
        $this->load->model('Vehicle_fuels_model');
        $this->load->model('Tasks_model');
        $this->load->library('form_validation');
    }

    public function index($action = '')
    {
        $data = array();
        $this->load->model('Reportmodel');
        $data['cart_summery'] = $this->Reportmodel->getIncomeExpense();
        $data['line_chart'] = $this->Reportmodel->dayByDayIncomeExpense();
        $incomes = array(
            'type' => array(TYPE_INCOME),
            'limit' => 5
        );
        $trans_table_input = array(
            "t_data" => $this->Transaction_model->getIncomeExpenseData($incomes),
            "show_class" => true
        );
        $data['latest_income'] = $this->Displaymodel->getTransactionTable($trans_table_input);

        $expenses = array(
            'type' => array(TYPE_EXPENSE),
            'limit' => 5
        );
        $trans_table_input["t_data"] = $this->Transaction_model->getIncomeExpenseData($expenses);
        $data['latest_expense'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        $trans_table_input["t_data"] = $this->Adminmodel->getTransaction(5, TYPE_TRANSFER);
        $data['latest_transfer'] = $this->Displaymodel->getTransactionTable($trans_table_input);

        $data['pie_data'] = $this->Reportmodel->sumOfIncomeExpense();
        $data['pie_data'] = $this->Reportmodel->sumOfIncomeExpense();
        $data['financialBalance'] = $this->Reportmodel->financialBalance(2);
        if ($action == 'asyn') {
            $this->load->view('theme/index', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/index', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For dashboard *
     */
    public function dashboard($action = '')
    {
        $data = array();
        $this->load->model('Reportmodel');
        $data['cart_summery'] = $this->Reportmodel->getIncomeExpense();
        $data['line_chart'] = $this->Reportmodel->dayByDayIncomeExpense();
        $incomes = array(
            'type' => array(TYPE_INCOME),
            'limit' => 5
        );
        $trans_table_input = array(
            "t_data" => $this->Transaction_model->getIncomeExpenseData($incomes),
            "show_class" => true
        );
        $data['latest_income'] = $this->Displaymodel->getTransactionTable($trans_table_input);

        $expenses = array(
            'type' => array(TYPE_EXPENSE),
            'limit' => 5
        );
        $trans_table_input["t_data"] = $this->Transaction_model->getIncomeExpenseData($expenses);
        $data['latest_expense'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        $trans_table_input["t_data"] = $this->Adminmodel->getTransaction(5, TYPE_TRANSFER);
        $data['latest_transfer'] = $this->Displaymodel->getTransactionTable($trans_table_input);

        $data['pie_data'] = $this->Reportmodel->sumOfIncomeExpense();
        $data['pie_data'] = $this->Reportmodel->sumOfIncomeExpense();
        $data['financialBalance'] = $this->Reportmodel->financialBalance(2);
        if ($action == 'asyn') {
            $this->load->view('theme/index', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/index', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For Add New Account and Account Page View *
     */
    public function addAccount($action = '', $param1 = null)
    {
        if ($action == 'asyn') {
            $this->load->view('theme/accounts/add_account1');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/accounts/add_account1');
            $this->load->view('theme/include/footer');
        }
        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['account_type'] = $this->input->post('account_type', true);
            $data['note'] = $this->input->post('note', true);
            $data['active'] = $this->input->post('active', true);
            $datetime24hr = changeDateFormat($this->input->post('updated', true));
            $data['updated'] = $datetime24hr;
            $data['user_id'] = $this->session->userdata('user_id');
            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required|min_length[4]|max_length[30]');
            $this->form_validation->set_rules('note', 'Note', 'trim|required');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists("accounts", "accounts_name", $data['accounts_name'])) {
                        $data['opening_balance'] = $this->input->post('opening_balance', true);

                        $this->db->trans_begin();
                        $this->db->insert('accounts', $data);
                        // insert Transaction Data
                        $this->insertTransactionInitial($data['accounts_name'], $data['opening_balance'], $data['updated']);
                        $this->db->trans_complete();
                        if ($this->db->trans_status() === FALSE) {
                            $this->db->trans_rollback();
                        } else {
                            echo "true";
                            $this->db->trans_commit();
                        }
                    } else {
                        echo "This Account Is Already Exists !";
                    }
                } else if ($do == 'update') {
                    $isUpdate = true;
                    $id = $this->input->post('accounts_id', true);

                    $old_name = getOld('accounts_id', $id, 'accounts');

                    // Start a transaction
                    $this->db->trans_start(); // Begin transaction

                    // Update the `accounts` table
                    $this->db->where('accounts_id', $id);
                    $this->db->update('accounts', $data);

                    $old_name = $old_name->accounts_name;
                    $new_name = $data['accounts_name'];

                    if ($old_name != $new_name) {
                        // Update the `transaction` table
                        $this->db->set('accounts_name', $new_name);
                        $this->db->where('accounts_name', $old_name);
                        $this->db->update('transaction');

                        // Update the `repeat_transaction` table with REPLACE function
                        $this->db->set('accounts_name', "REPLACE(accounts_name, '{$old_name}', '{$new_name}')", FALSE);
                        $this->db->like('accounts_name', $old_name);
                        $this->db->update('repeat_transaction');

                        // Update the `bulk_transaction` table with REPLACE function
                        $this->db->set('tansactions', "REPLACE(tansactions, '{$old_name}', '{$new_name}')", FALSE);
                        $this->db->like('tansactions', $old_name);
                        $this->db->update('bulk_transaction');
                    }

                    // Commit or rollback based on transaction status
                    $this->db->trans_complete(); // End transaction

                    // Check if the transaction was successful
                    if ($this->db->trans_status() === FALSE) {
                        // Transaction failed, rollback
                        $this->db->trans_rollback();
                        // Handle error
                        echo "false";
                    } else {
                        // Transaction successful, commit
                        $this->db->trans_commit();
                        echo "true";
                    }
                }
            } else {
                // echo "All Field Must Required With Valid Length !";
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('accounts', array(
            //     'accounts_id' => $param1
            // ));
        }
    }

    /**
     * Method For insert Transaction when new account create *
     */
    // TODO DELETE
    public function insertTransactionInitial($account, $amount, $date)
    {
        $data = array();
        $data['accounts_name'] = $account;
        $data['trans_date'] = changeDateFormat($date);
        $data['type'] = TYPE_TRANSFER;
        $data['category'] = '';
        $data['amount'] = $amount;
        $data['payer'] = 'System';
        $data['payee'] = '';
        $data['p_method'] = '';
        $data['ref'] = '';
        $data['note'] = 'Opening Balance';
        $data['dr'] = 0;
        $data['cr'] = $amount;
        $data['bal'] = $amount;
        $data['user_id'] = $this->session->userdata('user_id');
        $this->db->insert('transaction', $data);
    }

    /**
     * Method For view Manage Account Page *
     */
    public function manageAccount($action = '', $param1 = null)
    {
        if ($action == 'asyn' || $action == '') {
            $data = array();
            $data['accounts'] = $this->Adminmodel->getAllAccounts();
            $this->load->model('Reportmodel');
            $data['financialBalance'] = $this->Reportmodel->financialBalance();
            $data['fBalance'] = array();
            foreach ($data['accounts'] as $f) {
                foreach ($data['financialBalance'] as $fs) {
                    if ($fs->accounts_id == $f->accounts_id) {
                        $data['fBalance'][$f->accounts_id] = $fs->balance;
                        break;
                    }
                }
            }
        }
        if ($action == 'asyn') {
            $this->load->view('theme/accounts/manage_account', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/accounts/manage_account', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'addForm') {
            $data = array();
            if (!is_null($param1)) {
                $data['single'] = $this->Accounts_model->get_account_by_id($param1);
            }
            $content = $this->load->view('theme/accounts/add_account', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Edit Account" : 'Add Account'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'insert') {
            $data = [];
            $allowed_fields = ['accounts_name', 'account_type', 'note', 'active', 'updated'];
            $data = array_intersect_key($this->input->post(null, true), array_flip($allowed_fields));
            $data['updated'] = changeDateFormat($data['updated'] ?? '');
            $data['user_id'] = $this->session->userdata('user_id');

            $do = $this->input->post('action', true);

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required|min_length[4]|max_length[30]');
            $this->form_validation->set_rules('note', 'Note', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                // If validation fails, return error message
                $response = array(
                    'status' => 'error',
                    'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>'
                );
            } else {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists("accounts", "accounts_name", $data['accounts_name'])) {
                        $data['opening_balance'] = $this->input->post('opening_balance', true);

                        $this->db->trans_begin();
                        $this->db->insert('accounts', $data);
                        // insert Transaction Data
                        $this->Transaction_model->insertTransactionInitial($data['accounts_name'], $data['opening_balance'], $data['updated']);
                        $this->db->trans_complete();

                        // Check if the transaction was successful
                        if ($this->db->trans_status() === FALSE) {
                            // Transaction failed, rollback
                            $this->db->trans_rollback();
                            // Handle error
                            $response = array(
                                'status' => 'error',
                                'message' => '<div class="alert alert-danger" role="alert">
                            <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                            </div>'
                            );
                        } else {
                            // Transaction successful, commit
                            $this->db->trans_commit();
                            $response = array(
                                'status' => 'success',
                                'message' => '<div class="alert alert-success" role="alert">
                            <i class="bi bi-check-circle me-2"></i> Account added successfully!
                            </div>'
                            );
                        }
                    } else {
                        echo "This Account Is Already Exists !";
                    }
                } else if ($do == 'update') {
                    $isUpdate = true;
                    $id = $this->input->post('accounts_id', true);

                    $old_name = getOld('accounts_id', $id, 'accounts');

                    // Start a transaction
                    $this->db->trans_start(); // Begin transaction

                    // Update the `accounts` table
                    $this->db->where('accounts_id', $id);
                    $this->db->update('accounts', $data);

                    $old_name = $old_name->accounts_name;
                    $new_name = $data['accounts_name'];

                    if ($old_name != $new_name) {
                        // Update the `transaction` table
                        $this->db->set('accounts_name', $new_name);
                        $this->db->where('accounts_name', $old_name);
                        $this->db->update('transaction');

                        // Update the `repeat_transaction` table with REPLACE function
                        $this->db->set('accounts_name', "REPLACE(accounts_name, '{$old_name}', '{$new_name}')", FALSE);
                        $this->db->like('accounts_name', $old_name);
                        $this->db->update('repeat_transaction');

                        // Update the `bulk_transaction` table with REPLACE function
                        $this->db->set('tansactions', "REPLACE(tansactions, '{$old_name}', '{$new_name}')", FALSE);
                        $this->db->like('tansactions', $old_name);
                        $this->db->update('bulk_transaction');
                    }

                    // Commit or rollback based on transaction status
                    $this->db->trans_complete(); // End transaction

                    // Check if the transaction was successful
                    if ($this->db->trans_status() === FALSE) {
                        // Transaction failed, rollback
                        $this->db->trans_rollback();
                        // Handle error
                        $response = array(
                            'status' => 'error',
                            'message' => '<div class="alert alert-danger" role="alert">
                            <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                            </div>'
                        );
                    } else {
                        // Transaction successful, commit
                        $this->db->trans_commit();
                        $response = array(
                            'status' => 'success',
                            'message' => '<div class="alert alert-success" role="alert">
                            <i class="bi bi-check-circle me-2"></i> Account added successfully!
                            </div>'
                        );
                    }
                }
            }
            // ----End validation----//

            // Send JSON response
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));

        } elseif ($action == 'editDate') {
            $data = array();
            $this->db->trans_begin();
            $id = $this->input->post('accounts_id', true);
            $data['updated'] = $this->input->post('account_date', true);
            $this->db->where('accounts_id', $id);
            $this->db->update('accounts', $data);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['status'] = 0;
                $data['msg'] = 'Error';
            } else {
                $this->db->trans_commit();
                $data['status'] = 1;
            }

            echo json_encode($data);
        }
    }

    /**
     * Method For get account information for Account Edit *
     */
    public function editAccount($accounts_id, $action = '')
    {
        $data = array();
        $data['edit_account'] = $this->Adminmodel->getAccount($accounts_id);
        if ($action == 'asyn') {
            $this->load->view('theme/accounts/add_account1', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/accounts/add_account1', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For Income Page And Create New Income *
     */
    public function addIncome($action = '', $param1 = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(1);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_INCOME, STATUS_ACTIVE);
        $data['payers'] = $this->Adminmodel->getPayerAndPayee('Payer', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        $trans_table_input = array(
            "t_data" => $this->Adminmodel->getTransaction(20, TYPE_INCOME),
            "show_class" => true,
            "show_action" => true,
            "lend_array" => $this->Adminmodel->getLendingTransactionArray()
        );
        $data['t_data'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/add_income', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/add_income', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('income-date', true));
            $data['type'] = TYPE_INCOME;
            $data['category'] = $this->input->post('income-type', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = $this->input->post('payer', true);
            $data['payee'] = '';
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['note'] = $this->input->post('note', true);
            $data['dr'] = 0;
            $data['cr'] = $this->input->post('amount', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('income-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('income-type', 'Income Type', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('payer', 'Payer', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                $last_transaction = $this->Adminmodel->getLastTransactionByAccountName($data['accounts_name']);
                $data['bal'] = $this->Adminmodel->calculateAmount($last_transaction->bal, $data['amount'], "add");
                // $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "add");

                if ($do == 'insert') {
                    if ($this->db->insert('transaction', $data)) {
                        if ($last_transaction->trans_date > $data['trans_date']) {
                            $this->Adminmodel->updateAccountBalanceByDate($data['trans_date'], $data['accounts_name']);
                        }
                        echo "true";
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('trans_id', true);
                    $this->db->where('trans_id', $id);
                    $this->db->update('transaction', $data);
                    echo "true";
                }
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    public function addIncomeSplit($action = '', $param1 = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(1);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_INCOME, STATUS_ACTIVE);
        $data['payers'] = $this->Adminmodel->getPayerAndPayee('Payer', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        $trans_table_input = array(
            "t_data" => $this->Adminmodel->getTransaction(20, TYPE_INCOME),
            "show_class" => true
        );
        $data['t_data'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/add_income_split', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/add_income_split', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('income-date', true));
            $data['type'] = TYPE_INCOME;
            $data['category'] = $this->input->post('income-type', true);
            // $data['amount'] = $this->input->post('amount', true);
            $amount = $this->input->post('amount[]', true);
            // $data['payer'] = $this->input->post('payer', true);
            $payer = $this->input->post('payer[]', true);
            $data['payee'] = '';
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['note'] = $this->input->post('note', true);
            $data['dr'] = 0;
            // $data['cr'] = $this->input->post('amount', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('income-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('income-type', 'Income Type', 'trim|required');
            // $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            // $this->form_validation->set_rules('payer', 'Payer', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                $return = "false";
                foreach ($amount as $key => $amt) {
                    if ($amt <= 0)
                        continue;

                    $data['amount'] = $amt;
                    $data['payer'] = $payer[$key];
                    $data['cr'] = $amt;
                    $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "add");

                    if ($do == 'insert') {
                        if ($this->db->insert('transaction', $data)) {
                            $return = "true";
                        }
                    } else if ($do == 'update') {
                        $id = $this->input->post('trans_id', true);
                        $this->db->where('trans_id', $id);
                        $this->db->update('transaction', $data);
                        $return = "true";
                    }
                }
                echo $return;
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    /**
     * Method For Expense Page And Create New Expense *
     */
    public function addExpense($action = '', $param1 = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(1);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_EXPENSE, STATUS_ACTIVE);
        $data['payee'] = $this->Adminmodel->getPayerAndPayee('Payee', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        $trans_table_input = array(
            "t_data" => $this->Adminmodel->getTransaction(20, TYPE_EXPENSE),
            "show_class" => true,
            "show_action" => true,
            "lend_array" => $this->Adminmodel->getLendingTransactionArray()
        );
        $data['t_data'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/add_expense', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/add_expense', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('expense-date', true));
            $data['type'] = TYPE_EXPENSE;
            $data['category'] = $this->input->post('expense-type', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = '';
            $data['payee'] = $this->input->post('payee', true);
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['note'] = $this->input->post('note', true);
            $data['dr'] = $this->input->post('amount', true);
            $data['cr'] = 0;
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('expense-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('expense-type', 'Expense Type', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('payee', 'Payee', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                $last_transaction = $this->Adminmodel->getLastTransactionByAccountName($data['accounts_name']);
                $data['bal'] = $this->Adminmodel->calculateAmount($last_transaction->bal, $data['amount'], "sub");
                // $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "sub");

                if ($do == 'insert') {
                    if ($this->db->insert('transaction', $data)) {
                        if ($last_transaction->trans_date > $data['trans_date']) {
                            $this->Adminmodel->updateAccountBalanceByDate($data['trans_date'], $data['accounts_name']);
                        }
                        echo "true";
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('trans_id', true);
                    $this->db->where('trans_id', $id);
                    $this->db->update('transaction', $data);
                    echo "true";
                }
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    public function addExpenseSplit($action = '', $param1 = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(1);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_EXPENSE, STATUS_ACTIVE);
        $data['payee'] = $this->Adminmodel->getPayerAndPayee('Payee', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        $trans_table_input = array(
            "t_data" => $this->Adminmodel->getTransaction(20, TYPE_EXPENSE),
            "show_class" => true
        );
        $data['t_data'] = $this->Displaymodel->getTransactionTable($trans_table_input);
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/add_expense_split', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/add_expense_split', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('expense-date', true));
            $data['type'] = TYPE_EXPENSE;
            $data['category'] = $this->input->post('expense-type', true);
            // $data['amount'] = $this->input->post('amount', true);
            $amount = $this->input->post('amount[]', true);
            $data['payer'] = '';
            // $data['payee'] = $this->input->post('payee', true);
            $payee = $this->input->post('payee[]', true);
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['note'] = $this->input->post('note', true);
            // $data['dr'] = $this->input->post('amount', true);
            $data['cr'] = 0;
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('expense-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('expense-type', 'Expense Type', 'trim|required');
            // $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            // $this->form_validation->set_rules('payee', 'Payee', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                $return = "false";
                foreach ($amount as $key => $amt) {
                    if ($amt <= 0)
                        continue;

                    $data['amount'] = $amt;
                    $data['payee'] = $payee[$key];
                    $data['dr'] = $amt;
                    $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "sub");

                    if ($do == 'insert') {
                        if ($this->db->insert('transaction', $data)) {
                            $return = "true";
                        }
                    } else if ($do == 'update') {
                        $id = $this->input->post('trans_id', true);
                        $this->db->where('trans_id', $id);
                        $this->db->update('transaction', $data);
                        $return = "true";
                    }
                }
                echo $return;
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    function account_check($str)
    {
        $ty = $this->input->post('type[]', true);
        $acc = $this->input->post('accounts_name[]', true);
        $acc_to = $this->input->post('account_to[]', true);
        foreach ($ty as $ke => $t) {
            if ($t == TYPE_TRANSFER && $acc[$ke] == $acc_to[$ke]) {
                $this->form_validation->set_message('account_check', 'Account name should not be same.');
                return false;
            }
        }
        return true;
    }

    public function bulkTransactionAjax($action = '')
    {
        if ($action == 'validate') {
            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name[]', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('account_to[]', 'Account To', 'trim|required');
            $this->form_validation->set_rules('income_expense_date[]', 'Date', 'trim|required');
            $this->form_validation->set_rules('income_expense_type[]', 'Type', 'trim|required|callback_account_check');
            $this->form_validation->set_rules('amount[]', "Amount", "trim|required|numeric|greater_than[0]");
            $this->form_validation->set_rules('payee_payers[]', 'Payee Payers', 'trim|required');
            $this->form_validation->set_rules('p_method[]', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference[]', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note[]', 'Note', 'trim|required|min_length[8]');
            if (!$this->form_validation->run() == FALSE) {
                echo "true";
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
        } else if ($action == 'insert') {
            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name[]', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('account_to[]', 'Account To', 'trim|required');
            $this->form_validation->set_rules('income_expense_date[]', 'Date', 'trim|required');
            $this->form_validation->set_rules('income_expense_type[]', 'Type', 'trim|required|callback_account_check');
            $this->form_validation->set_rules('amount[]', "Amount", "trim|required|numeric|greater_than[0]");
            $this->form_validation->set_rules('payee_payers[]', 'Payee Payers', 'trim|required');
            $this->form_validation->set_rules('p_method[]', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference[]', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note[]', 'Note', 'trim|required|min_length[8]');
            $recalculate = $this->input->post('recalculate', true);

            if (!$this->form_validation->run() == FALSE) {
                $this->db->trans_begin();
                // $do = $this->input->post('action', true);
                $amount = $this->input->post('amount[]', true);

                $unique_accounts = array_unique($this->input->post('accounts_name[]', true));
                $this->Transaction_model->getCurrentBalances($unique_accounts);

                $lowestDateTransactions = [];

                foreach ($amount as $key => $amt) {
                    if ($amt <= 0)
                        continue;
                    $type = $this->input->post('type[' . $key . ']', true);

                    $transaction = new Transaction();
                    if ($type == TYPE_TRANSFER) {
                        $transaction->setAccountsName($this->Transaction_model->mergeAccountNameForTransfer($this->input->post('accounts_name[' . $key . ']', true), $this->input->post('account_to[' . $key . ']', true)));
                    } else {
                        $transaction->setAccountsName($this->input->post('accounts_name[' . $key . ']', true));
                    }
                    $datetime24hr = changeDateFormat($this->input->post('income_expense_date[' . $key . ']', true));
                    $transaction->setTransDate($datetime24hr);
                    $transaction->setType($type);
                    $transaction->setCategory($this->input->post('income_expense_type[' . $key . ']', true));
                    $transaction->setAmount($this->input->post('amount[' . $key . ']', true));
                    $transaction->setPayer($this->input->post('payee_payers[' . $key . ']', true));
                    $transaction->setPayee($this->input->post('payee_payers[' . $key . ']', true));
                    $transaction->setPMethod($this->input->post('p_method[' . $key . ']', true));
                    $transaction->setRef($this->input->post('reference[' . $key . ']', true));
                    $transaction->setNote($this->input->post('note[' . $key . ']', true));

                    if ($recalculate == '1' && ($type == TYPE_INCOME || $type == TYPE_EXPENSE)) {
                        // Check if this is the lowest date for the account
                        if (!isset($lowestDateTransactions[$transaction->getAccountsName()]) || $datetime24hr < $lowestDateTransactions[$transaction->getAccountsName()]->getTransDate()) {
                            $lowestDateTransactions[$transaction->getAccountsName()] = $transaction;
                        }
                    }
                    // echo '<pre>';print_r($transaction);echo '</pre>';
                    $this->Transaction_model->insertTransaction($transaction);
                }
                // end transaction
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_commit();
                    if ($recalculate == '1') {
                        // Insert only the transactions with the lowest date for each account
                        foreach ($lowestDateTransactions as $lt) {
                            $this->Adminmodel->updateAccountBalanceByDate($lt->getTransDate(), $lt->getAccountsName());
                        }
                    }
                    echo "true";
                }
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'search-transactions') {
            // Get the search query from the request
            $query = $this->input->post('query');

            // Validate the query
            if (empty($query)) {
                echo json_encode(['error' => 'Query cannot be empty']);
                return;
            }

            // Fetch transactions based on the query
            $this->db->where_in([TYPE_INCOME, TYPE_EXPENSE]);
            $this->db->like('amount', $query);
            $this->db->or_like('ref', $query);
            $this->db->or_like('note', $query);
            $this->db->limit(10);
            $this->db->order_by('trans_date', 'DESC');
            $transactions = $this->db->get('transaction')->result();

            // Return the list of transactions as a JSON response
            echo json_encode($transactions);
        }
        // TODO: Need to do
        // else if ($action == 'remove') {
        //     $this->db->delete('transaction', array(
        //         'trans_id' => $param1
        //     ));
        // }
    }

    public function getAccountNamesBulkTransaction($transact)
    {
        // Get the account and account_to arrays
        $accounts = $transact->accounts_name;
        $accounts_to = $transact->account_to;

        // Merge both arrays
        $merged_accounts = array_merge($accounts, $accounts_to);

        // Get unique values
        $unique_accounts = array_unique($merged_accounts);

        // Remove 'NILL' from the unique accounts
        $filtered_accounts = array_filter($unique_accounts, function ($account) {
            return $account !== 'NILL';
        });

        // Re-index the array to ensure clean keys
        $filtered_accounts = array_values($filtered_accounts);

        // Print the filtered unique accounts
        // print_r($filtered_accounts);

        return $filtered_accounts;
    }

    public function bulkTransaction($action = '', $param1 = '', $status = '')
    {
        $data = array();
        $data['action'] = $action;
        $data['form_html'] = '';
        if ($action == 'remove' && !empty($param1)) {
            $this->Bulk_transaction_model->deleteBulkTransactionById($param1);
        } else if ($action == 'action' && !empty($param1)) {
            $this->Bulk_transaction_model->changeStatus($param1, $status);
        }
        if ($action == 'asyn' || $action == '' || $action == 'remove') {
            $data['allbulktrans'] = $this->Bulk_transaction_model->getAllBulkTransactionList();
        }

        if ($action == 'asyn') {
            $this->load->view('theme/transactions/bulk_transaction', $data);
        } else if ($action == '' || $action == 'view' || $action == 'remove') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/bulk_transaction', $data);
            $this->load->view('theme/include/footer');
        }
    }

    public function bulkTransactionView($action = '', $param1 = '')
    {

        $data = array();
        $data['form_html'] = '';
        $transact = '';
        if (!empty($param1)) {
            $transaction = $this->Bulk_transaction_model->getBulkTransactionById($param1);
            if (is_null($transaction)) {
                redirect(site_url('Admin/transactions/bulkTransactionView/'), 'refresh');
            }
            $transact = json_decode($transaction->tansactions);
        }

        $data['form_html'] .= $this->Displaymodel->getBulkTransactionViewList($param1, $transact);

        $data['action'] = $action;
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/bulk_transaction_view', $data);
        } else if ($action == '' || $action == 'show') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/bulk_transaction_view', $data);
            $this->load->view('theme/include/footer');
        }

        if ($action == TYPE_INCOME || $action == TYPE_EXPENSE || $action == TYPE_TRANSFER) {
            if (!empty($this->input->post())) {
                // var_dump($this->input->post());
                $type = $this->input->post('type[0]', true);

                $transaction = new Transaction();
                if ($type == TYPE_TRANSFER) {
                    $transaction->setAccountsName($this->Transaction_model->mergeAccountNameForTransfer($this->input->post('accounts_name[0]', true), $this->input->post('account_to[0]', true)));
                } else {
                    $transaction->setAccountsName($this->input->post('accounts_name[0]', true));
                }
                $transaction->setTransDate($this->input->post('income_expense_date[0]', true));
                $transaction->settype($type);
                $transaction->setCategory($this->input->post('income_expense_type[0]', true));
                $transaction->setAmount($this->input->post('amount[0]', true));
                $transaction->setPayer($this->input->post('payee_payers[0]', true));
                $transaction->setPayee($this->input->post('payee_payers[0]', true));
                $transaction->setPMethod($this->input->post('p_method[0]', true));
                $transaction->setRef($this->input->post('reference[0]', true));
                $transaction->setNote($this->input->post('note[0]', true));
                // $this->Transaction_model->slno = $this->input->post('slno', true);
                echo $this->Displaymodel->getTransactionBox($action, $transaction);
            } else {
                echo $this->Displaymodel->getTransactionBox($action, '');
            }
        }

        if ($action == 'Insert') {
            $form_data = $this->input->post();
            $this->db->insert('bulk_transaction', array(
                'tansactions' => json_encode($form_data),
                'user_id' => $this->session->userdata('user_id')
            ));
            // echo '<pre>';print_r($form_data);echo '</pre>';
            //echo '<pre>';print_r(json_encode($form_data));echo '</pre>';
            echo $this->db->insert_id();
        } else if ($action == 'Update') {
            $form_data = $this->input->post();
            $this->db->where('trans_id', $param1);
            $this->db->update('bulk_transaction', array(
                'tansactions' => json_encode($form_data),
                'user_id' => $this->session->userdata('user_id')
            ));
            echo $param1;
        }
    }

    /**
     * Method For Transfer Page and create new transfer *
     */
    public function transfer($action = '', $param1 = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(1);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        $trans_table_input = array(
            "t_data" => $this->Adminmodel->getTransaction(40, TYPE_TRANSFER),
            "show_class" => true
        );
        $data['t_data'] = $this->Displaymodel->getTransactionTable($trans_table_input);

        if ($action == 'asyn') {
            $this->load->view('theme/transactions/add_transfer', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/add_transfer', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $to_account = $this->input->post('to-account', true);
            $data['accounts_name'] = $this->input->post('from-account', true);
            $data['trans_date'] = changeDateFormat($this->input->post('transfer-date', true));
            $data['type'] = TYPE_TRANSFER;
            $data['category'] = '';
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = '';
            $data['payee'] = '';
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['note'] = $this->input->post('note', true);
            $data['dr'] = $this->input->post('amount', true);
            $data['cr'] = 0;
            $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "sub");
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('from-account', 'From Account', 'trim|required');
            $this->form_validation->set_rules('to-account', 'To Account', 'trim|required');
            $this->form_validation->set_rules('transfer-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    if ($data['accounts_name'] != $to_account) {
                        $this->db->trans_begin();
                        $this->db->insert('transaction', $data);
                        $data['accounts_name'] = $to_account;
                        $data['dr'] = 0;
                        $data['cr'] = $this->input->post('amount', true);
                        $data['bal'] = $this->Adminmodel->getBalance($data['accounts_name'], $data['amount'], "add");
                        $this->db->insert('transaction', $data);

                        if ($this->db->trans_status() === FALSE) {
                            $this->db->trans_rollback();
                        } else {
                            echo "true";
                            $this->db->trans_commit();
                        }
                    } else {
                        echo "Sorry, Cannot Transfer Between Same Account !";
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('trans_id', true);
                    $this->db->where('trans_id', $id);
                    $this->db->update('transaction', $data);
                    echo "true";
                }
            } else {
                // echo "All Field Must Required With Valid Length !";
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    public function manageTransact($action = '')
    {
        $status = 0;
        $data = array();
        $trans_id = $this->input->post('trans_id', true);
        switch ($action) {
            case 'insert';
                $data['lend_id'] = NULL;
                $data['trans_id'] = $trans_id;
                if ($this->db->insert('lending_transaction', $data)) {
                    $status = 'insert';
                }
                break;
            case 'remove':
                $this->db->where('trans_id', $trans_id);
                if ($this->db->delete('lending_transaction')) {
                    $status = 'remove';
                }
                break;
        }
        echo $status;
    }

    /**
     * Method For view manage Income page *
     */
    public function manageIncome($action = '')
    {
        $data = array();
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/manage_income_ajax', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/manage_income_ajax', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view manage Expense page Ajax*
     */
    public function manageIncomeAjax()
    {
        //output to json format
        echo json_encode($this->Adminmodel->getTransactionAjax(TYPE_INCOME));
    }

    /**
     * Method For view manage Expense page *
     */
    public function manageExpense($action = '')
    {
        $data = array();
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/manage_expense_ajax', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/manage_expense_ajax', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view manage Expense page Ajax*
     */
    public function manageExpenseAjax()
    {
        //output to json format
        echo json_encode($this->Adminmodel->getTransactionAjax(TYPE_EXPENSE));
    }

    /**
     * Method For get data for edit transaction *
     */
    public function editTransaction($type, $trans_id, $action = '')
    {
        $data = array();
        $data['transaction'] = $this->Adminmodel->getSingleTransactionById($trans_id);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod();
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType($type);
        if ($type == TYPE_INCOME) {
            $data['payers'] = $this->Adminmodel->getPayerAndPayee('Payer');
        } else {
            $data['payee'] = $this->Adminmodel->getPayerAndPayee('Payee');
        }
        $data['type'] = $type;
        if ($action == 'asyn') {
            $this->load->view('theme/transactions/edit_transaction', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transactions/edit_transaction', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For Update Transaction *
     */
    public function updateTransaction()
    {
        $data = array();
        $id = $this->input->post('trans_id', true);
        $data['trans_date'] = changeDateFormat($this->input->post('trans_date', true));
        $type = $this->input->post('type', true);
        $data['category'] = $this->input->post('income_expense_type', true);
        $data['amount'] = $this->input->post('amount', true);
        if ($type == TYPE_INCOME) {
            $data['cr'] = $data['amount'];
            $data['payer'] = $this->input->post('payer', true);
        } else {
            $data['dr'] = $data['amount'];
            $data['payee'] = $this->input->post('payee', true);
        }
        $data['p_method'] = $this->input->post('p_method', true);
        $data['ref'] = $this->input->post('reference', true);
        $data['note'] = $this->input->post('note', true);
        $data['bal'] = $this->input->post('bal', true);

        $this->db->where('trans_id', $id);
        $this->db->update('transaction', $data);
        // $this->Adminmodel->updateAccountBalance($id);
        echo "true";
    }

    /**
     * Method For View Repeat Income page *
     */
    public function repeatIncome($action = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(0);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_INCOME, STATUS_ACTIVE);
        $data['payers'] = $this->Adminmodel->getPayerAndPayee('Payer', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);

        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/repeat_income', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/repeat_income', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('income-date', true));
            $data['type'] = TYPE_INCOME;
            $data['category'] = $this->input->post('income-type', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = $this->input->post('payer', true);
            $data['payee'] = '';
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['status'] = 'pending';
            $data['note'] = $this->input->post('note', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('income-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('rotation-income', 'Rotation', 'numeric|required');
            $this->form_validation->set_rules('income-type', 'Income Type', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('payer', 'Payer', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    $increment = $this->input->post('rotation', true);
                    $loop = $this->input->post('rotation-income', true);
                    if ($this->Repeat_transaction_model->insertRepeatTransaction($data, $increment, $loop)) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                }
            } else {
                // echo "All Field Must Required With Valid Length !";
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        }
    }

    /**
     * Method For View repeat Expense Page *
     */
    public function repeatExpense($action = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(0);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType(TYPE_EXPENSE, STATUS_ACTIVE);
        $data['payee'] = $this->Adminmodel->getPayerAndPayee('Payee', STATUS_ACTIVE);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/repeat_expense', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/repeat_expense', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('accounts_name', true);
            $data['trans_date'] = changeDateFormat($this->input->post('expense-date', true));
            $data['type'] = TYPE_EXPENSE;
            $data['category'] = $this->input->post('expense-type', true);
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = '';
            $data['payee'] = $this->input->post('payee', true);
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['status'] = 'unpaid';
            $data['note'] = $this->input->post('note', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('accounts_name', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('expense-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('rotation-expense', 'Rotation', 'numeric|required');
            $this->form_validation->set_rules('expense-type', 'Expense Type', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('payee', 'Payee', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    $increment = $this->input->post('rotation', true);
                    $loop = $this->input->post('rotation-expense', true);
                    if ($this->Repeat_transaction_model->insertRepeatTransaction($data, $increment, $loop)) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                }
            } else {
                // echo "All Field Must Required With Valid Length !";
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        }
    }

    /**
     * Method For View repeat Transfer Page *
     */
    public function repeatTransfer($action = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(0);
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE);
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/repeat_transfer', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/repeat_transfer', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('account-from', true) . '||' . $this->input->post('account-to', true);
            $data['trans_date'] = changeDateFormat($this->input->post('expense-date', true));
            $data['type'] = TYPE_TRANSFER;
            $data['category'] = '';
            $data['amount'] = $this->input->post('amount', true);
            $data['payer'] = '';
            $data['payee'] = '';
            $data['p_method'] = $this->input->post('p_method', true);
            $data['ref'] = $this->input->post('reference', true);
            $data['status'] = 'unpaid';
            $data['note'] = $this->input->post('note', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('account-from', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('account-to', 'Account Name', 'trim|required');
            $this->form_validation->set_rules('expense-date', 'Date', 'trim|required');
            $this->form_validation->set_rules('rotation-transfer', 'Rotation', 'numeric|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
            $this->form_validation->set_rules('p_method', 'Payment Method', 'trim|required');
            $this->form_validation->set_rules('reference', 'Reference No', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    $increment = $this->input->post('rotation', true);
                    $loop = $this->input->post('rotation-transfer', true);
                    if ($this->Repeat_transaction_model->insertRepeatTransaction($data, $increment, $loop)) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                }
            } else {
                // echo "All Field Must Required With Valid Length !";
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        }
    }

    /**
     * Method For view Process Income page *
     */
    public function processIncome($action = '')
    {
        $data = array();
        $repeat_income = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_INCOME, 'pending');
        $data['repeat_transaction'] = $this->Adminmodel->splitTransactionsByMonths($repeat_income);
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/process_repeat_income', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/process_repeat_income', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view Process Expense page *
     */
    public function processExpense($action = '')
    {
        $data = array();
        $repeat_expense = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_EXPENSE, 'unpaid');
        $data['repeat_transaction'] = $this->Adminmodel->splitTransactionsByMonths($repeat_expense);
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/process_repeat_expense', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/process_repeat_expense', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view Process Transfer page *
     */
    public function processTransfer($action = '')
    {
        $data = array();
        $repeat_transfer = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_TRANSFER, 'unpaid');
        $data['repeat_transaction'] = $this->Adminmodel->splitTransactionsByMonths($repeat_transfer);
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/process_repeat_transfer', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/process_repeat_transfer', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For get data for edit repeat transaction *
     */
    public function editRepeatTransaction($type, $trans_id, $action = '')
    {
        $data = array();
        $data['accounts'] = $this->Adminmodel->getAllAccounts(0);
        $data['transaction'] = $this->Repeat_transaction_model->getSingleRepeatTransaction($trans_id);
        $data['category'] = $this->Chart_of_accounts_model->getChartOfAccountByType($type);
        if ($type == TYPE_INCOME) {
            $data['payers'] = $this->Adminmodel->getPayerAndPayee('Payer');
        } else {
            $data['payee'] = $this->Adminmodel->getPayerAndPayee('Payee');
        }
        $data['p_method'] = $this->Payment_method_model->getAllPaymentmethod();
        $data['type'] = $type;
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/edit_repeat_transaction', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/edit_repeat_transaction', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For Update repeat Transaction *
     */
    public function updateRepeatTransaction()
    {
        $data = array();
        $id = $this->input->post('trans_id', true);
        $type = $this->input->post('type', true);
        if ($type == TYPE_TRANSFER) {
            $data['accounts_name'] = implode("||", $this->input->post('accounts_name', true));
        } else {
            $data['accounts_name'] = $this->input->post('accounts_name', true);
        }
        $data['trans_date'] = changeDateFormat($this->input->post('trans_date', true));
        $data['amount'] = $this->input->post('amount', true);
        if ($type == TYPE_TRANSFER) {
            $data['category'] = '';
        } else {
            $data['category'] = $this->input->post('income_expense_type', true);
        }

        if ($type == TYPE_INCOME) {
            $data['payer'] = $this->input->post('payer', true);
        } else if ($type == TYPE_EXPENSE) {
            $data['payee'] = $this->input->post('payee', true);
        }

        $data['p_method'] = $this->input->post('p_method', true);
        $data['ref'] = $this->input->post('reference', true);
        $data['note'] = $this->input->post('note', true);

        $this->db->where('trans_id', $id);
        $this->db->update('repeat_transaction', $data);

        // $edit_amount = $this->input->post('edit-amount', true);
        // if($edit_amount) {
        //     $data = array();
        //     $data['amount'] = $this->input->post('amount', true);
        //     $this->db->where('trans_id >', $id);
        //     $this->db->update('repeat_transaction', $data);
        // }

        echo "true";
    }

    /**
     * Method For view Income Calender page *
     */
    public function incomeCalender($action = '')
    {
        $data = array();
        $data['repeat_income'] = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_INCOME, array('pending', 'receive'));
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/income_calendar', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/income_calendar', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view Expense Calender page *
     */
    public function expenseCalender($action = '')
    {
        $data = array();
        $data['repeat_expense'] = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_EXPENSE, array('unpaid', 'paid'));
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/expense_calendar', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/expense_calendar', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For view Transfer Calender page *
     */
    public function transferCalender($action = '')
    {
        $data = array();
        $data['repeat_transfer'] = $this->Repeat_transaction_model->getRepeatTransaction(TYPE_TRANSFER, array('unpaid', 'paid'));
        if ($action == 'asyn') {
            $this->load->view('theme/recurring_transaction/transfer_calendar', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/recurring_transaction/transfer_calendar', $data);
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For process repeat Transaction *
     */
    public function processRepeatTransaction($action, $trans_id, $status = '')
    {
        if ($action == 'action') {
            if ($this->Adminmodel->processRepeatTransaction($trans_id, $status)) {
                echo "true";
            } else {
                echo "false";
            }
        } else if ($action == 'remove') {
            if ($this->deleteRepeatTransactionById($trans_id)) {
                echo "true";
            } else {
                echo "false";
            }
        }
    }

    /**
     * Delete Repeat Transaction By Id
     */
    public function deleteRepeatTransactionById($id)
    {
        $this->db->delete('repeat_transaction', array(
            'trans_id' => $id
        ));
        if ($this->db->affected_rows() > 0) {
            // Success
            return true;
        } else {
            // Failure
            return false;
        }
    }

    /**
     * Method For View,insert, update and delete Chart of accounts *
     */
    public function chartOfAccounts($action = '', $param1 = '', $status = '')
    {
        $data = array();
        $data['accountList'] = $this->Adminmodel->getAllChartOfAccounts();
        // ----For ajax load-----//
        if ($action == 'asyn') {
            $this->load->view('theme/transaction_settings/chart_of_accounts', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transaction_settings/chart_of_accounts', $data);
            $this->load->view('theme/include/footer');
        }
        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['accounts_name'] = $this->input->post('category_name', true);
            $data['accounts_type'] = $this->input->post('account-type', true);
            $data['user_id'] = $this->session->userdata('user_id');
            // -----Validation-----//
            if ($data['accounts_name'] != "" && $data['accounts_type'] != "" && strlen($data['accounts_name']) <= 30 && strlen($data['accounts_type']) <= 7) {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists2("chart_of_accounts", "accounts_name", $data['accounts_name'], "accounts_type", $data['accounts_type'])) {
                        if ($this->db->insert('chart_of_accounts', $data)) {
                            $last_id = $this->db->insert_id();
                            echo '{"result":"true", "action":"insert", "last_id":"' . $last_id . '"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"This Name Is Already Exists !"}';
                        ;
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('chart_id', true);
                    // Check Duplicate Entry
                    if (!value_exists2("chart_of_accounts", "accounts_name", $data['accounts_name'], "accounts_type", $data['accounts_type'], "chart_id", $id)) {
                        $this->db->where('chart_id', $id);
                        $this->db->update('chart_of_accounts', $data);
                        echo '{"result":"true","action":"update"}';
                    } else {
                        echo '{"result":"false", "message":"This Name Is Already Exists !"}';
                        ;
                    }
                }
            } else {
                echo '{"result":"false", "message":"All Field Must Required With Valid Length !"}';
            }
            // ----End validation----//
        } else if ($action == 'action') {
            $this->db->where('chart_id', $param1);
            $this->db->update('chart_of_accounts', array(
                'status' => $status
            ));
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('chart_of_accounts', array(
            //     'chart_id' => $param1
            // ));
        }
    }

    /**
     * Method For View,insert, update and delete payee And Payers *
     */
    // TODO: Multiple select check box
    public function payeeAndPayers($action = '', $param1 = '', $status = '')
    {
        $data = array();
        $data['p_list'] = $this->Adminmodel->getPayerAndPayee();
        // ----For ajax load-----//
        if ($action == 'asyn') {
            $this->load->view('theme/transaction_settings/Payee_Payers', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transaction_settings/Payee_Payers', $data);
            $this->load->view('theme/include/footer');
        }
        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['payee_payers'] = $this->input->post('p-name', true);
            $data['type'] = $this->input->post('p-type', true);
            $data['user_id'] = $this->session->userdata('user_id');
            // -----Validation-----//
            if ($data['payee_payers'] != "" && $data['type'] != "" && strlen($data['payee_payers']) <= 30 && strlen($data['type']) <= 5) {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists2("payee_payers", "payee_payers", $data['payee_payers'], "type", $data['type'])) {
                        if ($this->db->insert('payee_payers', $data)) {
                            $last_id = $this->db->insert_id();
                            echo '{"result":"true", "action":"insert", "last_id":"' . $last_id . '"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"This Name Is Already Exists !"}';
                        ;
                    }
                } else if ($do == 'update') {
                    // TODO :: Update Transaction table also
                    $id = $this->input->post('trace_id', true);
                    // Check duplicate Entry
                    if (!value_exists2("payee_payers", "payee_payers", $data['payee_payers'], "type", $data['type'], "trace_id", $id)) {
                        $old_name = $this->Payee_payers_model->get_by_id($id);

                        // Start a transaction
                        $this->db->trans_start(); // Begin transaction

                        $this->db->where('trace_id', $id);
                        $this->db->update('payee_payers', $data);

                        $old_name = $old_name->payee_payers;
                        $new_name = $data['payee_payers'];
                        $type = $data['type'];
                        $type = strtolower($type);

                        if ($old_name != $new_name) {
                            // Update the `transaction` table
                            $this->db->set($type, $new_name);
                            $this->db->where($type, $old_name);
                            $this->db->update('transaction');

                            // Update the `repeat_transaction` table with REPLACE function
                            $this->db->set($type, $new_name);
                            $this->db->where($type, $old_name);
                            $this->db->update('repeat_transaction');

                            // Update the `bulk_transaction` table with REPLACE function
                            $this->db->set('tansactions', "REPLACE(tansactions, '{$old_name}', '{$new_name}')", FALSE);
                            $this->db->like('tansactions', $old_name);
                            $this->db->update('bulk_transaction');
                        }

                        // Commit or rollback based on transaction status
                        $this->db->trans_complete(); // End transaction

                        // Check if the transaction was successful
                        if ($this->db->trans_status() === FALSE) {
                            // Transaction failed, rollback
                            $this->db->trans_rollback();
                            // Handle error
                            echo '{"result":"false", "message":"Failed to update"}';
                        } else {
                            // Transaction successful, commit
                            $this->db->trans_commit();
                            echo '{"result":"true", "action":"update"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"This Name Is Already Exists !"}';
                    }
                }
            } else {
                echo '{"result":"false", "message":"All Field Must Required With Valid Length !"}';
            }
            // ----End validation----//
        } else if ($action == 'action') {
            $this->db->where('trace_id', $param1);
            $this->db->update('payee_payers', array(
                'status' => $status
            ));
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('payee_payers', array(
            //     'chart_id' => $param1
            // ));
        }
    }

    /**
     * Method For View,insert, update and delete payment method *
     */
    public function paymentMethod($action = '', $param1 = '', $status = '')
    {
        $data = array();
        $data['p_list'] = $this->Payment_method_model->getAllPaymentmethod();
        // ----For ajax load-----//
        if ($action == 'asyn') {
            $this->load->view('theme/transaction_settings/payment_method', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/transaction_settings/payment_method', $data);
            $this->load->view('theme/include/footer');
        }
        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['p_method_name'] = $this->input->post('p_method', true);
            $data['user_id'] = $this->session->userdata('user_id');
            // -----Validation-----//
            if ($data['p_method_name'] != "" && strlen($data['p_method_name']) <= 20) {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists("payment_method", "p_method_name", $data['p_method_name'])) {
                        if ($this->db->insert('payment_method', $data)) {
                            $last_id = $this->db->insert_id();
                            echo '{"result":"true", "action":"insert", "last_id":"' . $last_id . '"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"This Payment Method Is Already Exists !"}';
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('p_method_id', true);
                    // Check Duplicate Entry
                    if (!value_exists("payment_method", "p_method_name", $data['p_method_name'], "p_method_id", $id)) {
                        $this->db->where('p_method_id', $id);
                        $this->db->update('payment_method', $data);
                        $last_id = $this->db->insert_id();
                        echo '{"result":"true", "action":"update"}';
                    } else {
                        echo '{"result":"false", "message":"This Payment Method Is Already Exists !"}';
                    }
                }
            } else {
                echo '{"result":"false", "message":"All Field Must Required With Valid Length !"}';
            }
            // ----End validation----//
        } else if ($action == 'action') {
            $this->db->where('p_method_id', $param1);
            $this->db->update('payment_method', array(
                'status' => $status
            ));
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('payment_method', array(
            //     'p_method_id' => $param1
            // ));
            // echo '{"result":"true", "action":"remove"}';
        }
    }

    /**
     * Method For User Management page *
     */
    public function userManagement($action = '', $param1 = '')
    {
        checkPermission('User');
        $data = array();
        $data['users'] = $this->Adminmodel->getAllUsers();
        // ----For ajax load-----//
        if ($action == 'asyn') {
            $this->load->view('theme/administration/user_management', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/administration/user_management', $data);
            $this->load->view('theme/include/footer');
        }
        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['user_name'] = $this->input->post('username', true);
            $data['fullname'] = $this->input->post('fullname', true);
            $data['email'] = $this->input->post('email', true);
            $data['user_type'] = $this->input->post('user-type', true);
            $data['password'] = $this->input->post('user-password', true);
            $data['creation_date'] = date(DATETIME_FORMAT_DB);

            // -----Validation-----//
            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[15]');
            $this->form_validation->set_rules('user-password', 'Password', 'trim|required|min_length[5]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('fullname', 'FullName', 'trim|required|min_length[6]|max_length[30]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    // Check Duplicate Entry
                    if (!value_exists("user", "user_name", $data['user_name'])) {
                        if (!value_exists("user", "email", $data['email'])) {
                            $data['password'] = md5($data['password']);
                            if ($this->db->insert('user', $data)) {
                                $last_id = $this->db->insert_id();
                                echo '{"result":"true", "action":"insert", "last_id":"' . $last_id . '"}';
                            }
                        } else {
                            echo '{"result":"false", "message":"This Email Is Already Exists !"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"Username Is Already Exists !"}';
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('user_id', true);
                    if (!value_exists("user", "user_name", $data['user_name'], "user_id", $id)) {
                        if (!value_exists("user", "email", $data['email'], "user_id", $id)) {
                            // if password change
                            if (strlen($this->input->post('user-password', true)) <= 15) {
                                $data['password'] = md5($data['password']);
                            }
                            $this->db->where('user_id', $id);
                            $this->db->update('user', $data);
                            echo '{"result":"true", "action":"update"}';
                        } else {
                            echo '{"result":"false", "message":"This Email Is Already Exists !"}';
                        }
                    } else {
                        echo '{"result":"false", "message":"Username Is Already Exists !"}';
                    }
                }
            } else {

                echo json_encode(validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>'));
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('user', array(
            //     'user_id' => $param1
            // ));
        }
    }

    /**
     * Method For User Management page *
     */
    public function syncAccount($action = '', $param1 = '')
    {
        checkPermission('User');
        $data = array();

        // ----For sync -----//
        if ($action == 'sync') {
            $trans_id = $this->input->post('trans_id', true);
            echo $this->Adminmodel->updateAccountBalance($trans_id);
        } else if ($action == 'remove') {
            // $this->db->delete('transaction', array(
            //     'trans_id' => $param1
            // ));
        }
    }

    /**
     * Method For get user information for edit *
     */
    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where("user_id", $user_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        echo json_encode($result);
    }

    /**
     * Method For add new language *
     */
    public function addLanguage($action = '')
    {
        checkPermission('User');
        if ($action == 'asyn') {
            $this->load->view('theme/administration/add_language');
        } else {
            $this->load->view('theme/include/header');
            $this->load->view('theme/administration/add_language');
            $this->load->view('theme/include/footer');
        }
    }

    /**
     * Method For general settings page *
     */
    public function generalSettings($action = '')
    {
        checkPermission('User');
        $data = array();
        $data['settings'] = $this->Adminmodel->getAllSettings();
        $data['timezone'] = timezone_list();
        if ($action == 'asyn') {
            $this->load->view('theme/administration/General_settings', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/administration/General_settings', $data);
            $this->load->view('theme/include/footer');
        }

        // update general Settings
        if ($action == 'update') {
            $data = array(
                array(
                    'settings' => 'company_name',
                    'value' => $this->input->post("company-name", true)
                ),
                array(
                    'settings' => 'language',
                    'value' => $this->input->post("language", true)
                ),
                array(
                    'settings' => 'currency_code',
                    'value' => $this->input->post("cur-symbol", true)
                ),
                array(
                    'settings' => 'email_address',
                    'value' => $this->input->post("email", true)
                ),
                array(
                    'settings' => 'address',
                    'value' => $this->input->post("address", true)
                ),
                array(
                    'settings' => 'phone',
                    'value' => $this->input->post("phone", true)
                ),
                array(
                    'settings' => 'website',
                    'value' => $this->input->post("website", true)
                ),
                array(
                    'settings' => 'timezone',
                    'value' => $this->input->post("timezone", true)
                ),
                array(
                    'settings' => 'info_upto_date',
                    'value' => $this->input->post("upto-date", true)
                )
            );
            // -----Validation-----//
            $this->form_validation->set_rules('company-name', 'Company Name', 'trim|required|min_length[2]|max_length[50]');
            if (!$this->form_validation->run() == FALSE) {
                // Update Code
                $this->db->update_batch('settings', $data, 'settings');
                echo "true";
                // Finish Update Code
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
        }
    }

    /**
     * Method For upload new logo *
     */
    public function uploadLogo()
    {
        checkPermission('User');
        $config['upload_path'] = './uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload("logo")) {
            echo $this->upload->display_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
        } else {
            $data = array(
                'upload_data' => $this->upload->data()
            );
            $object = array();
            $object['value'] = $data['upload_data']['file_name'];
            $this->db->where('settings', 'logo_path');
            if ($this->db->update('settings', $object)) {
                echo "true";
            }
        }
    }

    /**
     * Method For backup database *
     */
    public function backupDatabase($action = '', $table = '')
    {
        checkPermission('User');
        if ($action == 'asyn') {
            $this->load->view('theme/administration/backup_database');
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/administration/backup_database');
            $this->load->view('theme/include/footer');
        }

        if ($action == 'backup') {
            $this->load->model('Datamodel');
            $this->Datamodel->backup($table);
        }
        /*
         * else if($action=='delete'){
         * $this->load->model('Datamodel');
         * $this->Datamodel->truncate($table);
         * }
         */
    }

    /**
     * Method For update profile *
     */
    public function updateProfile()
    {
        $data = array();
        // -----Validation-----//
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[15]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('fullname', 'FullName', 'trim|required|min_length[6]|max_length[30]');

        if (!$this->form_validation->run() == FALSE) {
            $data['user_name'] = $this->input->post('username', true);
            $data['fullname'] = $this->input->post('fullname', true);
            $data['email'] = $this->input->post('email', true);

            $id = $this->session->userdata('user_id');
            if (!value_exists("user", "user_name", $data['user_name'], "user_id", $id)) {
                if (!value_exists("user", "email", $data['email'], "user_id", $id)) {
                    $this->db->where('user_id', $id);
                    $this->db->update('user', $data);
                    // update session
                    $this->session->set_userdata('username', $data['user_name']);
                    $this->session->set_userdata('fullname', $data['fullname']);
                    $this->session->set_userdata('email', $data['email']);

                    echo "true";
                } else {
                    echo "Sorry, This Email Is Already Exists !";
                }
            } else {
                echo "Sorry, Username Is Already Exists !";
            }
        } else {
            // echo validation_errors('<span class="ion-android-alert failedAlert2"> ','</span>');
            echo "All Field Must Required With Valid Information !";
        }
    }

    /**
     * Method For change password *
     */
    public function changePassword()
    {
        $this->form_validation->set_rules('new-password', 'Password', 'trim|required|min_length[5]');
        if (!$this->form_validation->run() == FALSE) {
            $data = array();
            $data['password'] = md5($this->input->post('new-password', true));
            $cp = md5($this->input->post('confrim-password', true));
            if ($data['password'] != $cp) {
                echo "New Password And Confrim Password Has Not Match !";
            } else {
                // update Password
                $id = $this->session->userdata('user_id');
                $this->db->where('user_id', $id);
                $this->db->update('user', $data);
                echo "true";
            }
        } else {
            echo "The Password field must be at least 5 characters in length";
        }
    }

    public function inforUptoDate()
    {
        $this->form_validation->set_rules('upto-date', 'Date', 'trim|required');
        if (!$this->form_validation->run() == FALSE) {
            $data = array();
            $data['value'] = $this->input->post('upto-date', true);
            $this->db->where('settings', "info_upto_date");
            $this->db->update('settings', $data);
            echo true;
            return;
        }
        echo 'Provide date';
    }

    public function addTask($action = '', $param1 = '')
    {
        $data = array();
        $data['tasks'] = $this->Adminmodel->getAllTasks(40);
        // echo '<pre>'; print_r($data['tasks']);echo '</pre>';
        $data['tasksorder'] = group_result_by_key("status", $data['tasks']);
        // echo '<pre>'; print_r($data['tasksorder']);echo '</pre>';
        $data['tasksorder'] = array_merge(array_flip(TASK_STATUS), $data['tasksorder']);
        // echo '<pre>'; print_r($data['tasksorder']);echo '</pre>';
        if ($action == 'asyn') {
            $this->load->view('theme/tasks/add_task', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/tasks/add_task', $data);
            $this->load->view('theme/include/footer');
        }

        // ----End Page Load------//
        // ----For Insert update and delete-----//
        if ($action == 'insert') {
            $data = array();
            $do = $this->input->post('action', true);
            $data['startdate'] = $this->input->post('startdate', true);
            $data['duedate'] = $this->input->post('duedate', true);
            $data['priority'] = $this->input->post('priority', true);
            $data['status'] = $this->input->post('status', true);
            $data['note'] = $this->input->post('note', true);
            $data['datefinished'] = $this->input->post('datefinished', true);
            $data['user_id'] = $this->session->userdata('user_id');

            // -----Validation-----//
            $this->form_validation->set_rules('startdate', 'startdate', 'trim|required');
            $this->form_validation->set_rules('duedate', 'duedate', 'trim|required');
            $this->form_validation->set_rules('priority', 'priority', 'trim|required');
            $this->form_validation->set_rules('status', 'status', 'trim|required');
            $this->form_validation->set_rules('note', 'Note', 'trim|required|min_length[8]');

            if (!$this->form_validation->run() == FALSE) {
                if ($do == 'insert') {
                    if ($this->db->insert('tasks', $data)) {
                        echo "true";
                    }
                } else if ($do == 'update') {
                    $id = $this->input->post('task_id', true);
                    $this->db->where('task_id', $id);
                    $this->db->update('tasks', $data);
                    echo "true";
                }
            } else {
                echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
            }
            // ----End validation----//
        } else if ($action == 'remove') {
            // TODO: Need to do
            // $this->db->delete('tasks', array(
            //     'task_id' => $param1
            // ));
        }
    }

    public function changeTaskStatus()
    {
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        $this->form_validation->set_rules('task_id', 'Task ID', 'trim|required');

        if (!$this->form_validation->run() == FALSE) {
            $data = array();
            $data['status'] = $this->input->post('status', true);
            $id = $this->input->post('task_id', true);
            $this->db->where('task_id', $id);
            $this->db->update('tasks', $data);
            echo "true";
        } else {
            echo validation_errors('<span class="ion-android-alert failedAlert2"> ', '</span>');
        }
    }

    public function editTask($task_id, $action = '')
    {
        $data = array();
        $data['task'] = $this->Adminmodel->getSingleTask($task_id);
        if ($action == 'asyn') {
            $this->load->view('theme/tasks/edit_task', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/tasks/edit_task', $data);
            $this->load->view('theme/include/footer');
        }
    }

    // TODO: Functionality check
    // FIXME: Add / Edit task
    public function manageTask($action = '')
    {
        $data = array();
        if ($action == 'asyn') {
            $this->load->view('theme/tasks/manage_task_ajax', $data);
        } else if ($action == '') {
            $this->load->view('theme/include/header');
            $this->load->view('theme/tasks/manage_task_ajax', $data);
            $this->load->view('theme/include/footer');
        }
    }

    public function manageTaskAjax()
    {
        //output to json format
        echo json_encode($this->Adminmodel->getTaskAjax());
    }

    /**
     * This is the API for Swap Transaction
     * Income / Expense
     */
    public function swapTransactionAjax()
    {
        $curr_trans_id = $this->input->post('curr_trans_id', true);
        $account_to = $this->input->post('account_to', true);
        echo $this->Adminmodel->swapTransaction($curr_trans_id, $account_to);
    }

    public function manageVehicles($action = 'view', $param1 = null)
    {
        $data = array();
        if ($action == 'view') {
            $p_list = $this->Vehicle_model->get_all();
            $data['table_list'] = $this->Displaymodel->getVehiclesListTable($p_list);
            $this->load->view('theme/include/header');
            $this->load->view('theme/vehicles/manage_vehicles', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'addForm') {
            if (!is_null($param1)) {
                $data['single'] = $this->Vehicle_model->get_by_id($param1);
            }
            $content = $this->load->view('theme/vehicles/add_vehicle', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Edit Vehicle" : 'Add Vehicle'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'insert') {
            // Set validation rules
            $this->form_validation->set_rules('action', 'Action', 'required');
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('type', 'Type', 'required');
            $this->form_validation->set_rules('registration_no', 'Registration No.', 'required');

            if ($this->form_validation->run() == FALSE) {
                // If validation fails, return error message
                $response = array(
                    'status' => 'error',
                    'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>'
                );
            } else {
                $action = $this->input->post('action');
                $vehicle = [
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'registration_no' => $this->input->post('registration_no'),
                    'fuel_liters' => $this->input->post('fuel_liters'),
                    'month_year' => $this->input->post('month_year'),
                    'note' => $this->input->post('note')
                ];
                if ($action == 'insert') {
                    $inserted = $this->Vehicle_model->insert($vehicle);
                } else {
                    $vehicle_id = $this->input->post('vehicle_id');
                    $inserted = $this->Vehicle_model->update($vehicle_id, $vehicle);
                }
                // If validation passes, process the data
                if (isset($inserted) && $inserted) {
                    // Return success message
                    $response = array(
                        'status' => 'success',
                        'message' => '<div class="alert alert-success" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Vehicle added successfully!
                        </div>'
                    );
                } else {
                    // Return error message if insertion failed
                    $response = array(
                        'status' => 'error',
                        'message' => '<div class="alert alert-danger" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                        </div>'
                    );
                }
            }

            // Send JSON response
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }

    public function manageTrips($action = 'view', $param1 = null)
    {
        $data = array();
        if ($action == 'view') {
            $v_list = $this->Vehicle_model->get_all();
            $v_list_obj = $this->Adminmodel->resultsObjectToArrayObject($v_list, 'vehicle_id');
            $p_list = $this->Vehicle_trip_model->get_all_limit(7);
            $p_list = $this->Vehicle_trip_model->get_vehicle_trips_with_details($p_list, $v_list_obj);
            $data['table_list'] = $this->Displaymodel->getTripsListTables($p_list);
            $this->load->view('theme/include/header');
            $this->load->view('theme/vehicles/manage_trips', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'addForm') {
            $vehicles = $this->Vehicle_model->get_all();
            $data['vehicles'] = $this->Adminmodel->resultsObjectToArray($vehicles, 'vehicle_id', 'name');
            $data['formaction'] = 'insert';
            if (!is_null($param1)) {
                $data['single'] = $this->Vehicle_trip_model->get_by_id($param1);
                $data['formaction'] = 'update';
            }
            $content = $this->load->view('theme/vehicles/add_trip', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Edit Trip" : 'Add Trip'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'copy') {
            $vehicles = $this->Vehicle_model->get_all();
            $data['vehicles'] = $this->Adminmodel->resultsObjectToArray($vehicles, 'vehicle_id', 'name');
            $data['formaction'] = 'insert';
            if (!is_null($param1)) {
                $data['single'] = $this->Vehicle_trip_model->get_by_id($param1);
            }
            $content = $this->load->view('theme/vehicles/add_trip', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Copy Trip" : 'Add Trip'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'insert') {
            // Set validation rules
            $this->form_validation->set_rules('action', 'Action', 'required');
            $this->form_validation->set_rules('vehicle_id', 'vehicle_id', 'required');
            $this->form_validation->set_rules('trip_date', 'trip_date', 'required');
            $this->form_validation->set_rules('meter_reading', 'meter_reading', 'required');
            $this->form_validation->set_rules('note', 'note', 'required');

            if ($this->form_validation->run() == FALSE) {
                // If validation fails, return error message
                $response = array(
                    'status' => 'error',
                    'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>'
                );
            } else {
                $action = $this->input->post('action');
                $s_data = [
                    'vehicle_id' => $this->input->post('vehicle_id'),
                    'trip_date' => $this->input->post('trip_date'),
                    'meter_reading' => $this->input->post('meter_reading'),
                    'fuel_percentage' => $this->input->post('fuel_percentage'),
                    'note' => $this->input->post('note')
                ];
                if ($action == 'insert') {
                    $inserted = $this->Vehicle_trip_model->insert($s_data);
                } else {
                    $trip_id = $this->input->post('trip_id');
                    $inserted = $this->Vehicle_trip_model->update($trip_id, $s_data);
                }
                // If validation passes, process the data
                if (isset($inserted) && $inserted) {
                    // Return success message
                    $response = array(
                        'status' => 'success',
                        'message' => '<div class="alert alert-success" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Trip added successfully!
                        </div>'
                    );
                } else {
                    // Return error message if insertion failed
                    $response = array(
                        'status' => 'error',
                        'message' => '<div class="alert alert-danger" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                        </div>'
                    );
                }
            }

            // Send JSON response
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }

    public function manageFuels($action = 'view', $param1 = null)
    {
        $data = array();
        if ($action == 'view') {
            $v_list = $this->Vehicle_model->get_all();
            $v_list_obj = $this->Adminmodel->resultsObjectToArrayObject($v_list, 'vehicle_id');
            $f_list = $this->Vehicle_fuels_model->get_all();
            if (!empty($f_list)) {
                $tripIdArray = $this->Adminmodel->resultsObjectToIdArray($f_list, 'trip_id');
                $t_list = $this->Vehicle_trip_model->getTripByTripIDs($tripIdArray);
                $t_list_obj = $this->Adminmodel->resultsObjectToArrayObject($t_list, 'trip_id');
                $f_list = $this->Vehicle_fuels_model->get_vehicle_fuels_with_details($f_list, $v_list_obj, $t_list_obj);
            }
            $data['table_list'] = $this->Displaymodel->getVehiclesFuelsListTable($f_list);
            $this->load->view('theme/include/header');
            $this->load->view('theme/vehicles/manage_fuels', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'addForm') {
            $vehicles = $this->Vehicle_model->get_all();
            $data['vehicles'] = $this->Adminmodel->resultsObjectToArray($vehicles, 'vehicle_id', 'name');
            if (!is_null($param1)) {
                $single = $this->Vehicle_fuels_model->get_by_id($param1);
                $trip = $this->Vehicle_trip_model->get_by_id($single->trip_id);
                $single->vehicle_id = $trip->vehicle_id;
                $single->fill_date = $trip->trip_date;
                $single->meter_reading = $trip->meter_reading;
                $single->fuel_percentage = $trip->fuel_percentage;
                $single->note = $trip->note;
                $data['single'] = $single;
            }
            $content = $this->load->view('theme/vehicles/add_fuel', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Edit Fuel" : 'Add Fuel'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'insert') {
            // Set validation rules
            $this->form_validation->set_rules('action', 'Action', 'required');
            $this->form_validation->set_rules('vehicle_id', 'vehicle_id', 'required');
            $this->form_validation->set_rules('fill_date', 'fill_date', 'required');
            $this->form_validation->set_rules('meter_reading', 'meter_reading', 'required');
            $this->form_validation->set_rules('fuel_liters', 'fuel_liters', 'required');
            $this->form_validation->set_rules('fuel_price', 'fuel_price', 'required');
            $this->form_validation->set_rules('note', 'Note', 'required');

            if ($this->form_validation->run() == FALSE) {
                // If validation fails, return error message
                $response = array(
                    'status' => 'error',
                    'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>'
                );
            } else {
                $this->db->trans_begin();
                $action = $this->input->post('action');
                $f_data = [
                    'fuel_liters' => $this->input->post('fuel_liters'),
                    'fuel_price' => $this->input->post('fuel_price')
                ];
                if ($action == 'insert') {
                    $t_data = [
                        'vehicle_id' => $this->input->post('vehicle_id'),
                        'trip_date' => $this->input->post('fill_date'),
                        'meter_reading' => $this->input->post('meter_reading'),
                        'fuel_percentage' => $this->input->post('fuel_percentage'),
                        'note' => $this->input->post('note')
                    ];
                    $this->Vehicle_trip_model->insert($t_data);
                    $f_data['trip_id'] = $this->db->insert_id();
                    $this->Vehicle_fuels_model->insert($f_data);
                } else {
                    $fuel_id = $this->input->post('fuel_id');
                    $fuel = $this->Vehicle_fuels_model->get_by_id($fuel_id);
                    $t_data = [
                        'vehicle_id' => $this->input->post('vehicle_id'),
                        'trip_date' => $this->input->post('fill_date'),
                        'meter_reading' => $this->input->post('meter_reading'),
                        'fuel_percentage' => $this->input->post('fuel_percentage'),
                        'note' => $this->input->post('note')
                    ];
                    $this->Vehicle_trip_model->update($fuel->trip_id, $t_data);

                    $this->Vehicle_fuels_model->update($fuel_id, $f_data);
                }

                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    // Return error message if insertion failed
                    $response = array(
                        'status' => 'error',
                        'message' => '<div class="alert alert-danger" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                        </div>'
                    );
                } else {
                    // Return success message
                    $response = array(
                        'status' => 'success',
                        'message' => '<div class="alert alert-success" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Fuel added successfully!
                        </div>'
                    );
                    $this->db->trans_commit();
                }
            }

            // Send JSON response
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }

    public function manageServices($action = 'view', $param1 = null)
    {
        $data = array();
        if ($action == 'view') {
            $v_list = $this->Vehicle_model->get_all();
            $v_list_obj = $this->Adminmodel->resultsObjectToArrayObject($v_list, 'vehicle_id');
            $p_list = $this->Vehicle_service_model->get_all();
            if (!empty($p_list)) {
                $tripIdArray = $this->Adminmodel->resultsObjectToIdArray($p_list, 'trip_id');
                $t_list = $this->Vehicle_trip_model->getTripByTripIDs($tripIdArray);
                $t_list_obj = $this->Adminmodel->resultsObjectToArrayObject($t_list, 'trip_id');
                $p_list = $this->Vehicle_service_model->get_vehicle_services_with_details($p_list, $v_list_obj, $t_list_obj);
            }
            $data['table_list'] = $this->Displaymodel->getVehicleServiceListTable($p_list);
            $this->load->view('theme/include/header');
            $this->load->view('theme/vehicles/manage_services', $data);
            $this->load->view('theme/include/footer');
        } else if ($action == 'addForm') {
            $vehicles = $this->Vehicle_model->get_all();
            $data['vehicles'] = $this->Adminmodel->resultsObjectToArray($vehicles, 'vehicle_id', 'name');
            if (!is_null($param1)) {
                $single = $this->Vehicle_service_model->get_by_id($param1);
                $trip = $this->Vehicle_trip_model->get_by_id($single->trip_id);
                $single->vehicle_id = $trip->vehicle_id;
                $single->service_date = $trip->trip_date;
                $single->meter_reading = $trip->meter_reading;
                $single->fuel_percentage = $trip->fuel_percentage;
                $single->note = $trip->note;
                $data['single'] = $single;
            }
            $content = $this->load->view('theme/vehicles/add_service', $data, true);
            $response = [
                "success" => true,
                "title" => (!is_null($param1) ? "Edit Service" : 'Add Service'),
                "content" => $content
            ];

            echo json_encode($response);
        } else if ($action == 'insert') {
            // Set validation rules
            $this->form_validation->set_rules('action', 'Action', 'required');
            $this->form_validation->set_rules('vehicle_id', 'Vehicle Id', 'required');
            $this->form_validation->set_rules('service_type', 'Service Type', 'required');
            $this->form_validation->set_rules('service_date', 'Service Date', 'required');
            $this->form_validation->set_rules('meter_reading', 'Meter Reading', 'required');
            $this->form_validation->set_rules('note', 'Note', 'required');

            if ($this->form_validation->run() == FALSE) {
                // If validation fails, return error message
                $response = array(
                    'status' => 'error',
                    'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>'
                );
            } else {
                $this->db->trans_begin();
                $action = $this->input->post('action');
                $s_data = [
                    'service_type' => $this->input->post('service_type')
                ];
                if ($action == 'insert') {
                    $t_data = [
                        'vehicle_id' => $this->input->post('vehicle_id'),
                        'trip_date' => $this->input->post('service_date'),
                        'meter_reading' => $this->input->post('meter_reading'),
                        'fuel_percentage' => $this->input->post('fuel_percentage'),
                        'note' => $this->input->post('note')
                    ];
                    $this->Vehicle_trip_model->insert($t_data);
                    $s_data['trip_id'] = $this->db->insert_id();
                    $this->Vehicle_service_model->insert($s_data);
                } else {
                    $service_id = $this->input->post('service_id');
                    $service = $this->Vehicle_service_model->get_by_id($service_id);
                    $t_data = [
                        'vehicle_id' => $this->input->post('vehicle_id'),
                        'trip_date' => $this->input->post('service_date'),
                        'meter_reading' => $this->input->post('meter_reading'),
                        'fuel_percentage' => $this->input->post('fuel_percentage'),
                        'note' => $this->input->post('note')
                    ];
                    $this->Vehicle_trip_model->update($service->trip_id, $t_data);
                    $this->Vehicle_service_model->update($service_id, $s_data);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    // Return error message if insertion failed
                    $response = array(
                        'status' => 'error',
                        'message' => '<div class="alert alert-danger" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Failed to insert data!
                        </div>'
                    );
                } else {
                    // Return success message
                    $response = array(
                        'status' => 'success',
                        'message' => '<div class="alert alert-success" role="alert">
                        <i class="bi bi-check-circle me-2"></i> Service added successfully!
                        </div>'
                    );
                    $this->db->trans_commit();
                }
            }

            // Send JSON response
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($response));
        }
    }
}
