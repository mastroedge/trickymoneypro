<?php

class Transaction
{
    private $trans_id;
    private $accounts_name;
    private $trans_date;
    private $type;
    private $category;
    private $amount;
    private $payer;
    private $payee;
    private $p_method;
    private $ref;
    private $note;
    private $dr;
    private $cr;
    private $bal;
    private $user_id;

    // Constructor to initialize values
    public function __construct($trans_id = null, $accounts_name = null, $trans_date = null, $type = null, $category = null, $amount = null, $payer = null, $payee = null, $p_method = null, $ref = null, $note = null, $dr = null, $cr = null, $bal = null, $user_id = null)
    {
        $this->trans_id = $trans_id;
        $this->accounts_name = $accounts_name;
        $this->trans_date = $trans_date;
        $this->type = $type;
        $this->category = $category;
        $this->amount = $amount;
        $this->payer = $payer;
        $this->payee = $payee;
        $this->p_method = $p_method;
        $this->ref = $ref;
        $this->note = $note;
        $this->dr = $dr;
        $this->cr = $cr;
        $this->bal = $bal;
        $this->user_id = $user_id;
    }

    // Method to convert the object to an associative array
    public function toArray()
    {
        return get_object_vars($this);
    }

    // Getters
    public function getTransId()
    {
        return $this->trans_id;
    }

    public function getAccountsName()
    {
        return $this->accounts_name;
    }

    public function getTransDate()
    {
        return $this->trans_date;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getPayer()
    {
        return $this->payer;
    }

    public function getPayee()
    {
        return $this->payee;
    }

    public function getPMethod()
    {
        return $this->p_method;
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function getDr()
    {
        return $this->dr;
    }

    public function getCr()
    {
        return $this->cr;
    }

    public function getBal()
    {
        return $this->bal;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    // Setters
    public function setTransId($trans_id)
    {
        $this->trans_id = $trans_id;
    }

    public function setAccountsName($accounts_name)
    {
        $this->accounts_name = $accounts_name;
    }

    public function setTransDate($trans_date)
    {
        $this->trans_date = $trans_date;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setPayer($payer)
    {
        $this->payer = $payer;
    }

    public function setPayee($payee)
    {
        $this->payee = $payee;
    }

    public function setPMethod($p_method)
    {
        $this->p_method = $p_method;
    }

    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    public function setNote($note)
    {
        $this->note = $note;
    }

    public function setDr($dr)
    {
        $this->dr = $dr;
    }

    public function setCr($cr)
    {
        $this->cr = $cr;
    }

    public function setBal($bal)
    {
        $this->bal = $bal;
    }

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
}
