<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('TYPE_INCOME')         or define('TYPE_INCOME', 'Income');
defined('TYPE_EXPENSE')        or define('TYPE_EXPENSE', 'Expense');
defined('TYPE_TRANSFER')       or define('TYPE_TRANSFER', 'Transfer');

defined('AMOUNT_CR')             or define('AMOUNT_CR', 'cr');
defined('AMOUNT_DR')             or define('AMOUNT_DR', 'dr');

defined('MIN_BALANCE')         or define('MIN_BALANCE', 500);
defined('SHOW_CURRENCY')       or define('SHOW_CURRENCY', true);
defined('TASK_STATUS')         or define('TASK_STATUS', array("Pending", "Partial", "Complete"));

defined('ACT_TYPE_ACT')        or define('ACT_TYPE_ACT', 1);
defined('ACT_TYPE_DIS_DIS')    or define('ACT_TYPE_DIS_DIS', 2);
defined('ACT_TYPE_DIS')        or define('ACT_TYPE_DIS', 0);
defined('ACT_TYPE_DEL')        or define('ACT_TYPE_DEL', 3);

defined('STATUS_ACTIVE')        or define('STATUS_ACTIVE', 1);
defined('STATUS_DISABLE')        or define('STATUS_DISABLE', 0);

defined('DATE_ONLY_FORMAT')        or define('DATE_ONLY_FORMAT', 'Y-m-d');
defined('DATETIME_FORMAT_DB')        or define('DATETIME_FORMAT_DB', 'Y-m-d H:i:s');
defined('DATETIME_FORMAT_UI')        or define('DATETIME_FORMAT_UI', 'Y-m-d h:i K');
defined('DATETIME_FORMAT_PHP')        or define('DATETIME_FORMAT_PHP', 'Y-m-d h:i A');

defined('REF_MAX_LEN')        or define('REF_MAX_LEN', 64);

defined('DEFAULT_ACCOUNT_NAME')        or define('DEFAULT_ACCOUNT_NAME', 'BANK - SBI - 6745');
defined('DEFAULT_PAYMENT_METHOD')        or define('DEFAULT_PAYMENT_METHOD', 'Paytm');
defined('DEFAULT_PAYEE')        or define('DEFAULT_PAYEE', 'Walk-in');
defined('DEFAULT_PAYER')        or define('DEFAULT_PAYER', 'Anonymous');
