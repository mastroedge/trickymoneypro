<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?=$this->Adminmodel->add_breadcrumb('Report Viewer');?>
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Transact Report</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-5 col-lg-5 col-sm-12">
                                    <div class="preloader"><img src="<?php echo base_url() ?>theme/images/ring.gif"></div>
                                    <?php if (count($income_expense) > 0) { ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th colspan="5">Balance Sheet</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th class="col-amount">Expense</th>
                                                    <th class="col-amount">Income</th>
                                                    <th class="col-amount">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($income_expense as $key => $value) { ?>
                                                    <tr>
                                                        <td><?= $key + 1 ?></td>
                                                        <td><?= $value['name'] ?></td>
                                                        <td class="text-end">
                                                            <?php if ($value['expense'] > 0) { ?><a onclick="viewTransact('<?= $value['name'] ?>', 'Expense')"
                                                                    class="aicon no-margin"> <?php } ?>
                                                                <?= displayMoney($value['expense']) ?></td>
                                                        <?php if ($value['expense'] > 0) { ?></a><?php } ?>
                                                        </td>
                                                        <td class="text-end">
                                                            <?php if ($value['income'] > 0) { ?><a onclick="viewTransact('<?= $value['name'] ?>', 'Income')"
                                                                    class="aicon no-margin"> <?php } ?>
                                                                <?= displayMoney($value['income']) ?></td>
                                                        <?php if ($value['income'] > 0) { ?></a><?php } ?>
                                                        </td>
                                                        <td class="text-end"><?= displayMoney($value['amount']) ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                                <div id="show-report" class="col-md-7 col-lg-7 col-sm-12">
                                </div>
                            </div>
                        </div>
                        <!--End Panel Body-->
                    </div>


                    <!--End Panel-->
                </div>
            </div>
            <!--End Inner container-->
        </div>
        <!--End Row-->
    </div>
    <!--End Main-content DIV-->
</section>
<!--End Main-content Section-->


<script type="text/javascript">
    function viewTransact(name, type) {
        //query data
        $.ajax({
            method: "POST",
            url: "<?php echo site_url('Reports/transactReport/view') ?>",
            data: {
                type: type,
                name: name
            },
            beforeSend: function() {
                $(".preloader").css("display", "block");
            },
            success: function(data) {
                $(".preloader").css("display", "none");
                if (data != "false") {
                    $("#show-report").html(data);
                } else {
                    $("#show-report").html("Error");
                }
            }

        });
    }
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_income_expense.js"></script>