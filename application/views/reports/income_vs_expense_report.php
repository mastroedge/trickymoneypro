<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Report Viewer'); ?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Income VS Expense Report</div>
                        <div class="panel-body">
                            <form id="transfer-report" action="<?php echo site_url('Reports/incomeVsExpense/view') ?>">

                                <div class="report-params">
                                    <div class="row m-0">
                                        <div class="col-md-2 col-lg-2 col-sm-2 col-6">
                                            <div class="mb-3">
                                                <div class='input-group date' id='date'>
                                                    <input type="text" class="form-control dateonly" placeholder="Date From" name="from-date" id="from-date" value="<?= displayDate('thisMonthFirstDay', DATE_ONLY_FORMAT) ?>" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-lg-2 col-sm-2 col-6">
                                            <div class="mb-3">
                                                <div class='input-group date' id='todate'>
                                                    <input type="text" class="form-control dateonly" placeholder="Date To" name="to-date" id="to-date" value="<?= displayDate('today', DATE_ONLY_FORMAT) ?>" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-lg-2 col-sm-2 col-3">
                                            <select class="form-control" name="order" id="order">
                                                <option value="ASC">ASC</option>
                                                <option value="DESC">DESC</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-sm-2 col-5">
                                            <input type="checkbox" name="check-balance" value="1"> Show balance
                                        </div>
                                    </div>

                                    <div class="row m-0">
                                        <div class="col-md-4 col-lg-4 col-sm-4 col-6">
                                            <div class="mb-3">
                                                <select class="select2sel form-control" name="income_account" id="income_account">
                                                    <option value="">All Income Accounts</option>
                                                    <?php foreach ($accounts as $account) { ?>
                                                        <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-lg-3 col-sm-3 col-6">
                                            <div class="mb-3">
                                                <select class="select2sel form-control" name="payer" id="payer">
                                                    <option value="">All Payers</option>
                                                    <?php foreach ($payerList as $list) { ?>
                                                        <option value="<?php echo $list->payee_payers ?>"><?php echo $list->payee_payers ?> </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5 col-lg-5 col-sm-5">
                                            <div class="mb-3 d-inline-block">
                                                <input type="checkbox" name="check-income" value="1" checked="checked">
                                            </div>
                                            <div class="mb-3 d-inline-block w-90">
                                                <select class="select2sel form-control" name="cat-income[]" multiple="multiple">
                                                    <?php foreach ($accountList as $key => $act) {
                                                        if ($key == TYPE_INCOME) { ?>
                                                            <optgroup label="<?= $key ?>">
                                                                <?php foreach ($act as $account) { ?>
                                                                    <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?> (<?php echo $account->accounts_type ?>)</option>
                                                                <?php } ?>
                                                            </optgroup>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row m-0">
                                        <div class="col-md-4 col-lg-4 col-sm-4 col-6">
                                            <div class="mb-3">
                                                <select class="select2sel form-control" name="expense_account" id="expense_account">
                                                    <option value="">All Expense Accounts</option>
                                                    <?php foreach ($accounts as $account) { ?>
                                                        <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3 col-6">
                                            <div class="mb-3">
                                                <select class="select2sel form-control" name="payee" id="payee">
                                                    <option value="">All Payees</option>
                                                    <?php foreach ($payeeList as $list) { ?>
                                                        <option value="<?php echo $list->payee_payers ?>"><?php echo $list->payee_payers ?> </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-5 col-lg-5 col-sm-5">
                                            <div class="mb-3 d-inline-block">
                                                <input type="checkbox" name="check-expense" value="1" checked="checked">
                                            </div>
                                            <div class="mb-3 d-inline-block w-90">
                                                <select class="select2sel form-control" name="cat-expense[]" multiple="multiple">
                                                    <?php foreach ($accountList as $key => $act) {
                                                        if ($key == TYPE_EXPENSE) { ?>
                                                            <optgroup label="<?= $key ?>">
                                                                <?php foreach ($act as $account) { ?>
                                                                    <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?> (<?php echo $account->accounts_type ?>)</option>
                                                                <?php } ?>
                                                            </optgroup>
                                                    <?php }
                                                    } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-1 col-lg-1 col-sm-1">
                                        <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-play"></i></button>
                                    </div>
                                </div>
                            </form>
                            <!-- <div class="Report-Toolbox col-md-6 col-lg-6 col-sm-6 col-md-offset-6 col-lg-offset-6 col-sm-offset-6">
                                <button type="button" class="btn btn-primary print-btn"><i class="fa fa-print"></i> Print</button>
                                <button type="button" class="btn btn-info pdf-btn"><i class="fa fa-file-pdf-o"></i> PDF Export</button>
                            </div> -->
                            <div id="Report-Table">
                                <div class="preloader"><img src="<?php echo base_url() ?>theme/images/ring.gif"></div>
                                <div class="report-heading">
                                    <h4>Income VS Expense Report</h4>
                                    <p>Date From - - - - To - - - -</p>
                                </div>
                                <div id="Table-div"></div>

                            </div>
                        </div>
                        <!--End Panel Body-->
                    </div>


                    <!--End Panel-->
                </div>
            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->

<?php $this->load->view('reports/common/swap-transaction-modal'); ?>


<script type="text/javascript">
    $(document).ready(function() {
        $(".select2sel").select2();

        $(document).on('click', '.check-income', function() {
            if ($(this).is(":checked")) {
                $(this).parent().parent().css("background", "#daede0");
            } else {
                $(this).parent().parent().removeAttr("style");
            }
            var tot = 0;
            $('input.check-income:checked').each(function() {
                tot += parseInt(this.value);
            });
            $('#total-income').text(tot);

        });

        $(document).on('click', '.check-expense', function() {
            if ($(this).is(":checked")) {
                $(this).parent().parent().css("background", "#f1d7d7");
            } else {
                $(this).parent().parent().removeAttr("style");
            }
            var tot = 0;
            $('input.check-expense:checked').each(function() {
                tot += parseInt(this.value);
            });
            $('#total-expense').text(tot);

        });

        $('#transfer-report').on('submit', function() {
            var link = $(this).attr("action");
            if ($("#from-date").val() != "" && $("#to-date").val() != "") {
                //query data
                $.ajax({
                    method: "POST",
                    url: link,
                    data: $(this).serialize(),
                    beforeSend: function() {
                        $(".preloader").css("display", "block");
                    },
                    success: function(data) {
                        $(".preloader").css("display", "none");
                        if (data != "false") {
                            $("#Table-div").html(data);
                            $(".report-heading p").html("Date From " + $("#from-date").val() + " To " + $("#to-date").val());
                        } else {
                            $("#Table-div").html("");
                            $(".report-heading p").html("Date From " + $("#from-date").val() + " To " + $("#to-date").val());
                            swal("Alert", "Sorry, No Data Found !", "info");
                        }
                    }

                });
            } else {
                swal("Alert", "Please Select Date Range.", "info");
            }

            return false;
        });


    });
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_income_expense.js"></script>