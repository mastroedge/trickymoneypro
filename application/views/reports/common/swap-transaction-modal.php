<?php
$accounts = $this->Adminmodel->resultsObjectToArray($accounts, 'accounts_name', 'accounts_name');
$selmt = array(
    'name' => 'account_to',
    'options' => $accounts,
    'slected' => '',
    'js' => 'class="select2sel account_to form-control"',
    'isLabel' => true,
    'label' => 'Account Name',
    'divClass' => 'row',
    'labelClass' => 'col-md-12 col-lg-12 col-sm-12 col-4',
    'optionClass' => 'col-md-12 col-lg-12 col-sm-12 col-8'
);
$account_to = $this->Displaymodel->selectBox($selmt);
?>
<!-- Profile Modal -->
<div id="swapTransactionModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">

                <!--Start Panel-->
                <div class="card panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Swap account</div>
                    <div class="panel-body add-client">
                        <form id="swap-transaction" method="post"
                            action="<?php echo site_url('Admin/swapTransactionAjax') ?>">
                            <input type="hidden" value="" class="form-control" name="curr_trans_id">
                            <div class="row m-0">
                                <div class="col-12 col-md-9 m-0">
                                <?= $account_to ?>
                                </div>
                                <div class="col-12 col-md-3 m-0">
                                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i>
                                Update</button>
                                </div>
                            </div>
                            
                            
                        </form>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>

        </div>
    </div>
</div>
<!--End Model-->