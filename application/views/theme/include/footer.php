<!--End Footer-->
</div>
</section>

<!--Start Footer-->
<footer id="footer">
    <p><b>Copyright</b> &copy; <?= date('Y') ?> Money</p>
</footer>

</div><!-- End Wrapper -->

<?php
// Array of JavaScript files
$js_files = [
    'theme/js/bootstrap.bundle.min.js',
    'theme/js/select2.min.js',
    'theme/js/flatpickr.js',
    'theme/js/moment.min.js',
    'theme/js/dataTables.min.js',
    'theme/js/dataTables.responsive.min.js',
    'theme/js/sweetalert.min.js',
    'theme/js/pdf/jspdf.debug.js',
    'theme/js/material.min.js',
];

// Loop through the JavaScript files and print <script> tags
foreach ($js_files as $js_file) {
    echo '<script src="' . base_url($js_file) . '"></script>' . PHP_EOL;
}

// Conditional JavaScript for Admin/dashboard
if ($this->uri->uri_string() === 'Admin/dashboard') {
    echo '<script src="' . base_url('theme/js/c3.min.js') . '"></script>' . PHP_EOL;
    echo '<script src="' . base_url('theme/js/d3.min.js') . '"></script>' . PHP_EOL;
}

// Conditional JavaScript for Admin/incomeCalender
$calender_uris = ['Admin/incomeCalender', 'Admin/expenseCalender', 'Admin/transferCalender'];
// Check if the current URI is in the array
if (in_array($this->uri->uri_string(), $calender_uris)) {
    echo '<script src="' . base_url('theme/js/fullcalendar.min.js') . '"></script>' . PHP_EOL;
}
echo '<script src="' . base_url('theme/js/custom.js') . '"></script>' . PHP_EOL;
?>


<script type="text/javascript">
    $(document).on('ready', function() {

        $(document).on('submit', '#edit-profile', function() {
            var link = $(this).attr("action");
            $.ajax({
                method: "POST",
                url: link,
                data: $(this).serialize(),
                beforeSend: function() {
                    $(".block-ui").css('display', 'block');
                },
                success: function(data) {
                    if (data == "true") {
                        swal("Alert", "Saved Sucessfully", "success");
                    } else {
                        swal("Alert", data, "info");
                    }
                    $(".block-ui").css("display", "none");
                }
            });

            return false;
        });

        $(document).on('submit', '#change-password', function() {
            var link = $(this).attr("action");
            $.ajax({
                method: "POST",
                url: link,
                data: $(this).serialize(),
                beforeSend: function() {
                    $(".block-ui").css('display', 'block');
                },
                success: function(data) {
                    if (data == "true") {
                        swal("Alert", "Saved Sucessfully", "success");
                        $("#change-password")[0].reset();
                    } else {
                        swal("Alert", data, "info");
                    }
                    $(".block-ui").css("display", "none");
                }
            });

            return false;
        });


        // loadpage(); 
        // loadpage2();
        <?php
        if (in_array($this->uri->uri_string(), $calender_uris)) {
            echo "$('#current-calendar').fullCalendar();";
        }
        ?>

    });
</script>

<!-- Profile Modal -->
<div id="profileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Profile</h4>
            </div>
            <div class="modal-body">

                <!--Start Panel-->
                <div class="card panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Edit Profile</div>
                    <div class="panel-body add-client">
                        <form id="edit-profile" method="post"
                            action="<?php echo site_url('Admin/updateProfile') ?>">
                            <div class="mb-3">
                                <label for="profle-username">Username</label>
                                <input type="text"
                                    value="<?php echo $this->session->userdata('username') ?>"
                                    class="form-control" name="username" id="profle-username">
                            </div>
                            <div class="mb-3">
                                <label for="profle-fullname">Fullname</label>
                                <input type="text"
                                    value="<?php echo $this->session->userdata('fullname') ?>"
                                    class="form-control" name="fullname" id="profle-fullname">
                            </div>
                            <div class="mb-3">
                                <label for="profle-email">Email</label>
                                <input type="text" value="<?php echo $this->session->userdata('email') ?>"
                                    class="form-control" name="email" id="profle-email">
                            </div>

                            <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i>
                                Update</button>
                        </form>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>

        </div>
    </div>
</div>
<!--End Model-->


<!-- Modal -->
<div id="passwordModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">

                <!--Start Panel-->
                <div class="card panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Change Password</div>
                    <div class="panel-body add-client">
                        <form id="change-password" method="post"
                            action="<?php echo site_url('Admin/changePassword') ?>">
                            <div class="mb-3">
                                <label for="new-password">New Password</label>
                                <input type="password" class="form-control" name="new-password"
                                    id="new-password">
                            </div>
                            <div class="mb-3">
                                <label for="confrim-change">Confrim Password</label>
                                <input type="password" class="form-control" name="confrim-password"
                                    id="confrim-password">
                            </div>


                            <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i>
                                Update</button>
                        </form>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>

        </div>
    </div>
</div>
<!--End Model-->

<!-- Calculator Model -->
<!-- Modal -->
<div id="calculatorModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calculator</h4>
            </div>
            <div class="modal-body">

                <div class="calculator">
                    <input type="text" readonly>
                    <div class="row">
                        <div class="key">1</div>
                        <div class="key">2</div>
                        <div class="key">3</div>
                        <div class="key last">0</div>
                    </div>
                    <div class="row">
                        <div class="key">4</div>
                        <div class="key">5</div>
                        <div class="key">6</div>
                        <div class="key last action instant">cl</div>
                    </div>
                    <div class="row">
                        <div class="key">7</div>
                        <div class="key">8</div>
                        <div class="key">9</div>
                        <div class="key last action instant">=</div>
                    </div>
                    <div class="row">
                        <div class="key action">+</div>
                        <div class="key action">-</div>
                        <div class="key action">x</div>
                        <div class="key last action">/</div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
<!--End Calculator Model-->

<!-- Calendar Model -->
<!-- Modal -->
<div id="calendarModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Calendar</h4>
            </div>
            <div class="modal-body">
                <!-- Create Calendar-->
                <div id="current-calendar"></div>

            </div>

        </div>
    </div>
</div>
<!--End Calendar Model-->

<!-- Common Modal -->
<div class="modal fade" id="myCommonModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myCommonModalTitle">Modal Title</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Loading...
            </div>
        </div>
    </div>
</div>
</body>

</html>