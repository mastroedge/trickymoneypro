<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>/theme/images/favicon.png">
    <title>Budget Management</title>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
    // Array of CSS files
    $css_files = [
        'theme/css/bootstrap.min.css',
        'theme/css/docs.css',
        'theme/css/font-awesome.css',
        'theme/css/ionicons.css',
        'theme/css/select2.css',
        'theme/css/flatpickr.min.css',
        'theme/css/fullcalendar.min.css',
        'theme/css/jquery.dataTables.min.css',
        'theme/css/responsive.dataTables.css',
        'theme/css/sweetalert.css',
        // 'theme/css/material.css',
        'theme/css/my-style.css'
    ];

    // Loop through the CSS files and print <link> tags
    foreach ($css_files as $css_file) {
        echo '<link href="' . base_url($css_file) . '" rel="stylesheet" type="text/css">' . PHP_EOL;
    }

    // Conditional CSS for Admin/dashboard
    if ($this->uri->uri_string() === 'Admin/dashboard') {
        echo '<link href="' . base_url('theme/css/c3.css') . '" rel="stylesheet" type="text/css">' . PHP_EOL;
    }
    // Array of JS files
    $js_files = [
        'theme/js/jquery-3.7.1.min.js',
        'theme/js/modernizr.js',
        // Add more JS files here as needed
        // 'theme/js/bootstrap.js',
        // Example: 'theme/js/jquery-ui.js' (if needed)
    ];

    // Loop through the JS files and print <script> tags
    foreach ($js_files as $js_file) {
        echo '<script src="' . base_url($js_file) . '"></script>' . PHP_EOL;
    }
    ?>

</head>

<body>
    <div class="block-ui">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
    <div id="wrapper">
        <header>

            <nav class="navbar navbar-expand-lg bg-body-tertiary">
                <div class="container-fluid">
                    <a class="navbar-brand" href="<?php echo site_url('Admin/dashboard') ?>">MONEY</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="<?php echo site_url('Admin/dashboard') ?>"><i class="fa fa-tachometer"></i> Home</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-university"></i> Accounts
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageAccount') ?>"><i
                                                class="fa fa-table"></i> Manage Accounts</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/addAccount') ?>"><i
                                                class="fa fa-user-plus"></i> Add Accounts</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-money"></i> Transactions
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item asyn-income" href="<?php echo site_url('Admin/addIncome') ?>"><i
                                                class=" fa fa-plus-square"></i> Add Income</a></li>
                                    <li><a class="dropdown-item asyn-incomesplit"
                                            href="<?php echo site_url('Admin/addIncomeSplit') ?>"><i
                                                class=" fa fa-plus-square"></i> Add Split Income</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item asyn-expense" href="<?php echo site_url('Admin/addExpense') ?>"><i
                                                class=" fa fa-minus-square"></i> Add Expense</a></li>
                                    <li><a class="dropdown-item asyn-expensesplit"
                                            href="<?php echo site_url('Admin/addExpenseSplit') ?>"><i
                                                class=" fa fa-minus-square"></i> Add Split Expense</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/transfer') ?>"><i
                                                class=" fa fa-retweet"></i> Transfer</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item asyn-bulk-Transaction"
                                            href="<?php echo site_url('Admin/bulkTransaction') ?>"><i
                                                class=" fa fa-archive"></i> Add Bulk Transaction</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageIncome') ?>"><i
                                                class=" fa fa-calculator"></i> Manage Income</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageExpense') ?>"><i
                                                class=" fa fa-calculator"></i> Manage Expense</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-repeat"></i> Recurring Transaction
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item asyn-repeat-income"
                                            href="<?php echo site_url('Admin/repeatIncome') ?>"><i
                                                class="fa fa-plus-circle"></i> Repeating Income</a></li>
                                    <li><a class="dropdown-item asyn-repeat-expense"
                                            href="<?php echo site_url('Admin/repeatExpense') ?>"><i
                                                class="fa fa-minus-circle"></i> Repeating Expense</a></li>
                                    <li><a class="dropdown-item asyn-repeat-transfer"
                                            href="<?php echo site_url('Admin/repeatTransfer') ?>"><i
                                                class="fa fa-minus-circle"></i> Repeating Transfer</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/processIncome') ?>"><i
                                                class="fa fa-calendar-plus-o"></i> Manage Repeating Income</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/processExpense') ?>"><i
                                                class="fa fa-calendar-minus-o"></i> Manage Repeating Expense</a>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/processTransfer') ?>"><i
                                                class="fa fa-calendar-minus-o"></i> Manage Repeating Transfer</a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/incomeCalender') ?>"><i
                                                class="fa fa-calendar"></i> Income Calendar</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/expenseCalender') ?>"><i
                                                class="fa fa-calendar"></i> Expense Calendar</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/transferCalender') ?>"><i
                                                class="fa fa-calendar"></i> Transfer Calendar</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-car" aria-hidden="true"></i> Vehicles
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageVehicles') ?>"><i class="fa fa-motorcycle" aria-hidden="true"></i>
                                            Manage Vehicles</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageTrips') ?>"><i class="fa fa-plane" aria-hidden="true"></i>
                                            Manage Trips</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageFuels') ?>"><i class="fa fa-filter" aria-hidden="true"></i>
                                            Manage Fuel</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/manageServices') ?>"><i class="fa fa-legal" aria-hidden="true"></i>
                                            Manage Services</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-plus-square"></i> Tasks
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item asyn-task" href="<?php echo site_url('Admin/addTask') ?>"><i
                                                class="fa fa-plus-square"></i> Add Task</a></li>
                                    <li><a class="dropdown-item asyn-managetask"
                                            href="<?php echo site_url('Admin/manageTask') ?>"><i
                                                class="fa fa-calculator"></i> Manage Task</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-area-chart"></i> Reporting
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/accountStatement') ?>"><i
                                                class="fa fa-angle-double-right"></i> Account Statement</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/incomeVsExpense') ?>"><i
                                                class="fa fa-angle-double-right"></i> Income Vs Expense Report</a>
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/transactReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Transact Report</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/datewiseIncomeReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Income Report By Date</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/datewiseExpenseReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Expense Report By Date</a>
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/daywiseIncomeReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Day Wise Income Report</a>
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/daywiseExpenseReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Day Wise Expense Report</a>
                                    </li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/reportByPayer') ?>"><i
                                                class="fa fa-angle-double-right"></i> Report By Payer</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/reportByPayee') ?>"><i
                                                class="fa fa-angle-double-right"></i> Report By Payee</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/transferReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Transfer Report</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Reports/incomeCategoryReport') ?>"><i
                                                class="fa fa-angle-double-right"></i> Report By Chart Of
                                            Accounts</a></li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-credit-card"></i> Transaction Settings
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/chartOfAccounts') ?>"><i
                                                class="fa fa-book"></i> Chart of Accounts</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/payeeAndPayers') ?>"><i
                                                class="fa fa-exchange"></i> Payees and payers</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('Admin/paymentMethod') ?>"><i
                                                class="fa fa-credit-card"></i> Payment Methods</a></li>
                                </ul>
                            </li>
                            <?php if ($this->session->userdata('user_type') != 'User') { ?>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-cog"></i> Administration
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="<?php echo site_url('Admin/userManagement') ?>"><i
                                                    class="fa fa-users"></i> User Management</a></li>
                                        <!--<li><a class="dropdown-item" href="<?php echo site_url('Admin/addLanguage') ?>"><i class="fa fa-language"></i>Add Language</a></li>-->
                                        <li><a class="dropdown-item" href="<?php echo site_url('Admin/generalSettings') ?>"><i
                                                    class="fa fa-cogs"></i> General Settings</a></li>
                                        <li><a class="dropdown-item pp" href="<?php echo site_url('Admin/backupDatabase') ?>"><i
                                                    class="fa fa-database"></i> Backup Database</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <!-- <a id="profile" href=""><img src="<?php echo base_url() ?>/theme/images/avatar.jpg"
                                    alt="" /> -->
                                    <span class="profile-info"><?php echo $this->session->userdata('username'); ?>
                                        <?php if ($this->session->userdata('user_type') == 'Admin') { ?>
                                            <small>Administrator</small>
                                        <?php } else { ?>
                                            <small><?php echo $this->session->userdata('user_type'); ?></small>
                                        <?php } ?>

                                    </span><span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="" data-toggle="modal" data-target="#profileModal"><i
                                                class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;Profile</a></li>
                                    <li><a class="dropdown-item" href="" data-toggle="modal" data-target="#passwordModal"><i
                                                class="fa fa-exchange"></i>&nbsp;&nbsp;&nbsp;Change Password</a></li>
                                    <li><a class="dropdown-item" href="<?php echo site_url('User/logout') ?>"><i
                                                class="fa fa-power-off"></i>&nbsp;&nbsp;&nbsp;Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <section>
            <div class="container-fluid content-area">