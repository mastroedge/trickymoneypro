<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Payee And Payer'); ?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->


        <div class="col-md-5 col-lg-5 col-sm-5">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Add Payee/Payers</div>
            <div class="panel-body add-client">
              <form id="add-payers-payee" action="<?php echo site_url('Admin/payeeAndPayers/insert') ?>">
                <input type="hidden" name="action" id="action" value="insert" />
                <input type="hidden" name="trace_id" id="trace_id" value="" />
                <div class="mb-3">
                  <label for="p-nam">Payee/Payer Name</label>
                  <input type="text" class="form-control" maxlength="30" name="p-name" id="p-name" autocomplete="off" />
                </div>

                <div class="mb-3">
                  <label for="p-type">Type</label>
                  <select name="p-type" class="form-control" id="p-type">
                    <option value="Payee">Payee</option>
                    <option value="Payer">Payer</option>
                  </select>
                </div>

                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Save</button>
              </form>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>



        <div class="col-md-7 col-lg-7 col-sm-7">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Payee And Payer List</div>
            <div class="panel-body">
            <input type="text" id="search" class="form-control" placeholder="Search here..." autocomplete="off">

              <!-- Nav tabs -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile1-tab" data-bs-toggle="tab" data-bs-target="#profile1" type="button" role="tab" aria-controls="profile1" aria-selected="false">Disabled</button>
                </li>
              </ul>

              <!-- Tab Content -->
              <div class="tab-content mt-3" id="myTabContent">
                <div class="tab-pane table-responsive fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <?= $this->Displaymodel->getPayerPayeeTable($p_list, STATUS_ACTIVE); ?>
                </div>
                <div class="tab-pane table-responsive fade" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
                  <?= $this->Displaymodel->getPayerPayeeTable($p_list, STATUS_DISABLE); ?>
                </div>
              </div>


            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>


      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
  $(document).ready(function() {
    $("#p-type").select2();
    $("#p-type").select2("val", "");

    $(document).on('click', '.action-edit-btn', function() {
      var main = $(this);
      $("#action").val("update");
      $("#trace_id").val($(main).attr("href"));
      $("#p-name").val($(main).closest("tr").find(".t_name").html());
      $("#p-type").select2("val", $(main).closest("tr").find(".t_type").html());
      //get table index
      var tr = $(main).closest('tr');
      myRow = tr.index();

      return false;
    });

  });
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_transaction_settings.js"></script>
<style>
  #search {
    width: 42%;
    position: absolute;
    right: 10px;
  }
</style>