<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Payment Method'); ?>

                <!--Alert-->
                <div class="system-alert-box sticky-alert">
                    <div class="alert alert-success ajax-notify"></div>
                </div>
                <!--End Alert-->


                <div class="col-md-5 col-lg-5 col-sm-5">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Add Payment Method</div>
                        <div class="panel-body add-client">
                            <form id="add-payment-method" action="<?php echo site_url('Admin/paymentMethod/insert') ?>">
                                <input type="hidden" name="action" id="action" value="insert" />
                                <input type="hidden" name="p_method_id" id="p_method_id" value="" />
                                <div class="mb-3">
                                    <label for="p_method">Name</label>
                                    <input type="text" class="form-control" maxlength="20" name="p_method" id="p_method" autocomplete="off" />
                                </div>

                                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Save</button>
                            </form>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>



                <div class="col-md-7 col-lg-7 col-sm-7">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Payment Method List</div>
                        <div class="panel-body">
                        <input type="text" id="search" class="form-control" placeholder="Search here..." autocomplete="off">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile1-tab" data-bs-toggle="tab" data-bs-target="#profile1" type="button" role="tab" aria-controls="profile1" aria-selected="false">Disabled</button>
                                </li>
                            </ul>

                            <!-- Tab Content -->
                            <div class="tab-content mt-3" id="myTabContent">
                                <div class="tab-pane table-responsive fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <?= $this->Displaymodel->getPaymentMethodTable($p_list, STATUS_ACTIVE); ?>
                                </div>
                                <div class="tab-pane table-responsive fade" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
                                    <?= $this->Displaymodel->getPaymentMethodTable($p_list, STATUS_DISABLE); ?>
                                </div>
                            </div>

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '.action-edit-btn', function() {
            var main = $(this);
            $("#action").val("update");
            $("#p_method_id").val($(main).attr("href"));
            $("#p_method").val($(main).closest("tr").find(".t_name").html());
            //get table index
            var tr = $(main).closest('tr');
            myRow = tr.index();
            return false;
        });
    });
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_transaction_settings.js"></script>
<style>
  #search {
    width: 42%;
    position: absolute;
    right: 10px;
  }
</style>