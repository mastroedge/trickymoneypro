<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Chart of Accounts'); ?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->


        <div class="col-md-5 col-lg-5 col-sm-5">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Add Income/Expense Category</div>
            <div class="panel-body add-client">
              <form id="add-Chart-account" action="<?php echo site_url('Admin/chartOfAccounts/insert') ?>">
                <input type="hidden" name="action" id="action" value="insert" />
                <input type="hidden" name="chart_id" id="chart_id" value="" />
                <div class="mb-3">
                  <label for="category_name">Category Name</label>
                  <input type="text" maxlength="30" class="form-control" name="category_name" id="category_name" autocomplete="off" />
                </div>


                <div class="mb-3">
                  <label for="account-type">Account Type</label>
                  <select name="account-type" class="form-control" id="account-type">
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </select>
                </div>

                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Save</button>
              </form>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>



        <div class="col-md-7 col-lg-7 col-sm-7">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Chart of Accounts</div>
            <div class="panel-body">
              <input type="text" id="search" class="form-control" placeholder="Search here..." autocomplete="off">
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile1-tab" data-bs-toggle="tab" data-bs-target="#profile1" type="button" role="tab" aria-controls="profile1" aria-selected="false">Disabled</button>
                </li>
              </ul>

              <!-- Tab Content -->
              <div class="tab-content mt-3" id="myTabContent">
                <div class="tab-pane table-responsive fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <?= $this->Displaymodel->getChartOfAccountsTable($accountList, STATUS_ACTIVE); ?>
                </div>
                <div class="tab-pane table-responsive fade" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
                  <?= $this->Displaymodel->getChartOfAccountsTable($accountList, STATUS_DISABLE); ?>
                </div>
              </div>

            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>


      </div><!--End row-->
    </div><!--End inner-contatier-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
  $(document).ready(function() {
    var myRow;
    $("#account-type").select2();
    $("#account-type").select2("val", "");

    $(document).on('click', '.action-edit-btn', function() {
      var main = $(this);
      $("#action").val("update");
      $("#chart_id").val($(main).attr("href"));
      $("#category_name").val($(main).closest("tr").find(".t_name").html());
      $("#account-type").select2("val", $(main).closest("tr").find(".t_type").html());
      //get table index
      var tr = $(main).closest('tr');
      myRow = tr.index();

      return false;
    });
  });
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_transaction_settings.js"></script>
<style>
  #search {
    width: 42%;
    position: absolute;
    right: 10px;
  }
</style>