<div id="ajax-message"></div>
<form id="add-vehicle-form">
    <input type="hidden" name="action" id="action" value="<?=(isset($single) ? "update" : 'insert')?>" />
    <input type="hidden" name="fuel_id" id="fuel_id" value="<?=(isset($single) ? $single->fuel_id : '')?>" />
    <div class="row mx-0">
        <?php
        $selmt = array(
            'name' => 'vehicle_id',
            'options' => $vehicles,
            'slected' => (isset($single) ? $single->vehicle_id : ''),
            'js' => array('class' =>'select2sel type form-control', 'required' => 'required'),
            'isLabel' => true,
            'label' => 'Vehicle Name',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->selectBox($selmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fill_date',
            'value' => (isset($single) ? $single->fill_date : displayDate()),
            'class' => 'datewithtimebulk',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Fill Date',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'meter_reading',
            'value' => (isset($single) ? $single->meter_reading : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Meter Reading',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fuel_liters',
            'value' => (isset($single) ? $single->fuel_liters : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Fuel Liters',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fuel_price',
            'value' => (isset($single) ? $single->fuel_price : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Fuel Price (Per liter)',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fuel_percentage',
            'value' => (isset($single) ? $single->fuel_percentage : ''),
            'class' => '',
            'isLabel' => true,
            'label' => 'Fuel Percentage (%)',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'note',
            'value' => (isset($single) ? $single->note : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Note',
            'divClass' => 'col-md-12 col-lg-12 col-sm-12 col-12 mb-5',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);
        ?>

        <button type="button" class="btn btn-primary" onclick="addDataToDB('add-vehicle-form', '<?=site_url('Admin/manageFuels/insert') ?>')"><i class="fa fa-check"></i> <?=(isset($single) ? "Edit" : 'Add')?></button>
    </div>
</form>