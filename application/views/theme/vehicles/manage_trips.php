<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
            <?php
                $top_menu = [
                    ["menu" => "Manage Vehicles", "link" => site_url('Admin/manageVehicles'), "icon" => "fa fa-motorcycle"],
                    ["menu" => "Manage Trips", "link" => site_url('Admin/manageTrips'), "icon" => "fa fa-plane", "active" => true],
                    ["menu" => "Manage Fuel", "link" => site_url('Admin/manageFuels'), "icon" => "fa fa-filter"],
                    ["menu" => "Manage Services", "link" => site_url('Admin/manageServices'), "icon" => "fa fa-legal"]
                ];
                echo $this->Adminmodel->add_breadcrumb('Manage Trips', $top_menu);
                ?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Manage Trips</div>
                        <div class="panel-body table-responsive">
                            <div class="add-button mb-3">
                                <button class="btn btn-primary mybtn btn-default asyn-link" onclick="showCommonModal('myCommonModal', '<?=site_url('Admin/manageTrips/addForm')?>')">Add Trip</button>
                                <button class="btn btn-primary mybtn btn-default asyn-link" onclick="showCommonModal('myCommonModal', '<?=site_url('Admin/manageFuels/addForm')?>')">Add Fuel</button>
                            </div>
                            <?=$table_list?>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->

<script src="<?= base_url('theme/js/custom_vehicle.js') ?>"></script>