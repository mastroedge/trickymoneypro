<div id="ajax-message"></div>
<form id="add-vehicle-form">
    <input type="hidden" name="action" id="action" value="<?=(isset($single) ? "update" : 'insert')?>" />
    <input type="hidden" name="vehicle_id" id="vehicle_id" value="<?=(isset($single) ? $single->vehicle_id : '')?>" />
    <div class="row mx-0">
        <?php
        $ielmt = array(
            'type' => 'text',
            'name' => 'name',
            'value' => (isset($single) ? $single->name : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Vehicle Name',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $selmt = array(
            'name' => 'type',
            'options' => array('Bike' => 'Bike', 'Car' => 'Car'),
            'slected' => (isset($single) ? $single->type : ''),
            'js' => array('class' =>'select2sel form-control', 'required' => 'required'),
            'isLabel' => true,
            'label' => 'Type',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->selectBox($selmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'registration_no',
            'value' => (isset($single) ? $single->registration_no : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Registration No.',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'month_year',
            'value' => (isset($single) ? $single->month_year : ''),
            'class' => '',
            'isLabel' => true,
            'label' => 'Month & Year',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fuel_liters',
            'value' => (isset($single) ? $single->fuel_liters : ''),
            'class' => '',
            'isLabel' => true,
            'label' => 'Fuel Capacity',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'note',
            'value' => (isset($single) ? $single->note : ''),
            'class' => '',
            'isLabel' => true,
            'label' => 'Note',
            'divClass' => 'col-md-12 col-lg-12 col-sm-12 col-6 mb-5',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);
        ?>

        <button type="button" class="btn btn-primary" onclick="addDataToDB('add-vehicle-form', '<?=site_url('Admin/manageVehicles/insert') ?>')"><i class="fa fa-check"></i> <?=(isset($single) ? "Edit" : 'Add')?></button>
    </div>
</form>