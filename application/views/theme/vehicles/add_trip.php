<div id="ajax-message"></div>
<form id="add-trip-form">
    <input type="hidden" name="action" id="action" value="<?= $formaction ?>" />
    <input type="hidden" name="trip_id" id="trip_id" value="<?= (isset($single) ? $single->trip_id : '') ?>" />
    <div class="row mx-0">
        <?php
        $selmt = array(
            'name' => 'vehicle_id',
            'options' => $vehicles,
            'slected' => (isset($single) ? $single->vehicle_id : ''),
            'js' => array('class' =>'select2sel type form-control', 'required' => 'required'),
            'isLabel' => true,
            'label' => 'Vehicle Name',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->selectBox($selmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'trip_date',
            'value' => (isset($single) ? $single->trip_date : displayDate()),
            'class' => 'datewithtimebulk',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Trip Date',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'meter_reading',
            'value' => (isset($single) ? $single->meter_reading : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Meter Reading',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'type' => 'text',
            'name' => 'fuel_percentage',
            'value' => (isset($single) ? $single->fuel_percentage : ''),
            'class' => '',
            'isLabel' => true,
            'label' => 'Fuel (%)',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);
        
        $ielmt = array(
            'type' => 'text',
            'name' => 'note',
            'value' => (isset($single) ? $single->note : ''),
            'class' => '',
            'required' => 'required',
            'isLabel' => true,
            'label' => 'Note',
            'divClass' => 'col-md-12 col-lg-12 col-sm-12 col-12 mb-5',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        echo $this->Displaymodel->textBox($ielmt);
        ?>

        <button type="button" class="btn btn-primary" onclick="addDataToDB('add-trip-form', '<?= site_url('Admin/manageTrips/insert') ?>')"><i class="fa fa-check"></i> <?= (isset($single) ? "Edit" : 'Add') ?></button>
    </div>
</form>