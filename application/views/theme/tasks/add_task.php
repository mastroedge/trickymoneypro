<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?=$this->Adminmodel->add_breadcrumb('Task');?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <div class="col-md-4 col-lg-4 col-sm-5 mb-3">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Add Task</div>
            <div class="panel-body add-client">
              <form id="add-task">
                <input type="hidden" name="action" id="action" value="insert" />
                <input type="hidden" name="trans_id" id="trans_id" value="" />
                <div class="mb-3">
                  <label for="startdate">Start Date</label>
                  <div class='input-group date'>
                    <input type='text' name="startdate" class="form-control dateonly" value="<?= displayDate() ?>" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="duedate">Due Date</label>
                  <div class='input-group date'>
                    <input type='text' name="duedate" class="form-control dateonly" value="<?= displayDate() ?>" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="priority">Priority</label>
                  <select name="priority" class="select2sel form-control">
                    <option value="Low">Low</option>
                    <option value="Medium">Medium</option>
                    <option value="High">High</option>
                  </select>
                </div>

                <div class="mb-3">
                  <label for="status">Status</label>
                  <select name="status" class="select2sel form-control">
                    <option value="Pending">Pending</option>
                    <option value="Partial">Partial</option>
                    <option value="Completed">Completed</option>
                  </select>
                </div>

                <div class="mb-3">
                  <label for="note">Note</label>
                  <textarea class="form-control" name="note" id="note" rows="5" placeholder="Enter note"></textarea>
                </div>

                <div class="mb-3">
                  <label for="datefinished">Date Finished</label>
                  <div class='input-group date'>
                    <input type='text' name="datefinished" class="form-control" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>


                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
              </form>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>


        <div class="col-md-8 col-lg-8 col-sm-7">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Tasks</div>
            <div class="panel-body">
              <table class="table table-striped table-bordered table-condensed income-table">
                <tr>
                  <th width="10">Status</th>
                  <th>Note</th>
                  <th class="col-date">Start Date</th>
                  <th class="col-date">Due Date</th>
                  <th class="col-date">Priority</th>
                </tr>
                <?php
                foreach ($tasksorder as $key => $tas) {
                  if (!is_array($tas)) {
                    continue;
                  }
                  echo '<tr><th colspan="5">' . $key . '</th></tr>';
                  foreach ($tas as $t) {
                ?>
                    <tr>
                      <td class="text-center">
                        <input type="checkbox" class="statuschange" value="<?= $t->task_id ?>" <?= (($t->status == "Complete") ? "checked" : "") ?> />
                      </td>
                      <td><?php echo $t->note ?></td>
                      <td><?php echo $t->startdate ?></td>
                      <td><?php echo $t->duedate ?></td>
                      <td><?php echo $t->priority ?></td>
                    </tr>
                <?php }
                } ?>
              </table>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>


      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->



<script type="text/javascript">
  $(document).ready(function() {
    $(".asyn-task").addClass("active-menu");
    $(".select2sel").select2();

    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
        (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });


    $('#add-task').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/addTask/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // appendRow();
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });

    function appendRow() {
      var date = $("#income-date").val();
      var note = $("#note").val();
      var account = $("#account").val();
      var type = $("#income-type").val();
      var payer = $("#payer").val();
      var amount = "<?php echo get_current_setting('currency_code_session') ?>" + " " + $("#amount").val();

      var row = "<tr>";
      row += "<td>" + date + "</td>";
      row += "<td>" + note + "<br/> - <span>" + payer + "</span> - <span>" + type + "</span> - <span>" + account + "</span></td>";
      row += "<td class='text-end'>" + amount + "</td>";
      row += "</tr>";
      $(".income-table tr:first").after(row);

    }

    $('.statuschange').on('change', function(event) {
      console.log($(this));
      var status = '';
      if ($(this).prop("checked") == true) {
        status = 'Complete';
      } else if ($(this).prop("checked") == false) {
        status = 'Pending';
      }
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/changeTaskStatus') ?>",
        data: "task_id=" + $(this).val() + "&status=" + status,
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // appendRow();
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });

  });
</script>