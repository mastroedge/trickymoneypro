<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?=$this->Adminmodel->add_breadcrumb('Edit task');?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <div class="col-md-5 col-lg-5 col-sm-5">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Edit Task</div>
            <div class="panel-body add-client">
              <form id="edit-task">
                <input type="hidden" name="action" id="action" value="update" />
                <input type="hidden" name="task_id" id="task_id" value="<?php echo $task->task_id ?>" />
                <div class="mb-3">
                  <label for="startdate">Start Date</label>
                  <div class='input-group date'>
                    <input type='text' name="startdate" class="form-control dateonly" value="<?php echo $task->startdate ?>" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="duedate">Due Date</label>
                  <div class='input-group date'>
                    <input type='text' name="duedate" class="form-control dateonly" value="<?php echo $task->duedate ?>" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>

                <div class="mb-3">
                  <label for="priority">Priority</label>
                  <select name="priority" class="select2sel form-control">
                    <option value="Low" <?= ($task->priority == "Low" ? "selected" : "") ?>>Low</option>
                    <option value="Medium" <?= ($task->priority == "Medium" ? "selected" : "") ?>>Medium</option>
                    <option value="High" <?= ($task->priority == "High" ? "selected" : "") ?>>High</option>
                  </select>
                </div>

                <div class="mb-3">
                  <label for="status">Status</label>
                  <select name="status" class="select2sel form-control">
                    <option value="Pending" <?= ($task->status == "Pending" ? "selected" : "") ?>>Pending</option>
                    <option value="Partial" <?= ($task->status == "Partial" ? "selected" : "") ?>>Partial</option>
                    <option value="Completed" <?= ($task->status == "Completed" ? "selected" : "") ?>>Completed</option>
                  </select>
                </div>

                <div class="mb-3">
                  <label for="note">Note</label>
                  <textarea class="form-control" name="note" id="note" rows="5" placeholder="Enter note"><?php echo $task->note ?></textarea>
                </div>

                <div class="mb-3">
                  <label for="datefinished">Date Finished</label>
                  <div class='input-group date'>
                    <input type='text' name="datefinished" class="form-control" value="<?php echo $task->datefinished ?>" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </span>
                  </div>
                </div>


                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
              </form>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>



      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->



<script type="text/javascript">
  $(document).ready(function() {
    $(".asyn-task").addClass("active-menu");
    if ($(".sidebar").width() == "0") {
      $(".main-content").css("padding-left", "0px");
    }

    $(".select2sel").select2();

    $('#edit-task').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/addTask/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // $('#edit-task')[0].reset(); 
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });



  });
</script>