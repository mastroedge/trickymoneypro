<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Income'); ?>
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Manage Task</div>
                        <div class="panel-body manage">
                            <div class="add-button">
                                <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/addTask') ?>">Add Task</a>
                            </div>
                            <table id="manage" class="display responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>startdate</th>
                                        <th>note</th>
                                        <th>priority</th>
                                        <th>duedate</th>
                                        <th>datefinished</th>
                                        <th>status</th>
                                        <th class="single-action">Manage</th>
                                    </tr>
                                </thead>

                                <tbody></tbody>
                            </table>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#manage').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('Admin/manageTaskAjax') ?>",
                "type": "POST"
            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            }, ],

        });

        /* $(document).on('click','.income-manage-btn',function(){
        
            var link=$(this).attr("href"); 
            $.ajax({
                method : "POST",
                url : link,
                beforeSend : function(){
                	$(".block-ui").css('display','block'); 
                },success : function(data){ 
                    //var link = location.pathname.replace(/^.*[\\\/]/, ''); //get filename only  
                    history.pushState(null, null,link);  
                    $('.asyn-div').load(link+'/asyn',function() {
                    	$(".block-ui").css('display','none');     
                    });
                }
            });
        
        return false;
        });*/


    });
</script>