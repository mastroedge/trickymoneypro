<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?=$this->Adminmodel->add_breadcrumb('Language Settings');?>
                <div class="col-md-5 col-lg-5 col-sm-5">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Add Language</div>
                        <div class="panel-body add-client">
                            <form id="add-account">
                                <div class="mb-3">
                                    <label for="account">Language</label>
                                    <input type="text" class="form-control" name="language" id="language" />
                                </div>

                                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Save</button>
                            </form>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>

                <!-- Start Table Section -->

                <div class="col-md-7 col-lg-7 col-sm-7">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Language List</div>
                        <div class="panel-body">
                            <table class="table table-striped table-bordered table-condensed">
                                <th>Language</th>
                                <th width="130">Action</th>

                                <tr>
                                    <td>Franch</td>
                                    <td><a class="btn btn-primary mybtn btn-info btn-xs" data-toggle="tooltip"
                                            title="Click For Receive" href="">Manage</a>
                                        <a class="btn btn-primary mybtn btn-danger btn-xs" data-toggle="tooltip"
                                            title="Click For Receive" href="">Remove</a>
                                    </td>
                                </tr>
                                <tr>

                                <tr>
                                    <td>Germany</td>
                                    <td><a class="btn btn-primary mybtn btn-info btn-xs" data-toggle="tooltip"
                                            title="Click For Receive" href="">Manage</a>
                                        <a class="btn btn-primary mybtn btn-danger btn-xs" data-toggle="tooltip"
                                            title="Click For Receive" href="">Remove</a>
                                    </td>
                                </tr>
                                <tr>



                            </table>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
    $(document).ready(function() {

    });
</script>