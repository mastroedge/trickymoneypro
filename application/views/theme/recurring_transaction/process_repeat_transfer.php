<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Transfer'); ?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Manage Transfer</div>
                        <div class="panel-body manage-client">
                            <div class="add-button">
                                <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/repeatTransfer') ?>">Add Transfer</a>
                            </div>
                            <hr>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <?php
                                $currentDate = new DateTime(); // Get the current date
                                $currentMonthYear = $currentDate->format('Y-m'); // Current year and month, e.g., '2024-10'
                                foreach ($repeat_transaction as $month => $rt) {
                                    $head = '';
                                    // Check if the key is a year or a year with a month (e.g., '2023' vs '2024-10')
                                    if (strlen($month) == 4) {
                                        // This is a year (e.g., '2023')
                                        $head = $month;
                                    } else {
                                        // This is a year and month (e.g., '2024-10'), so split them
                                        $dateTime = DateTime::createFromFormat('Y-m', $month);
                                        $head = $dateTime->format('M') . " " . $dateTime->format('Y');
                                    }
                                ?>
                                    <li class="nav-item" role="<?= $month ?>">
                                        <button class="nav-link <?= (($month === $currentMonthYear) ? 'active' : '') ?>" id="<?= $month ?>-tab" data-bs-toggle="tab" data-bs-target="#<?= $month ?>" type="button" role="tab" aria-controls="<?= $month ?>" aria-selected="true"><?= $head ?></button>
                                    </li>
                                <?php
                                }
                                ?>
                            </ul>

                            <!-- Tab Content -->
                            <div class="tab-content mt-3" id="myTabContent">
                                <?php
                                foreach ($repeat_transaction as $month => $rt) {
                                ?>
                                    <div class="tab-pane table-responsive fade show <?= (($month === $currentMonthYear) ? 'active' : '') ?>" id="<?= $month ?>" role="tabpanel" aria-labelledby="<?= $month ?>-tab">
                                        <?php
                                        $rtdata = array(
                                            't_data' => $rt
                                        );
                                        echo $this->Displaymodel->getRepeatTransactionTable($rtdata)
                                        ?>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->
<script src="<?php echo base_url() ?>/theme/js/custom_recurring_transaction.js"></script>