<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?=$this->Adminmodel->add_breadcrumb('Calendar');?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Income Calendar</div>
                        <div class="panel-body">
                            <!--Start Income Calendar-->
                            <div class="add-button">
                                <a class="btn btn-primary mybtn btn-default" href="">Add Income</a>
                            </div>
                            <hr>
                            <div id='income-calendar'></div>

                            <!--End Income Calendar-->

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->



<script type="text/javascript">
    $(document).ready(function() {

        $('#income-calendar').fullCalendar({
            events: [
                <?php foreach ($repeat_income as $income) {
                    $url = '';
                    if ($income->status == 'receive') {
                        $url = 1;
                    } else {
                        $url = site_url('Admin/processRepeatTransaction/action/' . $income->trans_id . '/receive');
                    }
                    // Define event color based on status
                    $eventColor = ($income->status == 'receive') ? 'green' : '#3a87ad';

                    $dis = '';
                    $dis .= $income->ref . '<br>';
                    $dis .= $income->note . '<br>';
                    $dis .= '<small>Account:-</small> ' . $income->accounts_name . ' <small>from</small> ' . $income->payer . '<br>';
                    $dis .= '<small>Amount:-</small> Rs.' . $income->amount . '<br>';
                    $dis .= $income->status . '<br>';
                ?> {
                        title: "<?= $dis ?>",
                        start: '<?php echo $income->trans_date ?>',
                        url: "<?php echo $url ?>",
                        color: "<?= $eventColor ?>"
                    },
                <?php } ?>

            ],
            color: 'yellow',
            textColor: 'black',
            dayClick: function(date, jsEvent, view) {

                //alert('Clicked on: ' + date.format());

            },
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
                element.attr('href', 'javascript:void(0);');
                element.click(function() {
                    //alert(event.title);
                    //showInfo(event.title);
                    showIncome(event.title, event.start.format(), event.url, event);
                    // $('#income-calendar').fullCalendar('refetchEvents'); // Fetch updated events from the source
                    // $('#income-calendar').fullCalendar('rerenderEvents'); // Rerender events without reloading
                    // $('#income-calendar').fullCalendar('updateEvent', event);
                });
            }


        });

    });
</script>