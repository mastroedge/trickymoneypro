<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Calendar'); ?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Transfer Calendar</div>
                        <div class="panel-body">
                            <div class="add-button">
                                <a class="btn btn-primary mybtn btn-default" href="">Add Transfer</a>
                            </div>
                            <hr>
                            <!--Start Income Calendar-->

                            <div id='transfer-calendar'></div>

                            <!--End Income Calendar-->

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
    $(document).ready(function() {

        $('#transfer-calendar').fullCalendar({
            events: [
                <?php foreach ($repeat_transfer as $expense) {
                    $url = '';
                    if ($expense->status == 'paid') {
                        $url = 1;
                    } else {
                        $url = site_url('Admin/processRepeatTransaction/action/' . $expense->trans_id . '/paid');
                    }
                    // Define event color based on status
                    $eventColor = ($expense->status == 'paid') ? 'green' : '#3a87ad';

                    $account_arr = split_account_by_delimiter($expense->accounts_name);
                    $dis = '';
                    $dis .= $expense->ref . '<br>';
                    $dis .= $expense->note . '<br>';
                    $dis .= '<small>Account:-</small> ' . $account_arr[0] . ' <small> -> </small> ' . $account_arr[1] . '<br>';
                    $dis .= '<small>Amount:-</small> Rs.' . $expense->amount . '<br>';
                    $dis .= $expense->status . '<br>';
                ?> {
                        title: "<?= $dis ?>",
                        start: '<?php echo $expense->trans_date ?>',
                        url: "<?php echo $url ?>",
                        color: "<?= $eventColor ?>"
                    },
                <?php } ?>

            ],
            color: 'yellow',
            textColor: 'black',
            dayClick: function(date, jsEvent, view) {

                alert('Clicked on: ' + date.format());

            },
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
                element.attr('href', 'javascript:void(0);');
                element.click(function() {
                    //alert(event.title);
                    //showInfo(event.title);
                    showExpense(event.title, event.start.format(), event.url, event);
                    // $('#transfer-calendar').fullCalendar('updateEvent', event);
                });
            }


        });

    });
</script>