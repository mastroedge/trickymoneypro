<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?=$this->Adminmodel->add_breadcrumb('Repeating Transfer');?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->


        <!--Start Panel-->
        <div class="card panel panel-default px-0">
          <!-- Default panel contents -->
          <div class="panel-heading">Add Repeating Transfer</div>
          <div class="panel-body add-client">
            <form id="add-repeat-transfer">
              <div class="row mx-0">
                <input type="hidden" name="action" id="action" value="insert" />
                <input type="hidden" name="trans_id" id="trans_id" value="" />
                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="account-from">Account From</label>
                    <select name="account-from" class="select2sel form-control" id="account-from">
                      <?php foreach ($accounts as $account) { ?>
                        <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="account-to">Account To</label>
                    <select name="account-to" class="select2sel form-control" id="account-to">
                      <?php foreach ($accounts as $account) { ?>
                        <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="date">Date</label>
                    <div class='input-group date' id='date'>
                      <input type='text' name="expense-date" id="expense-date" class="form-control datewithtime" value="<?= displayDate() ?>" />
                      <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="rotation">Rotation</label>
                    <select name="rotation" class="select2sel form-control" id="rotation">
                      <option value="+1 month">Monthly</option>
                      <option value="+1 week">Weekly</option>
                      <option value="+2 weeks">Bi Weekly</option>
                      <option value="+1 day">Everyday</option>
                      <option value="+2 months">Every 2 Month</option>
                      <option value="+3 months">Quarterly</option>
                      <option value="+6 months">Every 6 Month</option>
                      <option value="+1 year">Yearly</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="rotation-transfer">No. of Rotation (Payment)</label>
                    <input type="text" class="form-control" name="rotation-transfer" id="rotation-transfer">
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="amount">Amount</label>
                    <div class='input-group'>
                      <div class="input-group-addon">₹</div>
                      <input type="number" name="amount" id="amount" class="form-control amount" step="0.01" inputmode="decimal"/>
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="p_method">Payment Method</label>
                    <select name="p_method" class="select2sel form-control" id="p_method">
                      <?php foreach ($p_method as $method) { ?>
                        <option value="<?php echo $method->p_method_name ?>" <?= ($method->p_method_name == 'Cash' ? 'selected' : '') ?>><?php echo $method->p_method_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-4 col-sm-12">
                  <div class="mb-3">
                    <label for="reference">Reference No</label>
                    <input type="text" class="form-control" name="reference" id="reference" maxlength="<?=REF_MAX_LEN?>" placeholder="Enter text (max <?=REF_MAX_LEN?> characters)" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-2 col-lg-4 col-sm-12">
                  <div class="mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="Enter note" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12">
                  <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
                </div>
              </div>
            </form>
          </div>
          <!--End Panel Body-->
        </div>
        <!--End Panel-->





      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
  $(document).ready(function() {

    $(".asyn-repeat-transfer").addClass("active-menu");

    $("#rotation-transfer").keypress(function(e) {
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }
    });

    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
        (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });
    $(".select2sel").select2();
    $('#add-repeat-transfer').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/repeatTransfer/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // $('#add-repeat-transfer')[0].reset(); 
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });

  });
</script>