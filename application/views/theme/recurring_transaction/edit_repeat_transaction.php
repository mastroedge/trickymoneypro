<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Edit Repeating Transaction'); ?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <!--Start Panel-->
        <div class="card panel panel-default">
          <!-- Default panel contents -->
          <div class="panel-heading">Edit Transaction (<?= $type ?>)</div>
          <div class="panel-body add-client">
            <div class="add-button">
              <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/processIncome') ?>">Manage Repeat Income</a>
              <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/processExpense') ?>">Manage Repeat Expense</a>
              <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/processTransfer') ?>">Manage Repeat Transfer</a>
            </div>
            <hr>
            <form id="edit-repeat-transaction">
              <input type="hidden" name="action" id="action" value="update" />
              <input type="hidden" name="type" id="type" value="<?= $type ?>" />
              <input type="hidden" name="trans_id" id="trans_id" value="<?php echo $transaction->trans_id ?>" />
              <div class="row mx-0">

                <?php if ($type != TYPE_TRANSFER) { ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="from-account">Account</label>
                      <select name="accounts_name" class="select2sel form-control" id="accounts_name">
                        <?php foreach ($accounts as $account) { ?>
                          <option value="<?php echo $account->accounts_name ?>" <?= ($account->accounts_name == $transaction->accounts_name ? 'selected' : '') ?>><?php echo $account->accounts_name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } else {
                  $account_arr = split_account_by_delimiter($transaction->accounts_name);

                ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="from-account">Account From</label>
                      <select name="accounts_name[]" class="select2sel form-control" id="account-from">
                        <?php foreach ($accounts as $account) { ?>
                          <option value="<?php echo $account->accounts_name ?>" <?= ($account->accounts_name == $account_arr[0] ? 'selected' : '') ?>><?php echo $account->accounts_name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="from-account">Account To</label>
                      <select name="accounts_name[]" class="select2sel form-control" id="account-to">
                        <?php foreach ($accounts as $account) { ?>
                          <option value="<?php echo $account->accounts_name ?>" <?= ($account->accounts_name == $account_arr[1] ? 'selected' : '') ?>><?php echo $account->accounts_name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } ?>

                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <div class="mb-3">
                    <label for="date">Date</label>
                    <div class='input-group date' id='date'>
                      <input type='text' name="trans_date" id="date" value="<?php echo $transaction->trans_date ?>" class="form-control datewithtime" />
                      <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>

                <?php if ($type != TYPE_TRANSFER) { ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="income_expense_type"><?= $type ?> Type</label>
                      <select name="income_expense_type" class="select2sel form-control" id="income_expense_type">
                        <?php foreach ($category as $cat) { ?>
                          <option value="<?php echo $cat->accounts_name ?>" <?= ($transaction->category == $cat->accounts_name ? "selected" : "") ?>><?php echo $cat->accounts_name ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } ?>

                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <div class="mb-3">
                    <label for="amount">Amount <input type="checkbox" name="edit-amount" value="1"></label>
                    <div class='input-group'>
                      <div class="input-group-addon">₹</div>
                      <input type="number" name="amount" id="amount" value="<?php echo $transaction->amount ?>" class="form-control amount" step="0.01" inputmode="decimal" />
                    </div>
                  </div>
                </div>

                <?php if ($type == TYPE_INCOME && $type != TYPE_TRANSFER) { ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="payer">Payer</label>
                      <select name="payer" class="select2sel form-control" id="payer">
                        <?php foreach ($payers as $p) { ?>
                          <option value="<?php echo $p->payee_payers ?>" <?= ($transaction->payer == $p->payee_payers ? "selected" : "") ?>><?php echo $p->payee_payers ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } elseif ($type != TYPE_TRANSFER) { ?>
                  <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                    <div class="mb-3">
                      <label for="payee">Payee</label>
                      <select name="payee" class="select2sel form-control" id="payee">
                        <?php foreach ($payee as $p) { ?>
                          <option value="<?php echo $p->payee_payers ?>" <?= ($transaction->payee == $p->payee_payers ? "selected" : "") ?>><?php echo $p->payee_payers ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } ?>

                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <div class="mb-3">
                    <label for="p_method">Payment Method</label>
                    <select name="p_method" class="select2sel form-control" id="payment-method">
                      <?php foreach ($p_method as $method) { ?>
                        <option value="<?php echo $method->p_method_name ?>"><?php echo $method->p_method_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row mx-0">
                <div class="col-md-2 col-lg-4 col-sm-12 col-12">
                  <div class="mb-3">
                    <label for="reference">Reference No</label>
                    <input type="text" class="form-control" value="<?php echo $transaction->ref ?>" name="reference" id="reference" maxlength="<?= REF_MAX_LEN ?>" placeholder="Enter text (max <?= REF_MAX_LEN ?> characters)" autocomplete="off">
                  </div>
                </div>
                <div class="col-md-2 col-lg-8 col-sm-12 col-12">
                  <div class="mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" value="<?php echo $transaction->note ?>" name="note" id="note" placeholder="Enter note" autocomplete="off">
                  </div>
                </div>


                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
              </div>
            </form>
          </div>
          <!--End Panel Body-->
        </div>
        <!--End Panel-->




      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->



<script type="text/javascript">
  $(document).ready(function() {

    if ($(".sidebar").width() == "0") {
      $(".main-content").css("padding-left", "0px");
    }

    $(".select2sel").select2();
    $("#payment-method").select2("val", "<?php echo $transaction->p_method ?>");

    /*$("#amount").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 &&  (e.which < 48 || e.which > 57)) {
           //display error message
           return false;
       }
      });*/

    $('#edit-repeat-transaction').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/updateRepeatTransaction') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // $('#edit-repeat-transaction')[0].reset(); 
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });



  });
</script>