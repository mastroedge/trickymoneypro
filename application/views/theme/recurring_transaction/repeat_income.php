<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?=$this->Adminmodel->add_breadcrumb('Repeating Income');?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <!--Start Panel-->
        <div class="card panel panel-default px-0">
          <!-- Default panel contents -->
          <div class="panel-heading">Add Repeating Income</div>
          <div class="panel-body add-client">
            <form id="add-repeat-income" method="post" action="<?php echo site_url('Admin/repeatIncome/insert') ?>">
              <input type="hidden" name="action" id="action" value="insert" />
              <input type="hidden" name="trans_id" id="trans_id" value="" />
              <div class="row mx-0">
                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <div class="mb-3">
                    <label for="accounts_name">Account Name</label>
                    <select name="accounts_name" class="select2sel form-control" id="accounts_name">
                      <?php foreach ($accounts as $account) { ?>
                        <option value="<?php echo $account->accounts_name ?>"><?php echo $account->accounts_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <div class="mb-3">
                    <label for="accounts_name">Date</label>
                    <div class='input-group date' id='date'>
                      <input type='text' name="income-date" id="income-date" class="form-control datewithtime" value="<?= displayDate() ?>" />
                      <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="reference">Rotation</label>
                    <select name="rotation" class="select2sel form-control" id="rotation">
                      <option value="+1 month">Monthly</option>
                      <option value="+1 week">Weekly</option>
                      <option value="+2 weeks">Bi Weekly</option>
                      <option value="+1 day">Everyday</option>
                      <option value="+2 months">Every 2 Month</option>
                      <option value="+3 months">Quarterly</option>
                      <option value="+6 months">Every 6 Month</option>
                      <option value="+1 year">Yearly</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="rotation-income">No. of Rotation (Income)</label>
                    <input type="text" class="form-control" name="rotation-income" id="rotation-income">
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="income-type">Income Type</label>
                    <select name="income-type" class="select2sel form-control" id="income-type">
                      <?php foreach ($category as $cat) { ?>
                        <option value="<?php echo $cat->accounts_name ?>"><?php echo $cat->accounts_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="amount">Amount</label>
                    <div class='input-group'>
                      <div class="input-group-addon">₹</div>
                      <input type="number" name="amount" id="amount" class="form-control amount" step="0.01" inputmode="decimal"/>
                    </div>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="payer">Payer</label>
                    <select name="payer" class="select2sel form-control" id="payer">
                      <?php foreach ($payers as $p) { ?>
                        <option value="<?php echo $p->payee_payers ?>" <?= ($p->payee_payers == 'Anonymous' ? 'selected' : '') ?>><?php echo $p->payee_payers ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-4 col-4">
                  <div class="mb-3">
                    <label for="p_method">Payment Method</label>
                    <select name="p_method" class="select2sel form-control" id="p_method">
                      <?php foreach ($p_method as $method) { ?>
                        <option value="<?php echo $method->p_method_name ?>" <?= ($method->p_method_name == 'Cash' ? 'selected' : '') ?>><?php echo $method->p_method_name ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-2 col-lg-4 col-sm-12">
                  <div class="mb-3">
                    <label for="reference">Reference No</label>
                    <input type="text" class="form-control" name="reference" id="reference" maxlength="<?=REF_MAX_LEN?>" placeholder="Enter text (max <?=REF_MAX_LEN?> characters)" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-2 col-lg-4 col-sm-12">
                  <div class="mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="Enter note" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-2 col-lg-2 col-sm-6 col-6">
                  <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
                </div>
              </div>
            </form>
          </div>
          <!--End Panel Body-->
        </div>
        <!--End Panel-->



      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
  $(document).ready(function() {
    $(".asyn-repeat-income").addClass("active-menu");

    $("#rotation-income").keypress(function(e) {
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }
    });

    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
        (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });

    $(".select2sel").select2();

    $('#add-repeat-income').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/repeatIncome/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            // $('#add-repeat-income')[0].reset(); 
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });


  });
</script>