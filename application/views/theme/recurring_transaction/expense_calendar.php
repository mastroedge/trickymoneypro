<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?=$this->Adminmodel->add_breadcrumb('Calendar');?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Expense Calendar</div>
                        <div class="panel-body">
                            <!--Start Income Calendar-->
                            <div class="add-button">
                                <a class="btn btn-primary mybtn btn-default" href="">Add Expense</a>
                            </div>
                            <hr>
                            <div id='expense-calendar'></div>

                            <!--End Income Calendar-->

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
    $(document).ready(function() {

        $('#expense-calendar').fullCalendar({
            events: [
                <?php foreach ($repeat_expense as $expense) {
                    $url = '';
                    if ($expense->status == 'paid') {
                        $url = 1;
                    } else {
                        $url = site_url('Admin/processRepeatTransaction/action/' . $expense->trans_id . '/paid');
                    }
                    // Define event color based on status
                    $eventColor = ($expense->status == 'paid') ? 'green' : '#3a87ad';

                    $dis = '';
                    $dis .= $expense->ref . '<br>';
                    $dis .= $expense->note . '<br>';
                    $dis .= '<small>Account:-</small> ' . $expense->accounts_name . ' <small>to</small> ' . $expense->payee . '<br>';
                    $dis .= '<small>Amount:-</small> Rs.' . $expense->amount . '<br>';
                    $dis .= $expense->status . '<br>';
                ?> {
                        title: "<?= $dis ?>",
                        start: '<?php echo displayDate('date', DATE_ONLY_FORMAT, $expense->trans_date) ?>',
                        url: "<?php echo $url ?>",
                        color: "<?= $eventColor ?>"
                    },
                <?php } ?>

            ],
            color: 'yellow',
            textColor: 'black',
            dayClick: function(date, jsEvent, view) {

                alert('Clicked on: ' + date.format());

            },
            eventRender: function(event, element) {
                element.find('.fc-title').html(event.title);
                element.attr('href', 'javascript:void(0);');
                element.click(function() {
                    //alert(event.title);
                    //showInfo(event.title);
                    showExpense(event.title, event.start.format(), event.url, event);
                    // $('#expense-calendar').fullCalendar('updateEvent', event);
                });
            }


        });

    });
</script>