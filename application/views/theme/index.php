<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="row">
            <?= $this->Adminmodel->add_breadcrumb('Dashboard'); ?>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-success asyn-income quick-btn" href="<?php echo site_url('Admin/addIncome') ?>"><i class="fa fa-plus-square"></i> Add Income</a>
            </div>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-danger asyn-expense quick-btn" href="<?php echo site_url('Admin/addExpense') ?>"><i class="fa fa-minus-square"></i> Add Expense</a>
            </div>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-warning quick-btn" href="<?php echo site_url('Admin/transfer') ?>"><i class="fa fa-retweet"></i> Transfer</a>
            </div>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-primary quick-btn" href="<?php echo site_url('Admin/bulkTransaction') ?>"><i class="fa fa-table"></i> Bulk Transaction</a>
            </div>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-info quick-btn" href="#" onclick="showCommonModal('myCommonModal', '<?= site_url('Admin/manageTrips/addForm') ?>')"><i class="fa fa-cab"></i> Add Trip</a>
            </div>
            <div class="clo-md-2 col-lg-2 col-sm-6 col-6 mb-2">
                <a class="btn btn-success quick-btn" href="#" onclick="showCommonModal('myCommonModal', '<?= site_url('Admin/manageFuels/addForm') ?>')"><i class="fa fa-money"></i> Add Fuel</a>
            </div>

            <?php /* ?><form id="info-upto-date">
            <div class="mb-3">
                <label for="upto-date">Upto Date : <?=get_current_setting('info_upto_date');?></label>
                <div class='input-group date' id='date'>
                    <input type='text' name="upto-date" id="upto-date" class="form-control" />
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                </div>
                <input type="submit" value="Go">
            </div>
            </form> */ ?>
        </div>

        <div class="row">
            <div class="clo-md-3 col-lg-3 col-sm-6">
                <div class="card-box">
                    <div class="box-callout-green">
                        <div class="rightside-cart">
                            <p class="card-head">Current Day Income<br>
                                <canvas id="current-day-income" height="100" width="160"></canvas>
                            <div class="cart-caption">
                                <div class="cart-symbol"></div>
                                <div id="current-income-preview" class="cart-preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clo-md-3 col-lg-3 col-sm-6">
                <div class="card-box">
                    <div class="box-callout-orange">

                        <div class="rightside-cart">
                            <p class="card-head">Current Day Expense<br>
                                <canvas id="current-day-expense" height="100" width="160"></canvas>
                            <div class="cart-caption">
                                <div class="cart-symbol"></div>
                                <div id="current-expense-preview" class="cart-preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clo-md-3 col-lg-3 col-sm-6">
                <div class="card-box">
                    <div class="box-callout-green">
                        <div class="rightside-cart">
                            <p class="card-head">Current Month Income<br>
                                <canvas id="current-month-income" height="100" width="160"></canvas>
                            <div class="cart-caption">
                                <div class="cart-symbol"></div>
                                <div id="month-income-preview" class="cart-preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="clo-md-3 col-lg-3 col-sm-6">
                <div class="card-box">
                    <div class="box-callout-orange">
                        <div class="rightside-cart">
                            <p class="card-head">Current Month Expense<br>
                                <canvas id="current-month-expense" height="100" width="160"></canvas>
                            <div class="cart-caption">
                                <div class="cart-symbol"></div>
                                <div id="month-expense-preview" class="cart-preview"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--End Card box-->

        <div class="row">

            <!--Start Income Vs Expense Chart-->
            <div class="col-md-5 col-sm-5 col-lg-5">
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Income Vs Expense - <?= date('M Y') ?></div>
                    <div class="panel-body">
                        <div id="inc_vs_exp"></div>

                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>
            <!--End Income Vs Expense Chart-->

            <!--Start Account Status-->
            <div class="col-md-7 col-sm-7 col-lg-7">
                <!--Start Panel-->
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Financial Balance Status - <?= date('M Y') ?></div>
                    <div class="panel-body financial-bal table-responsive">
                        <table class="table table-bordered">
                            <tr>
                                <th>Account</th>
                                <!-- <th class="width-30x">Date Updated</th> -->
                                <th class="col-amount text-end">Balance</th>
                            </tr>
                            <?php foreach ($financialBalance as $balance) { ?>
                                <tr id="<?php echo $balance->accounts_id ?>">
                                    <td>
                                        <?php echo $balance->account ?>
                                        <div class="date-updated-box">
                                            <span class="editSpan account_date"><?php echo $balance->updated ?><?= getDaysfromDate($balance->updated) ?></span>
                                            <div class='input-group editInput' id='date' style="display: none;">
                                                <input type='text' name="account_date" class="form-control account_date date" value="<?php echo $balance->updated ?>">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>


                                            <i class="editBtn float-right fa fa-retweet"></i>
                                            <button type="button" class="saveBtn" style="display: none;">Save</button>
                                            <button type="button" class="cancelBtn" style="display: none;">Cancel</button>
                                        </div>
                                    </td>
                                    <td class="text-end"><?php echo displayMoney($balance->balance); ?></td>
                                </tr>
                            <?php } ?>

                        </table>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>
            <!--End Account Status Col-->

            <!--Start Income Vs Expense Line Chart-->
            <div class="col-md-12 col-sm-12 col-lg-12">
                <!--Start Panel-->
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Income Vs Expense - <?= date('M Y') ?></div>
                    <div class="panel-body">
                        <!--<canvas id="inc_vs_exp2"></canvas>-->
                        <div id="inc_vs_exp2"></div>
                    </div>
                    <!--End Panel Body-->

                </div>
                <!--End Panel-->
            </div>
            <!--End Income Col-->


            <!--Start Income-->
            <div class="col-md-4 col-sm-4 col-lg-4">
                <!--Start Panel-->
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Latest 5 Income</div>
                    <div class="panel-body table-responsive">
                        <!--Income Table-->
                        <?= $latest_income ?>
                    </div>
                    <!--End Panel Body-->

                </div>
                <!--End Panel-->
            </div>
            <!--End Income Col-->

            <!--Start Transfer-->
            <div class="col-md-4 col-sm-4 col-lg-4">
                <!--Start Panel-->
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Latest 5 Transfer</div>
                    <div class="panel-body table-responsive">
                        <?= $latest_transfer ?>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>
            <!--End Transfer Col-->

            <!--Start Expense-->
            <div class="col-md-4 col-sm-4 col-lg-4">
                <!--Start Panel-->
                <div class="card panel panel-default custom-box">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Latest 5 Expense</div>
                    <div class="panel-body table-responsive">
                        <?= $latest_expense ?>
                    </div>
                    <!--End Panel Body-->
                </div>
                <!--End Panel-->
            </div>
            <!--End Expense Col-->
        </div>
        <!--End Row-->
    </div>
</section>
<!--End Main-content-->
<script src="<?php echo base_url() ?>/theme/js/gauge.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('focus', ".date", function() {
            $(this).flatpickr({
                enableTime: true,
                dateFormat: '<?= DATETIME_FORMAT_UI ?>',
                disableMobile: true, // Force Flatpickr on mobile devices
                time_24hr: false,
                formatDate: function(date, format) {
                    const padZero = (number) => String(number).padStart(2, '0');
                    const year = date.getFullYear();
                    const month = padZero(date.getMonth() + 1); // Months are zero-based
                    const day = padZero(date.getDate());
                    let hours = date.getHours() % 12 || 12; // Convert to 12-hour format
                    const minutes = padZero(date.getMinutes());
                    const ampm = date.getHours() >= 12 ? 'PM' : 'AM';
                    // Adding 07 instead of 7 in time hour
                    return `${year}-${month}-${day} ${padZero(hours)}:${minutes} ${ampm}`;
                }
            });
        });
        /*
        $('#info-upto-date').on('submit',function(event){ 
        $.ajax({
        method : "POST",
        url : "<?php //echo site_url('Admin/inforUptoDate') 
                ?>",
        data : $(this).serialize(),
        beforeSend : function(){
        //$(".block-ui").css('display','block'); 
        },success : function(data){  
        if(data=="true"){   
        	alert("Saved Sucessfully"); 
        }else{
        	alert(data);
        }   
        }
        });    
        return false;
        });*/

        $('.editBtn').on('click', function() {
            //hide edit span
            $(this).closest("tr").find(".editSpan").hide();

            //show edit input
            $(this).closest("tr").find(".editInput").show();

            //hide edit button
            $(this).closest("tr").find(".editBtn").hide();

            //show save button
            $(this).closest("tr").find(".saveBtn").show();

            //show cancel button
            $(this).closest("tr").find(".cancelBtn").show();

        });

        $('.saveBtn').on('click', function() {
            var trObj = $(this).closest("tr");
            var ID = $(this).closest("tr").attr('id');
            var inputData = $(this).closest("tr").find(".account_date").serialize();
            $.ajax({
                type: 'POST',
                url: 'manageAccount/editDate',
                dataType: "json",
                data: 'accounts_id=' + ID + '&' + inputData,
                success: function(response) {
                    if (response.status == 1) {
                        trObj.find(".editSpan.account_date").text(response.updated);

                        trObj.find(".editInput").hide();
                        trObj.find(".editSpan").show();
                        trObj.find(".saveBtn").hide();
                        trObj.find(".cancelBtn").hide();
                        trObj.find(".editBtn").show();
                        // trObj.find(".deleteBtn").show();
                    } else {
                        alert(response.msg);
                    }
                }
            });
        });

        $('.cancelBtn').on('click', function() {
            //hide & show buttons
            $(this).closest("tr").find(".saveBtn").hide();
            $(this).closest("tr").find(".cancelBtn").hide();
            $(this).closest("tr").find(".confirmBtn").hide();
            $(this).closest("tr").find(".editBtn").show();
            $(this).closest("tr").find(".deleteBtn").show();

            //hide input and show values
            $(this).closest("tr").find(".editInput").hide();
            $(this).closest("tr").find(".editSpan").show();
        });

        var chart;
        chart = c3.generate({
            bindto: '#inc_vs_exp2',
            data: {
                x: 'x',
                //        xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
                columns: [
                    ['x'
                        <?php for ($i = 1; $i <= count($line_chart[0]); $i++) {
                            echo ",";
                            echo "'" . $line_chart[0][$i]['date'] . "'";
                        } ?>

                    ],

                    ['Income',
                        <?php for ($i = 1; $i <= count($line_chart[0]); $i++) {
                            echo  $line_chart[0][$i]['amount'] . ",";
                        } ?>
                    ],


                    ['Expense',
                        <?php for ($i = 1; $i <= count($line_chart[1]); $i++) {
                            echo  $line_chart[1][$i]['amount'] . ",";
                        } ?>
                    ]
                ]
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m-%d'
                    }
                }
            }
        });



        chart = c3.generate({
            bindto: '#inc_vs_exp',
            data: {
                columns: [
                    ['Income', <?php echo $pie_data['income'] ?>],
                    ['Expense', <?php echo $pie_data['expense'] ?>],
                ],
                type: 'donut',
                onclick: function(d, i) {
                    // console.log("onclick", d, i);
                },
                onmouseover: function(d, i) {
                    // console.log("onmouseover", d, i);
                },
                onmouseout: function(d, i) {
                    // console.log("onmouseout", d, i);
                }
            },
            color: {
                pattern: ['#23c6c8', '#f39c12']
            },
            donut: {
                title: "Income VS Expense"
            }
        });
    });

    //Current Day Income Gauge init
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.40, // The line thickness
        pointer: {
            length: 0.8, // The radius of the inner circle
            strokeWidth: 0.035, // The rotation offset
            color: '#34495e' // Fill color
        },
        limitMax: 'false', // If true, the pointer will not go past the end of the gauge
        colorStart: '#23c6c8', // Colors
        colorStop: '#23c6c8', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: true
    };
    var target = document.getElementById('current-day-income'); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = <?php echo $cart_summery['current_day_income'] * 1.4 ?>; // set max gauge value
    gauge.animationSpeed = 100; // set animation speed (32 is default value)
    gauge.set(<?php echo $cart_summery['current_day_income'] ?>); // set actual value	
    gauge.setTextField(document.getElementById("current-income-preview"));

    //Current Day Expense Gauge init
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.40, // The line thickness
        pointer: {
            length: 0.8, // The radius of the inner circle
            strokeWidth: 0.035, // The rotation offset
            color: '#34495e' // Fill color
        },
        limitMax: 'false', // If true, the pointer will not go past the end of the gauge
        colorStart: '#f39c12', // Colors
        colorStop: '#f39c12', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: true
    };
    var target = document.getElementById('current-day-expense'); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = <?php echo $cart_summery['current_day_expense'] * 1.3 ?>; // set max gauge value
    gauge.animationSpeed = 100; // set animation speed (32 is default value)
    gauge.set(<?php echo $cart_summery['current_day_expense'] ?>); // set actual value	
    gauge.setTextField(document.getElementById("current-expense-preview"));

    //Current Month Income Gauge init
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.40, // The line thickness
        pointer: {
            length: 0.8, // The radius of the inner circle
            strokeWidth: 0.035, // The rotation offset
            color: '#34495e' // Fill color
        },
        limitMax: 'false', // If true, the pointer will not go past the end of the gauge
        colorStart: '#23c6c8', // Colors
        colorStop: '#23c6c8', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: true
    };
    var target = document.getElementById('current-month-income'); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = <?php echo $cart_summery['current_month_income'] * 1.4 ?>; // set max gauge value
    gauge.animationSpeed = 100; // set animation speed (32 is default value)
    gauge.set(<?php echo $cart_summery['current_month_income'] ?>); // set actual value	
    gauge.setTextField(document.getElementById("month-income-preview"));

    //Current Month Expense Gauge init
    var opts = {
        lines: 12, // The number of lines to draw
        angle: 0, // The length of each line
        lineWidth: 0.40, // The line thickness
        pointer: {
            length: 0.8, // The radius of the inner circle
            strokeWidth: 0.035, // The rotation offset
            color: '#34495e' // Fill color
        },
        limitMax: 'false', // If true, the pointer will not go past the end of the gauge
        colorStart: '#f39c12', // Colors
        colorStop: '#f39c12', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: true
    };
    var target = document.getElementById('current-month-expense'); // your canvas element
    var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = <?php echo $cart_summery['current_month_expense'] * 1.6 ?> // set max gauge value
    gauge.animationSpeed = 100; // set animation speed (32 is default value)
    gauge.set(<?php echo $cart_summery['current_month_expense'] ?>); // set actual value	
    gauge.setTextField(document.getElementById("month-expense-preview"));
</script>