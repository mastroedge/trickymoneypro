<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?= $this->Adminmodel->add_breadcrumb('Accounts'); ?>

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Manage Accounts</div>
                        <div class="panel-body manage-client">
                            <div class="add-button">
                                <button class="btn btn-primary mybtn btn-default asyn-link" onclick="showCommonModal('myCommonModal', '<?=site_url('Admin/manageAccount/addForm')?>')">Add Account</button>
                                <a class="btn btn-primary mybtn btn-default asyn-link" href="<?php echo site_url('Admin/addAccount') ?>">Add Account</a>
                            </div>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profiledis-tab" data-bs-toggle="tab" data-bs-target="#profiledis" type="button" role="tab" aria-controls="profiledis" aria-selected="false">Disable (display)</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="display-tab" data-bs-toggle="tab" data-bs-target="#display" type="button" role="tab" aria-controls="display" aria-selected="false">Disable</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Deleted</button>
                                </li>
                            </ul>

                            <!-- Tab Content -->
                            <div class="tab-content mt-3" id="myTabContent">
                                <div class="tab-pane table-responsive fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <?= $this->Displaymodel->getAccountTable($accounts, $fBalance, ACT_TYPE_ACT); ?>
                                </div>
                                <div class="tab-pane table-responsive fade" id="profiledis" role="tabpanel" aria-labelledby="profiledis-tab">
                                    <?= $this->Displaymodel->getAccountTable($accounts, $fBalance, ACT_TYPE_DIS_DIS); ?>
                                </div>
                                <div class="tab-pane table-responsive fade" id="display" role="tabpanel" aria-labelledby="display-tab">
                                    <?= $this->Displaymodel->getAccountTable($accounts, $fBalance, ACT_TYPE_DIS); ?>
                                </div>
                                <div class="tab-pane table-responsive fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <?= $this->Displaymodel->getAccountTable($accounts, $fBalance, ACT_TYPE_DEL); ?>
                                </div>
                            </div>

                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->

                </div>


            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->

<script type="text/javascript">
    $(document).ready(function() {
        $('.account-remove-btn').on('click', function() {
            var main = $(this);
            swal({
                title: "Are you sure Want To Delete?",
                text: "You will not be able to recover this Account Data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                ///////////////    
                var link = $(main).attr("href");
                $.ajax({
                    url: link,
                    beforeSend: function() {
                        $(".block-ui").css('display', 'block');
                    },
                    success: function(data) {
                        $(main).closest("tr").remove();
                        //sucessAlert("Remove Sucessfully");
                        $(".system-alert-box").empty();
                        swal("Deleted!", "Remove Sucessfully", "success");
                        $(".block-ui").css('display', 'none');
                    }
                });
            });
            return false;
        });

        // $('.account-edit-btn').on('click', function() {
        //     var link = $(this).attr("href");
        //     $.ajax({
        //         method: "POST",
        //         url: link,
        //         beforeSend: function() {
        //             $(".block-ui").css('display', 'block');
        //         },
        //         success: function(data) {
        //             //var link = location.pathname.replace(/^.*[\\\/]/, ''); //get filename only  
        //             history.pushState(null, null, link);
        //             $('.asyn-div').load(link + '/asyn', function() {
        //                 $(".block-ui").css('display', 'none');
        //             });

        //         }
        //     });

        //     return false;
        // });
    });
</script>