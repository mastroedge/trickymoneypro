<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Account'); ?>
        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->


        <div class="account-div">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><?= (isset($edit_account) ? 'Edit' : 'Add') ?> Account</div>
            <div class="panel-body add-client">
              <form id="add-accounts">
                <div class="row mx-0">
                  <input type="hidden" name="action" id="action" value="<?= (isset($edit_account) ? 'update' : 'insert') ?>" />
                  <input type="hidden" name="accounts_id" id="accounts_id" value="<?= (isset($edit_account) ? $edit_account->accounts_id : '') ?>" />
                  <div class="col-lg-5 col-md-5 col-sm-5 col-5 mb-3">
                    <label for="acc_name">Account Name</label>
                    <input type="text" class="form-control" name="accounts_name" id="acc_name" <?= (isset($edit_account) ? 'value="' . $edit_account->accounts_name . '"' : '') ?> autocomplete="off" required>
                  </div>
                  <?php if (!isset($edit_account)) { ?>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-2 mb-1">
                      <label for="balance">Balance</label>
                      <input type="text" class="form-control" name="opening_balance" id="balance" value="0" required>
                    </div>
                  <?php } ?>
                  <?php
                  $selmt = array(
                    'name' => 'account_type',
                    'options' => [
                      'Bank' => 'Bank',
                      'Credit Card' => 'Credit Card',
                      'Fixed Deposit' => 'Fixed Deposit',
                      'Mutual Fund' => 'Mutual Fund',
                      'Gold' => 'Gold',
                      'Wallet' => 'Wallet',
                      'Insurance' => 'Insurance'
                    ],
                    'js' => 'class="form-control"',
                    'divClass' => 'col-lg-2 col-md-2 col-sm-2 col-2 mb-3',
                    'slected' => (isset($edit_account) ? $edit_account->account_type : ''),
                    'isLabel' => true,
                    'label' => 'Active'
                  );
                  echo $this->Displaymodel->selectBox($selmt);
                  ?>
                  <div class='col-lg-2 col-md-2 col-sm-2 col-2 mb-3'>
                    <label for="note">Updated</label>
                    <input type='text' name="updated" class="form-control datewithtime" id="updated" value="<?= (isset($edit_account) ? $edit_account->updated : displayDate()) ?>">
                  </div>

                  <?php
                  $selmt = array(
                    'name' => 'active',
                    'options' => [
                      '1' => 'Active',
                      '2' => 'Disable (display)',
                      '0' => 'Disable',
                      '3' => 'Deleted'
                    ],
                    'js' => 'class="form-control"',
                    'divClass' => 'col-lg-2 col-md-2 col-sm-2 col-2 mb-3',
                    'slected' => (isset($edit_account) ? $edit_account->active : ''),
                    'isLabel' => true,
                    'label' => 'Active'
                  );
                  echo $this->Displaymodel->selectBox($selmt);
                  ?>
                  <div class="mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="Enter note" <?= (isset($edit_account) ? 'value="' . $edit_account->note . '"' : '') ?> required>
                  </div>

                  <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Save</button>
                </div>
              </form>

            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->

        </div>


      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->
<script type="text/javascript">
  $(document).ready(function() {
    if ($(".sidebar").width() == "0") {
      $(".main-content").css("padding-left", "0px");
    }

    $("#balance").keypress(function(e) {
      //if the letter is not digit then display error and don't type anything
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        return false;
      }
    });


    $('#add-accounts').on('submit', function() {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/addAccount/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            if ($("#action").val() != 'update') {
              $('#acc_name').val("");
              $("#balance").val("");
              $('#note').val("");
              $('#updated').val("");
              $('#active').val("1");
            }
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });

  });
</script>