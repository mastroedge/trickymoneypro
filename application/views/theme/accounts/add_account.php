<div id="ajax-message"></div>
<form id="add-vehicle-form">
  <input type="hidden" name="action" id="action" value="<?= (isset($single) ? "update" : 'insert') ?>" />
  <input type="hidden" name="accounts_id" id="accounts_id" value="<?= (isset($single) ? $single->accounts_id : '') ?>" />
  <div class="row mx-0">
    <?php
    $ielmt = array(
      'type' => 'text',
      'name' => 'accounts_name',
      'value' => (isset($single) ? $single->accounts_name : ''),
      'class' => '',
      'required' => 'required',
      'isLabel' => true,
      'label' => 'Account Name',
      'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
      'groupClass' => '',
      'labelClass' => '',
      'optionClass' => ''
    );
    echo $this->Displaymodel->textBox($ielmt);

    if (!isset($single)) {
      $ielmt = array(
        'type' => 'text',
        'name' => 'opening_balance',
        'value' => '0',
        'class' => '',
        'isLabel' => true,
        'label' => 'Opening Balance',
        'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
        'groupClass' => '',
        'labelClass' => '',
        'optionClass' => ''
      );
      echo $this->Displaymodel->textBox($ielmt);
    }

    $selmt = array(
      'name' => 'account_type',
      'options' => [
        'Bank' => 'Bank',
        'Credit Card' => 'Credit Card',
        'Fixed Deposit' => 'Fixed Deposit',
        'Mutual Fund' => 'Mutual Fund',
        'Gold' => 'Gold',
        'Wallet' => 'Wallet',
        'Insurance' => 'Insurance'
      ],
      'slected' => (isset($single) ? $single->account_type : ''),
      'js' => array('class' => 'select2sel type form-control', 'required' => 'required'),
      'isLabel' => true,
      'label' => 'Account Name',
      'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
      'labelClass' => '',
      'optionClass' => ''
    );
    echo $this->Displaymodel->selectBox($selmt);

    $ielmt = array(
      'type' => 'text',
      'name' => 'updated',
      'value' => (isset($single) ? $single->updated : displayDate()),
      'class' => 'datewithtimebulk',
      'required' => 'required',
      'isLabel' => true,
      'label' => 'Updated',
      'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
      'groupClass' => '',
      'labelClass' => '',
      'optionClass' => ''
    );
    echo $this->Displaymodel->textBox($ielmt);

    $selmt = array(
      'name' => 'active',
      'options' => [
        '1' => 'Active',
        '2' => 'Disable (display)',
        '0' => 'Disable',
        '3' => 'Deleted'
      ],
      'slected' => (isset($single) ? $single->active : ''),
      'js' => array('class' => 'select2sel type form-control', 'required' => 'required'),
      'isLabel' => true,
      'label' => 'Active',
      'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6 mb-3',
      'labelClass' => '',
      'optionClass' => ''
    );
    echo $this->Displaymodel->selectBox($selmt);

    $ielmt = array(
      'type' => 'text',
      'name' => 'note',
      'value' => (isset($single) ? $single->note : ''),
      'class' => '',
      'required' => 'required',
      'isLabel' => true,
      'label' => 'Note',
      'divClass' => 'col-md-12 col-lg-12 col-sm-12 col-12 mb-5',
      'groupClass' => '',
      'labelClass' => '',
      'optionClass' => ''
    );
    echo $this->Displaymodel->textBox($ielmt);
    ?>

    <button type="button" class="btn btn-primary" onclick="addDataToDB('add-vehicle-form', '<?= site_url('Admin/manageAccount/insert') ?>')"><i class="fa fa-check"></i> <?= (isset($single) ? "Edit" : 'Add') ?></button>
  </div>
</form>