<!--Statt Main Content-->
<section>
    <div class="main-content">
        <div class="inner-contatier">
            <div class="row">
                <?=$this->Adminmodel->add_breadcrumb('Bulk Upload');?>

                <!--Alert-->
                <div class="system-alert-box sticky-alert">
                    <div class="alert alert-success ajax-notify"></div>
                </div>
                <!--End Alert-->

                <div class="col-md-12 col-lg-12 col-sm-12">
                    <!--Start Panel-->
                    <div class="card panel panel-default">
                        <!-- Default panel contents -->
                        <div class="panel-heading">Bulk Transaction </div>
                        <div class="panel-body">
                            <?= $form_html ?>
                        </div>
                        <!--End Panel Body-->
                    </div>
                    <!--End Panel-->
                </div>

                <!-- Start Table Section-->

            </div><!--End Inner container-->
        </div><!--End Row-->
    </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->

<script src="<?= base_url('theme/js/custom_bulk_transaction.js') ?>"></script>