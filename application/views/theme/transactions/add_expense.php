<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Expense'); ?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <div class="col-md-4 col-lg-4 col-sm-5 mb-3">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Add Expense</div>
            <div class="panel-body add-client">
              <form id="add-expense">
                <input type="hidden" name="action" id="action" value="insert" />
                <input type="hidden" name="trans_id" id="trans_id" value="" />
                <div class="row mx-0">
                  <?php
                  $accountArray = $this->Adminmodel->resultsObjectToArray($accounts, 'accounts_name', 'accounts_name');
                  $selmt = array(
                    'name' => 'accounts_name',
                    'options' => $accountArray,
                    'slected' => DEFAULT_ACCOUNT_NAME,
                    'js' => 'class="select2sel account form-control"',
                    'isLabel' => true,
                    'label' => 'Account Name',
                    'divClass' => 'col-md-6 col-lg-6 col-sm-6 col-6',
                    'labelClass' => '',
                    'optionClass' => ''
                  );
                  echo $this->Displaymodel->selectBox($selmt);
                  ?>
                  <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <label for="date">Date</label>
                    <div class='input-group date' id='date'>
                      <input type='text' name="expense-date" id="expense-date" class="form-control datewithtime" value="<?= displayDate() ?>" />
                      <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </span>
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <label for="expense-type">Expense Type</label>
                    <select name="expense-type" class="select2sel form-control" id="expense-type">
                      <?php foreach ($category as $cat) { ?>
                        <option value="<?php echo $cat->accounts_name ?>" <?= ($cat->accounts_name == 'Groceries' ? 'selected' : '') ?>><?php echo $cat->accounts_name ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <label for="amount">Amount</label>
                    <div class='input-group'>
                      <div class="input-group-addon">₹</div>
                      <input type="number" name="amount" id="amount" class="form-control amount" step="0.01" inputmode="decimal" />
                    </div>
                  </div>

                  <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <label for="payer">Payee</label>
                    <select name="payee" class="select2sel form-control" id="payee">
                      <?php foreach ($payee as $p) { ?>
                        <option value="<?php echo $p->payee_payers ?>" <?= ($p->payee_payers == 'Walk-in' ? 'selected' : '') ?>><?php echo $p->payee_payers ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="col-md-6 col-lg-6 col-sm-6 col-6">
                    <label for="p_method">Payment Method</label>
                    <select name="p_method" class="select2sel form-control" id="p_method">
                      <?php foreach ($p_method as $method) { ?>
                        <option value="<?php echo $method->p_method_name ?>" <?= ($method->p_method_name == 'Cash' ? 'selected' : '') ?>><?php echo $method->p_method_name ?></option>
                      <?php } ?>
                    </select>
                  </div>

                  <div class="mb-3">
                    <label for="reference">Reference No</label>
                    <input type="text" class="form-control" name="reference" id="reference" maxlength="<?= REF_MAX_LEN ?>" placeholder="Enter text (max <?= REF_MAX_LEN ?> characters)" autocomplete="off">
                  </div>

                  <div class="mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" name="note" id="note" placeholder="Enter note" autocomplete="off">
                  </div>

                </div>
                <button type="submit" class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>
              </form>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>

        <!-- Start Table Section-->

        <div class="col-md-8 col-lg-8 col-sm-7">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Expense</div>
            <div class="panel-body table-responsive">
              <?= $t_data ?>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>


      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->


<script type="text/javascript">
  $(document).ready(function() {

    $(".asyn-expense").addClass("active-menu");
    $(".select2sel").select2();

    $('#amount').keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
        (event.which < 48 || event.which > 57)) {
        event.preventDefault();
      }
    });


    $('#add-expense').on('submit', function(event) {
      $.ajax({
        method: "POST",
        url: "<?php echo site_url('Admin/addExpense/insert') ?>",
        data: $(this).serialize(),
        beforeSend: function() {
          $(".block-ui").css('display', 'block');
        },
        success: function(data) {
          if (data == "true") {
            sucessAlert("Saved Sucessfully");
            $(".block-ui").css('display', 'none');
            location.reload(); // Reloads the current page
          } else {
            failedAlert2(data);
            $(".block-ui").css('display', 'none');
          }
        }
      });
      return false;
    });

  });
</script>
<script src="<?php echo base_url() ?>/theme/js/custom_income_expense.js"></script>