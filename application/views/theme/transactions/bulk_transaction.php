<!--Statt Main Content-->
<section>
  <div class="main-content">
    <div class="inner-contatier">
      <div class="row">
        <?= $this->Adminmodel->add_breadcrumb('Bulk Upload'); ?>

        <!--Alert-->
        <div class="system-alert-box sticky-alert">
          <div class="alert alert-success ajax-notify"></div>
        </div>
        <!--End Alert-->

        <div class="col-md-12 col-lg-12 col-sm-12">
          <!--Start Panel-->
          <div class="card panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">Bulk Transaction </div>
            <div class="panel-body">
            <div class="add-button"><a class="btn btn-primary mybtn btn-success" href="<?=site_url('Admin/bulkTransactionView/') ?>"><i class="fa fa-plus"></i> Add</a></div>
            <hr> 
            <!-- Nav tabs -->
              <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                  <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Active</button>
                </li>
                <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile1-tab" data-bs-toggle="tab" data-bs-target="#profile1" type="button" role="tab" aria-controls="profile1" aria-selected="false">Disabled</button>
                </li>
              </ul>

              <!-- Tab Content -->
              <div class="tab-content mt-3" id="myTabContent">
                <div class="tab-pane table-responsive fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                  <?= $this->Displaymodel->getBulkTransactionListTable($allbulktrans, STATUS_ACTIVE); ?>
                </div>
                <div class="tab-pane table-responsive fade" id="profile1" role="tabpanel" aria-labelledby="profile1-tab">
                  <?= $this->Displaymodel->getBulkTransactionListTable($allbulktrans, STATUS_DISABLE); ?>
                </div>
              </div>
            </div>
            <!--End Panel Body-->
          </div>
          <!--End Panel-->
        </div>

        <!-- Start Table Section-->

      </div><!--End Inner container-->
    </div><!--End Row-->
  </div><!--End Main-content DIV-->
</section><!--End Main-content Section-->

<script type="text/javascript">
  $(document).ready(function() {
    $(document).on('click', '.bulk-trans-remove-btn', function() {
      var main = $(this);
      swal({
        title: "Are you sure Want To Delete?",
        text: "You will not be able to recover this Data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      }, function() {
        ///////////////      
        var link = $(main).attr("href");
        $.ajax({
          url: link,
          beforeSend: function() {
            $(".block-ui").css('display', 'block');
          },
          success: function(data) {
            $(main).closest("tr").remove();
            //sucessAlert("Remove Sucessfully"); 
            $(".system-alert-box").empty();
            swal("Deleted!", "Sucessfully", "success");
            $(".block-ui").css('display', 'none');
          }
        });
      });
      return false;
    });

  });
</script>