<?php
class Payee_payers_model extends CI_Model {
    protected $table = 'payee_payers';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_payee_payers() {
        return $this->db->get($this->table)->result();
    }

    public function insert_payee_payer($data) {
        return $this->db->insert($this->table, $data);
    }

    /**
     * Fetch a record by ID
     * 
     * @param int $id The ID of the record to fetch
     * @return object|null The record object if found, null otherwise
     */
    public function get_by_id($id) {
        // Fetch record by ID
        $this->db->where('trace_id', $id);
        $query = $this->db->get('payee_payers');

        // Check if the record exists
        if ($query->num_rows() > 0) {
            return $query->row(); // Return a single record object
        }

        return null; // Return null if no record found
    }
}
