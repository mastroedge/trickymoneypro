<?php
class Bulk_transaction_model extends CI_Model
{
    protected $table = 'bulk_transaction';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_bulk_transactions()
    {
        return $this->db->get($this->table)->result();
    }

    public function insert_bulk_transaction($data)
    {
        return $this->db->insert($this->table, $data);
    }

    /**
     * Delete Bulk Trnasaction By Id
     */
    public function deleteBulkTransactionById($id)
    {
        $this->db->delete('bulk_transaction', array(
            'trans_id' => $id
        ));
    }

    public function changeStatus($id, $status)
    {
        $this->db->where('trans_id', $id);
        $this->db->update('bulk_transaction', array(
            'status' => $status
        ));
    }

    /**
     * get All Bulk Transaction
     */
    public function getAllBulkTransaction($limit = 1000)
    {
        $this->db->select('*');
        $this->db->from('bulk_transaction');
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->limit($limit);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getAllBulkTransactionList($limit = 10)
    {
        // Manually construct the SQL query
        $sql = "(SELECT * FROM bulk_transaction WHERE status = 1)
            UNION
            (SELECT * FROM bulk_transaction WHERE status = 0 LIMIT ".$limit.")
            ";

        // Execute the query
        $query = $this->db->query($sql);

        // Fetch results
        $results = $query->result();
        return $results;
    }

    /**
     * get Bulk Transaction By Id
     */
    public function getBulkTransactionById($id)
    {
        $this->db->select('*');
        $this->db->from('bulk_transaction');
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->where('trans_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
}
