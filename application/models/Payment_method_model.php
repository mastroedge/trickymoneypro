<?php
class Payment_method_model extends CI_Model {
    protected $table = 'payment_method';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_payment_methods() {
        return $this->db->get($this->table)->result();
    }

    public function insert_payment_method($data) {
        return $this->db->insert($this->table, $data);
    }

    // get all payment method
    public function getAllPaymentmethod($status = 'ALL', $sort = 'ASC')
    {
        $this->db->select('*');
        $this->db->from('payment_method');
        if($status != 'ALL') {
            $this->db->where("status", $status);
        }
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->order_by("p_method_name", $sort);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
}
