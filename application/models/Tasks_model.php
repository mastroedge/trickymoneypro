<?php
class Tasks_model extends CI_Model {
    protected $table = 'tasks';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_tasks() {
        return $this->db->get($this->table)->result();
    }

    public function insert_task($data) {
        return $this->db->insert($this->table, $data);
    }

    public function get_task_by_id($task_id) {
        return $this->db->get_where($this->table, ['task_id' => $task_id])->row();
    }
}
