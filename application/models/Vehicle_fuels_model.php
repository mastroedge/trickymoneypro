<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle_fuels_model extends CI_Model
{

    private $table = 'vehicle_fuels';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($fuel_id)
    {
        return $this->db->get_where($this->table, ['fuel_id' => $fuel_id])->row();
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($fuel_id, $data)
    {
        return $this->db->where('fuel_id', $fuel_id)->update($this->table, $data);
    }

    public function delete($fuel_id)
    {
        return $this->db->where('fuel_id', $fuel_id)->delete($this->table);
    }

    // Get all fuel records for a specific vehicle
    public function get_fuel_records_by_vehicle($vehicle_id)
    {
        $this->db->where('vehicle_id', $vehicle_id);
        $this->db->order_by('fill_date', 'DESC');
        return $this->db->get($this->table)->result();
    }

    // Get a single fuel record by ID
    public function get_fuel_record($fuel_id)
    {
        $this->db->where('fuel_id', $fuel_id);
        return $this->db->get($this->table)->row();
    }

    // Update a fuel record
    public function update_fuel_record($fuel_id, $data)
    {
        $this->db->where('fuel_id', $fuel_id);
        return $this->db->update($this->table, $data);
    }

    // Delete a fuel record
    public function delete_fuel_record($fuel_id)
    {
        $this->db->where('fuel_id', $fuel_id);
        return $this->db->delete($this->table);
    }

    // Get total fuel consumption for a vehicle
    public function get_total_fuel_consumed($vehicle_id)
    {
        $this->db->select_sum('fuel_liters');
        $this->db->where('vehicle_id', $vehicle_id);
        $query = $this->db->get($this->table);
        return $query->row()->fuel_liters;
    }

    // Get total fuel expenses for a vehicle
    public function get_total_fuel_cost($vehicle_id)
    {
        $this->db->select_sum('total_cost');
        $this->db->where('vehicle_id', $vehicle_id);
        $query = $this->db->get($this->table);
        return $query->row()->total_cost;
    }

    public function get_vehicle_fuels_with_details($fuels, $v_list_obj, $t_list_obj)
    {

        foreach ($fuels as $fuel) {
            // Ensure trip data exists
            if (!isset($t_list_obj[$fuel->trip_id])) {
                continue;
            }

            $trip = $t_list_obj[$fuel->trip_id];

            // Ensure vehicle data exists
            if (!isset($v_list_obj[$trip->vehicle_id])) {
                continue;
            }

            $vehicle = $v_list_obj[$trip->vehicle_id];

            $fuel->vehicle_name = $vehicle->name;
            $fuel->fill_date = $trip->trip_date;
            $fuel->meter_reading = $trip->meter_reading;
            $fuel->fuel_percentage = $trip->fuel_percentage;
            $fuel->total_cost = ($fuel->fuel_liters * $fuel->fuel_price);
            $fuel->note = $trip->note;
        }

        return $fuels;
    }
}
