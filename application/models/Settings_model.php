<?php
class Settings_model extends CI_Model {
    protected $table = 'settings';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_settings() {
        return $this->db->get($this->table)->result();
    }

    public function update_setting($id, $data) {
        return $this->db->update($this->table, $data, ['id' => $id]);
    }
}
