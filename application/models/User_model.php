<?php
class User_model extends CI_Model {
    protected $table = 'user';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_users() {
        return $this->db->get($this->table)->result();
    }

    public function get_user_by_id($user_id) {
        return $this->db->get_where($this->table, ['user_id' => $user_id])->row();
    }

    public function insert_user($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update_user($user_id, $data) {
        return $this->db->update($this->table, $data, ['user_id' => $user_id]);
    }

    public function delete_user($user_id) {
        return $this->db->delete($this->table, ['user_id' => $user_id]);
    }

    public function login($email, $password)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $query = $this->db->get();
        $row_count = $query->num_rows();
        if ($row_count > 0) {
            $userdata = $query->row();
            $newdata = array(
                'user_id' => $userdata->user_id,
                'username' => $userdata->user_name,
                'fullname' => $userdata->fullname,
                'user_type' => $userdata->user_type,
                'email' => $userdata->email,
                'settings' => array(
                    'currency_code' => get_current_setting('currency_code'),
                ),
                'logged_in' => TRUE
            );
            $this->session->set_userdata($newdata);
            $this->setLoginTime($userdata->user_id);
            return true;
        }
        return false;
    }

    public function logout()
    {
        $newdata = array(
            'user_id' => '',
            'username' => '',
            'fullname' => '',
            'email' => '',
            'settings' => '',
            'logged_in' => FALSE
        );
        $this->session->set_userdata($newdata);
    }

    public function setLoginTime($user_id)
    {
        $data['last_login'] = date(DATETIME_FORMAT_DB);
        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data);
    }
}
