<?php

class DisplayModel extends CI_Model
{

    /**
     * Build Select Box
     * array(
     *   'name' => '',
     *   'options' => '',
     *   'slected' => '', // optional
     *   'js' => '', // optional
     *   'isLabel' => false, // optional
     *   'label' => '', // optional
     *   'divClass' => '', // optional
     *   'labelClass' => '', // optional
     *   'optionClass' => '' // optional
     * )
     */
    function selectBox($ielmt)
    {
        $name = $ielmt['name'] ?? "";
        $options = $ielmt['options'] ?? [];
        $slected = $ielmt['slected'] ?? "";
        $jsAttributes = $ielmt['js'] ?? [];
        $isLabel = $ielmt['isLabel'] ?? false;
        $label = $ielmt['label'] ?? "";
        $divClass = $ielmt['divClass'] ?? "";
        $labelClass = $ielmt['labelClass'] ?? "";
        $optionClass = $ielmt['optionClass'] ?? "";
        $placeholder = $ielmt['placeholder'] ?? null;
        $error = $ielmt['error'] ?? null;

        $js = '';
        $isRequired = false;
        if (is_array($jsAttributes)) {
            foreach ($jsAttributes as $key => $value) {
                $js .= $key . '="' . $value . '" ';
                if ($key == 'required' && ($value == 'required' || $value == 'true')) {
                    $isRequired = true;
                }
            }
        } else {
            $js = $jsAttributes;
        }

        $html = '';
        $html .= '<div class="form-group ' . $divClass . '">';
        if ($isLabel) {
            $html .= '<div class="' . $labelClass . '">';
            $html .= '<label for="' . $name . '">' . $label . ($isRequired ? ' <span class="text-danger">*</span>' : '') . '</label>';
            $html .= '</div>';
        }

        if (empty($options)) {
            $html .= '<span class="text-danger">No options available</span>';
            return $html;
        }

        $html .= '<div class="' . $optionClass . '">';
        $html .= '<select name="' . $name . '" ' . $js . '>';
        if ($placeholder) {
            $html .= '<option value="">' . $placeholder . '</option>';
        }
        foreach ($options as $key => $value) {
            if (is_array($value)) {
                $html .= '<optgroup label="' . htmlspecialchars($key) . '">';
                foreach ($value as $subKey => $subValue) {
                    $selected = ($subKey == $slected) ? 'selected' : '';
                    $html .= '<option value="' . htmlspecialchars($subKey) . '" ' . $selected . '>' . htmlspecialchars($subValue) . '</option>';
                }
                $html .= '</optgroup>';
            } else {
                $selected = ($key == $slected) ? 'selected' : '';
                $html .= '<option value="' . htmlspecialchars($key) . '" ' . $selected . '>' . htmlspecialchars($value) . '</option>';
            }
        }
        $html .= '</select>';
        $html .= '</div>';

        if ($error) {
            $html .= '<span class="text-danger">' . $error . '</span>';
        }

        $html .= '</div>';

        return $html;
    }

    /**
     * Build Check Box
     */
    function checkBox($ielmt)
    {
        $name = $ielmt['name'] ?? "";
        $value = $ielmt['value'] ?? "1";
        $class = $ielmt['class'] ?? "";
        $id = $ielmt['id'] ?? "";
        $isLabel = $ielmt['isLabel'] ?? false;
        $label = $ielmt['label'] ?? "";
        $divClass = $ielmt['divClass'] ?? "";
        $labelClass = $ielmt['labelClass'] ?? "";
        $checked = $ielmt['checked'] ?? false;
        $error = $ielmt['error'] ?? "";
        $extraAttributes = $ielmt['extraAttributes'] ?? [];
        $labelAttributes = $ielmt['labelAttributes'] ?? "";

        $data = [
            'name' => $name,
            'id' => $id,
            'class' => $class,
            'value' => $value,
            'checked' => $checked,
        ];

        foreach ($extraAttributes as $key => $val) {
            $data[$key] = $val;
        }

        $html = '';
        if ($isLabel) {
            $html .= '<div class="form-group ' . $divClass . '">';
            $html .= '<div class="' . $labelClass . '">';
            $html .= '<label for="' . $id . '" ' . $labelAttributes . '>' . $label . '</label>';
            $html .= '</div>';
        }

        $html .= form_checkbox($data);

        if ($error) {
            $html .= '<span class="text-danger">' . $error . '</span>';
        }

        if ($isLabel) {
            $html .= '</div>';
        }

        return $html;
    }

    /**
     * Build Text Box
     * array(
     *   'type' => '',
     *   'name' => '',
     *   'value' => '', // optional
     *   'class' => '', // optional
     *   'isLabel' => false, // optional
     *   'label' => '', // optional
     *   'divClass' => '', // optional
     *   'groupClass' => '', // optional
     *   'labelClass' => '', // optional
     *   'optionClass' => '' // optional
     * )
     */
    function textBox($ielmt)
    {
        $type = $ielmt['type'] ?? "text";
        $name = $ielmt['name'] ?? "";
        $value = $ielmt['value'] ?? "";
        $class = $ielmt['class'] ?? "";
        $required = $ielmt['required'] ?? "";
        $isLabel = $ielmt['isLabel'] ?? false;
        $label = $ielmt['label'] ?? "";
        $divClass = $ielmt['divClass'] ?? "";
        $groupClass = $ielmt['groupClass'] ?? "";
        $labelClass = $ielmt['labelClass'] ?? "";
        $maxlength = $ielmt['maxlength'] ?? "";
        $addonBefore = $ielmt['addonBefore'] ?? "";
        $addonAfter = $ielmt['addonAfter'] ?? "";
        $placeholder = $ielmt['placeholder'] ?? "";
        $autocomplete = $ielmt['autocomplete'] ?? "off";
        $error = $ielmt['error'] ?? "";

        $data = [
            'type' => $type,
            'class' => 'form-control ' . $class,
            'name' => $name,
            'value' => $value,
            'required' => $required,
            'maxlength' => $maxlength,
            'placeholder' => $placeholder,
            'autocomplete' => $autocomplete
        ];

        if ($type === 'decimal') {
            $data['type'] = 'number';
            $data['step'] = '0.01';
            $data['inputmode'] = 'decimal';
        }

        $isRequired = false;
        if (array_key_exists('required', $data) && $data['required'] === 'required') {
            $isRequired = true;
        }

        $html = '';
        if ($isLabel) {
            $html .= '<div class="form-group ' . $divClass . '">';
            $html .= '<label for="' . $name . '" class="' . $labelClass . '">' . $label . ($isRequired ? ' <span class="text-danger">*</span>' : '') . '</label>';
        }

        if ($addonBefore || $addonAfter) {
            $html .= '<div class="input-group ' . $groupClass . '">';
            if ($addonBefore) {
                $html .= '<div class="input-group-prepend"><span class="input-group-text">' . $addonBefore . '</span></div>';
            }
        }

        $html .= form_input($data);

        if ($addonAfter) {
            $html .= '<div class="input-group-append"><span class="input-group-text">' . $addonAfter . '</span></div>';
        }

        if ($addonBefore || $addonAfter) {
            $html .= '</div>';
        }

        if ($error) {
            $html .= '<span class="text-danger">' . $error . '</span>';
        }

        if ($isLabel) {
            $html .= '</div>';
        }

        return $html;
    }

    public function getTransactionBox($type, $obj)
    {
        $accounts = $this->Adminmodel->resultsObjectToArray($this->Adminmodel->getAllAccounts(1), 'accounts_name', 'accounts_name');
        $p_method = $this->Adminmodel->resultsObjectToArray($this->Payment_method_model->getAllPaymentmethod(STATUS_ACTIVE), 'p_method_name', 'p_method_name');
        $form_html = '';
        $form_html .= '<fieldset class="row split-box ' . strtolower($type) . '">';
        $form_html .= '<legend>' . $type . '</legend>';
        $form_html .= '<div class="add-button">';
        $form_html .= '<a class="close"><i class="fa fa-times-circle"></i></a>';
        $form_html .= '<a class="up"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>';
        $form_html .= '<a class="down"><i class="fa fa-arrow-down" aria-hidden="true"></i></a>';
        $form_html .= '<a onClick="addMoreBox(\'' . $type . '\', this)"><i class="fa fa-clone" aria-hidden="true"></i></a>';
        $form_html .= '</div>';
        $ielmt = array(
            'type' => 'hidden',
            'name' => 'type[]',
            'value' => $type,
            'class' => 'type'
        );
        $form_html .= $this->textBox($ielmt);

        if (!empty($obj) && $type == TYPE_TRANSFER && !empty($obj->getAccountsName())) {
            $accounts_name_transfer = split_account_by_delimiter($obj->getAccountsName());
            $accounts_name =  $accounts_name_transfer[0];
            $accounts_name_to =  $accounts_name_transfer[1];
        } else if (!empty($obj)) {
            $accounts_name = $obj->getAccountsName();
        }
        $selmt = array(
            'name' => 'accounts_name[]',
            'options' => $accounts,
            'slected' => (!empty($obj) && !empty($accounts_name) ? $accounts_name : DEFAULT_ACCOUNT_NAME),
            'js' => 'class="select2sel account form-control"',
            'isLabel' => true,
            'label' => 'Account Name',
            'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
            'labelClass' => '',
            'optionClass' => ''
        );
        $form_html .= $this->selectBox($selmt);
        if ($type == TYPE_TRANSFER) {
            $selmt = array(
                'name' => 'account_to[]',
                'options' => $accounts,
                'slected' => (!empty($obj) && !empty($accounts_name_to) ? $accounts_name_to : ''),
                'js' => 'class="select2sel account form-control"',
                'isLabel' => true,
                'label' => 'Account To',
                'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
                'labelClass' => '',
                'optionClass' => ''
            );
            $form_html .= $this->selectBox($selmt);
        } else {
            $ielmt = array(
                'type' => 'hidden',
                'name' => 'account_to[]',
                'value' => 'NILL'
            );
            $form_html .= $this->textBox($ielmt);
        }
        $ielmt = array(
            'type' => 'text',
            'name' => 'income_expense_date[]',
            'value' => (!empty($obj) && !empty($obj->getTransDate()) ? $obj->getTransDate() : displayDate()),
            'class' => 'income_expense_date datewithtimebulk',
            'isLabel' => true,
            'label' => 'Date',
            'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
            'groupClass' => 'date',
            'labelClass' => '',
            'optionClass' => ''
        );
        $form_html .= $this->textBox($ielmt);

        if ($type == TYPE_INCOME) {
            // Income
            $category = $this->Adminmodel->getAllChartOfAccountsSplit();
            $income_category = $this->Adminmodel->resultsObjectToArray($category[TYPE_INCOME], 'accounts_name', 'accounts_name');
            $selmt = array(
                'name' => 'income_expense_type[]',
                'options' => $income_category,
                'slected' => (!empty($obj) && !empty($obj->getCategory()) ? $obj->getCategory() : 'Petty Cash'),
                'js' => 'class="select2sel income_expense_type form-control"',
                'isLabel' => true,
                'label' => 'Income Type',
                'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
                'labelClass' => '',
                'optionClass' => ''
            );
            $form_html .= $this->selectBox($selmt);
        } else if ($type == TYPE_EXPENSE) {
            // Expense
            $category = $this->Adminmodel->getAllChartOfAccountsSplit();
            $expense_category = $this->Adminmodel->resultsObjectToArray($category[TYPE_EXPENSE], 'accounts_name', 'accounts_name');
            $selmt = array(
                'name' => 'income_expense_type[]',
                'options' => $expense_category,
                'slected' => (!empty($obj) && !empty($obj->getCategory()) ? $obj->getCategory() : 'Groceries'),
                'js' => 'class="select2sel income_expense_type form-control"',
                'isLabel' => true,
                'label' => 'Expense Type',
                'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
                'labelClass' => '',
                'optionClass' => ''
            );
            $form_html .= $this->selectBox($selmt);
        } else {
            $ielmt = array(
                'type' => 'hidden',
                'name' => 'income_expense_type[]',
                'value' => 'NILL'
            );
            $form_html .= $this->textBox($ielmt);
        }

        $ielmt = array(
            'type' => 'decimal',
            'name' => 'amount[]',
            'value' => (!empty($obj) ? $obj->getAmount() : ''),
            'class' => 'amount',
            'isLabel' => true,
            'label' => 'Amount',
            'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
            'groupClass' => '',
            'labelClass' => '',
            'optionClass' => ''
        );
        $form_html .= $this->textBox($ielmt);

        if ($type == TYPE_INCOME) {
            $payers = $this->Adminmodel->resultsObjectToArray($this->Adminmodel->getPayerAndPayee('Payer', STATUS_ACTIVE), 'payee_payers', 'payee_payers');
            $selmt = array(
                'name' => 'payee_payers[]',
                'options' => $payers,
                'slected' => (!empty($obj) && !empty($obj->getPayer()) ? $obj->getPayer() : DEFAULT_PAYER),
                'js' => 'class="select2sel payer form-control"',
                'isLabel' => true,
                'label' => 'Payer',
                'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
                'labelClass' => '',
                'optionClass' => ''
            );
            $form_html .= $this->selectBox($selmt);
        } else if ($type == TYPE_EXPENSE) {
            $payee = $this->Adminmodel->resultsObjectToArray($this->Adminmodel->getPayerAndPayee('Payee', STATUS_ACTIVE), 'payee_payers', 'payee_payers');
            $selmt = array(
                'name' => 'payee_payers[]',
                'options' => $payee,
                'slected' => (!empty($obj) && !empty($obj->getPayee()) ? $obj->getPayee() : DEFAULT_PAYEE),
                'js' => 'class="select2sel payee form-control"',
                'isLabel' => true,
                'label' => 'Payee',
                'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
                'labelClass' => '',
                'optionClass' => ''
            );
            $form_html .= $this->selectBox($selmt);
        } else {
            $ielmt = array(
                'type' => 'hidden',
                'name' => 'payee_payers[]',
                'value' => 'NILL'
            );
            $form_html .= $this->textBox($ielmt);
        }

        $selmt = array(
            'name' => 'p_method[]',
            'options' => $p_method,
            'slected' => (!empty($obj) && !empty($obj->getPMethod()) ? $obj->getPMethod() : DEFAULT_PAYMENT_METHOD),
            'js' => 'class="select2sel p_method form-control"',
            'isLabel' => true,
            'label' => 'Payment Method',
            'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-6',
            'labelClass' => '',
            'optionClass' => ''
        );
        $form_html .= $this->selectBox($selmt);
        $ielmt = array(
            'type' => 'text',
            'name' => 'reference[]',
            'value' => (!empty($obj) ? $obj->getRef() : ''),
            'class' => 'reference',
            'isLabel' => true,
            'label' => 'Reference No',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6',
            'groupClass' => 'col-md-12 col-lg-12 col-sm-12',
            'maxlength' => REF_MAX_LEN
        );
        $form_html .= $this->textBox($ielmt);
        $ielmt = array(
            'type' => 'text',
            'name' => 'note[]',
            'value' => (!empty($obj) ? $obj->getNote() : ''),
            'class' => 'note',
            'isLabel' => true,
            'label' => 'Note',
            'divClass' => 'col-md-6 col-lg-6 col-sm-6',
            'groupClass' => 'col-md-12 col-lg-12 col-sm-12'
        );
        $form_html .= $this->textBox($ielmt);

        // TODO: Add Functionality 
        // $form_html .= '<fieldset class="split-box inner">';
        // $form_html .= $this->textBox('text', (!empty($obj) ? $obj->slno : "0").'_amount[]', (!empty($obj) ? $obj->amount : ''), 'amount', true, 'Amount', 'col-md-3 col-lg-3 col-sm-3 col-6');
        // if ($type == TYPE_INCOME) {
        //     $form_html .= $this->selectBox((!empty($obj) ? $obj->slno : "0").'_payee_payers[]', $payers, (!empty($obj) ? $obj->payer : 'Anonymous'), 'class="select2sel payer form-control"', true, 'Payer', 'col-md-4 col-lg-4 col-sm-4');
        // } else if ($type == TYPE_EXPENSE) {
        //     $form_html .= $this->selectBox((!empty($obj) ? $obj->slno : "0").'_payee_payers[]', $payee, (!empty($obj) ? $obj->payee : 'Walk-in'), 'class="select2sel payee form-control"', true, 'Payee', 'col-md-4 col-lg-4 col-sm-4');
        // } else {
        //     $form_html .= $this->textBox('hidden', (!empty($obj) ? $obj->slno : "0").'_payee_payers[]', 'NILL');
        // }
        // $form_html .= '</fieldset>';

        $form_html .= '</fieldset>';
        return $form_html;
    }

    /**
     * Dsiplay Table for Income / Expense / Transfer
     * with options
     * array(
     *       "t_data" => [],
     *       "lend_array" => [],
     *       "show_total" => false,
     *       "show_balance" => true,
     *       "show_amount" => true,
     *       "show_class" => false,
     *       "show_action" => false,
     *       "show_checkbox" => true,
     *       "show_credit_debit" => true,
     *       "show_type" => '' // cr / dr
     *       "show_calculations" => false,
     *      );
     */
    // TODO:: check transfer table
    public function getTransactionTable($tdata)
    {

        $isMobile = false;
        $deviceType = getDeviceType();
        if ($deviceType === 'Mobile') {
            $isMobile = true;
        }

        // INPUTS
        $col_span = 2;
        $t_type = '';
        $t_data = (isset($tdata['t_data']) ? $tdata['t_data'] : []);
        $show_total = (isset($tdata['show_total']) ? $tdata['show_total'] : false);
        if ($show_total) {
            $t_total = 0;
            $t_total_count = 0;
        }
        $show_amount = (isset($tdata['show_amount']) ? $tdata['show_amount'] : true);
        $show_balance = (isset($tdata['show_balance']) ? $tdata['show_balance'] : false);
        $show_class = (isset($tdata['show_class']) ? $tdata['show_class'] : false);
        $show_action = (isset($tdata['show_action']) ? $tdata['show_action'] : false);
        if ($show_action) {
            $lend_array = (isset($tdata['lend_array']) ? $tdata['lend_array'] : []);
        }

        $show_checkbox = (isset($tdata['show_checkbox']) ? $tdata['show_checkbox'] : false);
        $show_credit_debit = (isset($tdata['show_credit_debit']) ? $tdata['show_credit_debit'] : false);
        $show_calculations = (isset($tdata['show_calculations']) ? $tdata['show_calculations'] : false);
        if ($show_calculations) {
            $calculation_total = 0;
            $opening_bal = 'INITIAL';
            $calculation_amount = 0;
            $order = checkOrderOfArray($t_data, 'trans_date');
        }


        $show_credit = false;
        $show_debit = false;
        if ($show_credit_debit) {
            $show_credit = true;
            $show_debit = true;
            $show_type = (isset($tdata['show_type']) ? $tdata['show_type'] : '');
            if ($show_type == 'cr') {
                $show_credit = true;
                $show_debit = false;
            } else if ($show_type == 'dr') {
                $show_credit = false;
                $show_debit = true;
            }
        }


        $table = '<table class="table table-striped table-bordered table-condensed transaction-table">';
        $table .= '<tr>';
        $table .= '<th ' . ($show_class ? 'class="col-date text-center"' : 'class="text-center"') . '>Date</th>';
        $table .= '<th ' . ($show_class ? 'class="col-description"' : '') . '>Description</th>';
        if ($show_action) {
            $table .= '<th class="col-icons text-center"><i class="fa fa-recycle" aria-hidden="true"></i></th>';
            $col_span++;
        }
        if ($show_amount) {
            $table .= '<th ' . ($show_class ? 'class="col-amount text-center"' : 'text-center') . '>Amount</th>';
        }
        if ($show_credit) {
            $cr = 0;
            $cr_count = 0;
            $table .= '<th ' . ($show_class ? 'class="col-amount text-center"' : 'text-center') . '>Cr</th>';
        }
        if ($show_debit) {
            $dr = 0;
            $dr_count = 0;
            $table .= '<th ' . ($show_class ? 'class="col-amount text-center"' : 'text-center') . '>Dr</th>';
        }
        if ($show_checkbox) {
            if ($show_credit_debit) {
                if ($show_credit) {
                    $table .= '<th class="col-amount text-center"><i class="fa fa-check-square-o" aria-hidden="true"></i></th>';
                }
                if ($show_debit) {
                    $table .= '<th class="col-amount text-center"><i class="fa fa-check-square-o" aria-hidden="true"></i></th>';
                }
            } else {
                $table .= '<th class="col-amount text-center"><i class="fa fa-check-square-o" aria-hidden="true"></i></th>';
            }
        }
        if ($show_balance) {
            $table .= '<th class="col-amount text-center">Balance</th>';
        }
        if ($show_calculations) {
            $table .= '<th class="col-amount text-center">Balance</th>';
        }
        $table .= '</tr>';


        foreach ($t_data as $t) {
            $t_type = $t->type;
            if ($show_total) {
                $t_total = $t_total + $t->amount;
                $t_total_count++;
            }
            $table .= '<tr>';
            $table .= '<td style="text-align: center;">' . displayDateCalendar($t->trans_date, ($isMobile ? 'size0_75x' : 'size1x')) . '</td>';
            $table .= '<td ' . ($show_class ? 'class="col-description"' : '') . '>' . displayDescription($t) . '</td>';
            if ($show_action) {
                $table .= '<td class="text-center ' . ($show_class ? 'col-icons' : '') . '">';
                if ($t_type != TYPE_TRANSFER) {
                    if ($t_type == TYPE_EXPENSE) {
                        $name = $t->payee;
                    } else {
                        $name = $t->payer;
                    }
                    $l_action = 'insert';
                    $l_icon = '<i class="fa fa-user-plus" aria-hidden="true"></i>';
                    if (!empty($lend_array) && in_array($t->trans_id, $lend_array)) {
                        $l_action = 'remove';
                        $l_icon = '<i class="fa fa-user-times" aria-hidden="true"></i>';
                    }
                    $table .= '<a href="" onclick ="addToTransact(event, \'' . $t->trans_id . '\', \'' . site_url() . '\')" class="myicon aicon ' . $l_action . '">' . $l_icon . '</a>';
                    $table .= '<a href="" onclick ="swapTransaction(event, \'' . $t->trans_id . '\')" class="myicon"><i class="fa fa-exchange" aria-hidden="true"></i></a>';
                    $table .= '<a class="myicon" href="' . site_url('Admin/editTransaction/' . $t->type . '/' . $t->trans_id) . '"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>';
                }
                $table .= '<a href="" onclick ="syncTransaction(event, \'' . $t->trans_id . '\', \'' . site_url() . '\')" class="myicon"><i class="fa fa-refresh" aria-hidden="true"></i></a>';
                $table .= '</td>';
            }
            $indicator_class = strtolower($t_type);
            if ($show_amount) {
                $table .= '<td class="text-end t-indicator ' . $indicator_class . '">' . displayMoney($t->amount) . '</td>';
            }
            if ($show_credit) {
                $cr += $t->cr;
                ($t->cr > 0) ? $cr_count++ : '';
                $table .= '<td class="text-end t-indicator ' . (($t->cr > 0) ? $indicator_class : '') . '">' . (($t->cr > 0) ? displayMoney($t->cr) : "") . '</td>';
            }
            if ($show_debit) {
                $dr += $t->dr;
                ($t->dr > 0) ? $dr_count++ : '';
                $table .= '<td class="text-end t-indicator ' . (($t->dr > 0) ? $indicator_class : '') . '">' . (($t->dr > 0) ? displayMoney($t->dr) : "") . '</td>';
            }
            if ($show_checkbox) {
                if ($show_credit_debit) {
                    if ($show_credit) {
                        if ($t->cr > 0) {
                            $table .= '<td class="text-center"><input type="checkbox" value="' . $t->amount . '" class="check-in" /></td>';
                        } else {
                            $table .= '<td></td>';
                        }
                    }
                    if ($show_debit) {
                        if ($t->dr > 0) {
                            $table .= '<td class="text-center"><input type="checkbox" value="' . $t->amount . '" class="check-ex" /></td>';
                        } else {
                            $table .= '<td></td>';
                        }
                    }
                } else {
                    $table .= '<td class="text-center"><input type="checkbox" value="' . $t->amount . '" class="check-' . strtolower($t_type) . '" /></td>';
                }
            }
            if ($show_balance) {
                $table .= '<td class="text-end">' . displayMoney($t->bal) . '</td>';
            }
            if ($show_calculations) {
                // GET Opening Balance
                if ($opening_bal == 'INITIAL') {
                    if ($t->cr > 0) {
                        if ($order == 'ASC') {
                            $opening_bal = $t->bal - $t->amount;
                        } else {
                            $opening_bal = $t->bal + $t->amount;
                        }
                    } else {
                        if ($order == 'ASC') {
                            $opening_bal = $t->bal + $t->amount;
                        } else {
                            $opening_bal = $t->bal - $t->amount;
                        }
                    }
                    $calculation_total = $opening_bal;
                    if ($order == 'DESC') {
                        $calculation_amount = $t->amount;
                    }
                }
                if ($order == 'ASC') {
                    $calculation_amount = $t->amount;
                }
                if ($t->cr > 0) {
                    if ($order == 'ASC') {
                        $calculation_total = $calculation_total + $calculation_amount;
                    } else if ($order == 'DESC') {
                        $calculation_total = $calculation_total + $calculation_amount;
                    }
                } else {
                    if ($order == 'ASC') {
                        $calculation_total = $calculation_total - $calculation_amount;
                    } else if ($order == 'DESC') {
                        $calculation_total = $calculation_total + $calculation_amount;
                    }
                }
                if ($order == 'DESC') {
                    $calculation_amount = $t->amount;
                }

                $table .= '<td class="text-end ' . ((abs($t->bal - $calculation_total) < 0.00001) ? 'bg-success text-white' : 'bg-danger text-white') . '">' . displayMoney($calculation_total, false) . '</td>';
            }
            $table .= '</tr>';
        }

        if ($show_total) {
            // TOTAL
            $table .= '<tr>';
            $table .= '<td colspan="' . $col_span . '">Total</td>';
            if ($show_amount) {
                $table .= '<td class="text-end">' . displayMoney($t_total) . '</td>';
            }
            if ($show_credit) {
                $table .= '<td class="text-end">' . displayMoney($cr) . '</td>';
            }
            if ($show_debit) {
                $table .= '<td class="text-end">' . displayMoney($dr) . '</td>';
            }
            if ($show_checkbox) {
                if ($show_credit_debit) {
                    if ($show_credit) {
                        $table .= '<td id="total-in" class="text-end"></td>';
                    }
                    if ($show_debit) {
                        $table .= '<td id="total-ex" class="text-end"></td>';
                    }
                } else {
                    $table .= '<td class="text-end" id="total-' . strtolower($t_type) . '"></td>';
                }
            }
            if ($show_balance) {
                $table .= '<th></th>';
            }
            if ($show_calculations) {
                $table .= '<td class="text-end"></td>';
            }
            $table .= '</tr>';

            // COUNT
            $table .= '<tr>';
            $table .= '<td colspan="' . $col_span . '">' . ($show_calculations ? 'Opening Balance: ' . displayMoney($opening_bal) . ' / ' : '') . 'Total Count</td>';
            if ($show_amount) {
                $table .= '<td class="text-center">' . $t_total_count . '</td>';
            }
            if ($show_credit) {
                $table .= '<td class="text-center">' . $cr_count .  '</td>';
            }
            if ($show_debit) {
                $table .= '<td class="text-center">' . $dr_count . '</td>';
            }
            if ($show_checkbox) {
                if ($show_credit_debit) {
                    if ($show_credit) {
                        $table .= '<td class="text-end"></td>';
                    }
                    if ($show_debit) {
                        $table .= '<td class="text-end"></td>';
                    }
                } else {
                    $table .= '<td class="text-end"></td>';
                }
            }
            if ($show_balance) {
                $table .= '<th></th>';
            }
            if ($show_calculations) {
                $table .= '<td class="text-end"></td>';
            }
            $table .= '</tr>';
        }
        $table .= '</table>';
        return $table;
    }

    /**
     * 
     */
    public function getRepeatTransactionTable($rtdata)
    {
        $t_data = (!empty($rtdata['t_data']) ? $rtdata['t_data'] : []);

        $display = '<table class="table table-striped table-bordered table-condensed display responsive nowrap transaction-table">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th class="col-date">Date</th>';
        $display .= '<th>Account</th>';
        $display .= '<th class="col-amount">Amount</th>';
        $display .= '<th>Method</th>';
        $display .= '<th>Status</th>';
        $display .= '<th class="action">Action</th>';
        $display .= '</tr>';
        $display .= '</thead>';
        $display .= '<tbody>';

        // Loop through the data and add rows
        foreach ($t_data as $account) {
            $display .= '<tr>';
            $display .= '<td style="text-align: center;">' . displayDateCalendar($account->trans_date) . '</td>';
            $display .= '<td class="col-description">' . displayDescription($account) . '</td>';
            $display .= '<td class="text-end">' . displayMoney($account->amount) . '</td>';
            $display .= '<td>' . $account->p_method . '</td>';
            $status = '';
            $edit_url = '';
            $site_url = site_url('Admin/processRepeatTransaction/');
            switch ($account->type) {
                case TYPE_INCOME: {
                        $status = 'Pending';
                        $edit_url = site_url('Admin/editRepeatTransaction/Income/' . $account->trans_id);
                        $action = 'receive';
                        break;
                    }
                case TYPE_EXPENSE: {
                        $status = 'Unpaid';
                        $edit_url = site_url('Admin/editRepeatTransaction/Expense/' . $account->trans_id);
                        $action = 'paid';
                        break;
                    }
                case TYPE_TRANSFER: {
                        $status = 'Unpaid';
                        $edit_url = site_url('Admin/editRepeatTransaction/Transfer/' . $account->trans_id);
                        $action = 'paid';
                        break;
                    }
            }
            $display .= '<td><span class="btn btn-primary mybtn btn-danger btn-xs process-trans">' . $status . '</span></td>';
            $display .= '<td class="text-center col-icons">';
            $display .= '<a class="myicon" data-toggle="tooltip" title="Click For Edit" href="' . $edit_url . '"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>';
            $display .= '<a class="myicon" onClick="processRepeatTransaction(event, \'' . $account->trans_id . '\', \'' . $site_url . '\', \'' . $action . '\')"><i class="fa fa-arrows" aria-hidden="true"></i></a>';
            $display .= '<a class="myicon" onclick="deleteRepeatTransaction(event, \'' . $account->trans_id . '\', \'' . $site_url . '\')"><i class="fa fa-trash-o"></i></a>';
            $display .= '</td>';
            $display .= '</tr>';
        }

        $display .= '</tbody>';
        $display .= '</table>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getAccountTable($accounts, $fBalance, $accountType)
    {
        $display = '<table class="table table-striped table-bordered table-condensed">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th class="col-account">Account</th>';
        $display .= '<th class="col-amount">Balance</th>';
        $display .= '<th class="col-type">Type</th>';
        $display .= '<th class="col-description">Note</th>';
        $display .= '<th class="col-date-1">Updated</th>';
        $display .= '<th class="action">Action</th>';
        $display .= '</tr>';
        $display .= '</thead>';
        $display .= '<tbody>';

        // Loop through the accounts and add rows
        foreach ($accounts as $account) {
            if ($account->active == $accountType) {
                $display .= '<tr>';
                $display .= '<td>' . $account->accounts_name . '</td>';
                $display .= '<td class="text-end">' . displayMoney($fBalance[$account->accounts_id]) . '</td>';
                $display .= '<td>' . $account->account_type . '</td>';
                $display .= '<td>' . $account->note . '</td>';
                $display .= '<td>' . $account->updated . getDaysfromDate($account->updated) . '</td>';
                $display .= '<td>';
                $display .= '<a class="btn btn-primary mybtn btn-info btn-xs account-edit-btn" href="' . site_url('Admin/editAccount/' . $account->accounts_id) . '"><i class="fa fa-search"></i> View</a>';
                $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageAccount/addForm/' . $account->accounts_id) . '\')">Edit</button>';
                // Uncomment if you want to add delete functionality
                // $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs account-remove-btn hide" href="' . site_url('Admin/addAccount/remove/' . $account->accounts_id) . '"><i class="fa fa-trash-o"></i> Delete</a>';
                $display .= '</td>';
                $display .= '</tr>';
            }
        }

        $display .= '</tbody>';
        $display .= '</table>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getPayerPayeeTable($p_list, $status)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed">';
        $display .= '<th class="sc-col-4">Payee/Payer Name</th>';
        $display .= '<th class="sc-col-3">Type</th>';
        $display .= '<th class="sc-col-3">Action</th>';
        $display .= '</table>';

        $display .= '<div class="scroll-div">';
        $display .= '<table class="table table-striped table-bordered table-condensed">';
        $display .= '<tbody id="table-body">';
        if (empty($p_list)) {
            $display .= '<tr>';
            $display .= '<td class="sc-col-4 t_name"></td>';
            $display .= '<td class="sc-col-3 t_type"></td>';
            $display .= '<td class="sc-col-3"></td>';
            $display .= '</tr>';
        }
        if ($status == STATUS_ACTIVE) {
            $astatus = STATUS_DISABLE;
            $atxt = 'Disable';
        } else if ($status == STATUS_DISABLE) {
            $astatus = STATUS_ACTIVE;
            $atxt = 'Enable';
        }
        foreach ($p_list as $list) {
            if ($list->status == $status) {
                $display .= '<tr>';
                $display .= '<td class="sc-col-4 t_name">' . $list->payee_payers . '</td>';
                $display .= '<td class="sc-col-3 t_type">' . $list->type . '</td>';
                $display .= '<td class="sc-col-3">';
                $display .= '<a class="btn btn-primary mybtn btn-info btn-xs action-edit-btn" href="' . $list->trace_id . '">Edit</a>';
                $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/payeeAndPayers/action/' . $list->trace_id . '/' . $astatus) . '">' . $atxt . '</a>';
                $display .= '</td>';
                $display .= ' </tr>';
            }
        }
        $display .= '</tbody>';
        $display .= ' </table>';
        $display .= ' </div>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getChartOfAccountsTable($p_list, $status)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed">';
        $display .= '<th class="sc-col-4">Account Name</th>';
        $display .= '<th class="sc-col-3">Acc-Type</th>';
        $display .= '<th class="sc-col-3">Action</th>';
        $display .= '</table>';

        $display .= '<div class="scroll-div">';
        $display .= '<table class="table table-striped table-bordered table-condensed">';
        $display .= '<tbody id="table-body">';
        if (empty($p_list)) {
            $display .= '<tr>';
            $display .= '<td class="sc-col-4 t_name"></td>';
            $display .= '<td class="sc-col-3 t_type"></td>';
            $display .= '<td class="sc-col-3"></td>';
            $display .= '</tr>';
        }
        if ($status == STATUS_ACTIVE) {
            $astatus = STATUS_DISABLE;
            $atxt = 'Disable';
        } else if ($status == STATUS_DISABLE) {
            $astatus = STATUS_ACTIVE;
            $atxt = 'Enable';
        }
        foreach ($p_list as $list) {
            if ($list->status == $status) {
                $display .= '<tr>';
                $display .= '<td class="sc-col-4 t_name">' . $list->accounts_name . '</td>';
                $display .= '<td class="sc-col-3 t_type">' . $list->accounts_type . '</td>';
                $display .= '<td class="sc-col-3">';
                $display .= '<a class="btn btn-primary mybtn btn-info btn-xs action-edit-btn" href="' . $list->chart_id . '">Edit</a>';
                $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/chartOfAccounts/action/' . $list->chart_id . '/' . $astatus) . '">' . $atxt . '</a>';
                $display .= '</td>';
                $display .= ' </tr>';
            }
        }
        $display .= '</tbody>';
        $display .= ' </table>';
        $display .= ' </div>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getPaymentMethodTable($p_list, $status)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed">';
        $display .= '<th class="sc-col-4">Payee/Payer Name</th>';
        $display .= '<th class="sc-col-3">Action</th>';
        $display .= '</table>';

        $display .= '<div class="scroll-div">';
        $display .= '<table class="table table-striped table-bordered table-condensed">';
        $display .= '<tbody id="table-body">';
        if (empty($p_list)) {
            $display .= '<tr>';
            $display .= '<td class="sc-col-4 t_name"></td>';
            $display .= '<td class="sc-col-3"></td>';
            $display .= '</tr>';
        }
        if ($status == STATUS_ACTIVE) {
            $astatus = STATUS_DISABLE;
            $atxt = 'Disable';
        } else if ($status == STATUS_DISABLE) {
            $astatus = STATUS_ACTIVE;
            $atxt = 'Enable';
        }
        foreach ($p_list as $list) {
            if ($list->status == $status) {
                $display .= '<tr>';
                $display .= '<td class="sc-col-4 t_name">' . $list->p_method_name . '</td>';
                $display .= '<td class="sc-col-3">';
                $display .= '<a class="btn btn-primary mybtn btn-info btn-xs action-edit-btn" href="' . $list->p_method_id . '">Edit</a>';
                $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/paymentMethod/action/' . $list->p_method_id . '/' . $astatus) . '">' . $atxt . '</a>';
                $display .= '</td>';
                $display .= ' </tr>';
            }
        }
        $display .= '</tbody>';
        $display .= ' </table>';
        $display .= ' </div>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    function getBulkTransactionListTable($allBuilkTrans, $status)
    {
        $form_html = '';
        $form_html .= '<table class="table table-bordered">';
        foreach ($allBuilkTrans as $tran) {
            if ($tran->status == $status) {
                $transact = json_decode($tran->tansactions);
                // echo '<pre>';print_r($tran);echo '</pre>';
                $me = 0;
                $mt = 0;
                $mi = 0;
                if (!empty($transact->type)) {
                    foreach ($transact->type as $key => $intran) {
                        switch ($intran) {
                            case TYPE_INCOME: {
                                    $mi += (!empty($transact->amount[$key]) ? $transact->amount[$key] : 0);
                                    break;
                                }
                            case TYPE_EXPENSE: {
                                    $me += (!empty($transact->amount[$key]) ? $transact->amount[$key] : 0);
                                    break;
                                }
                            case TYPE_TRANSFER: {
                                    $mt += (!empty($transact->amount[$key]) ? $transact->amount[$key] : 0);
                                    break;
                                }
                        }
                    }
                }
                if ($status == STATUS_ACTIVE) {
                    $astatus = STATUS_DISABLE;
                    $atxt = '<i class="fa fa-folder" aria-hidden="true"></i>';
                } else if ($status == STATUS_DISABLE) {
                    $astatus = STATUS_ACTIVE;
                    $atxt = '<i class="fa fa-folder-open" aria-hidden="true"></i>';
                }
                $form_html .= '<tr><td>' . $transact->name . '</td>';
                $form_html .= '<td class="width-20x">Income: ' . displayMoney($mi) . '<br/>Expense: ' . displayMoney($me) . '<br/>Transfer: ' . displayMoney($mt) . '</td>';
                $form_html .= '<td class="width-5x text-align-center" style="font-size: 25px;">';
                $form_html .= '<a href="' . site_url('Admin/bulkTransactionView/show/' . $tran->trans_id) . '"><i class="fa fa-pencil"></i></a>';
                $form_html .= '<a class="action-enabledisable-btn" href="' . site_url('Admin/bulkTransaction/action/' . $tran->trans_id . '/' . $astatus) . '">' . $atxt . '</a>';
                // TODO: Change confirmation alert with sweet alert
                // $form_html .= '<td class="width-5x text-align-center" style="font-size: 25px;"><a href="' . site_url('Admin/bulkTransaction/remove/' . $tran->trans_id) . '" class="bulk-trans-remove-btn"><i class="fa fa-trash"></i></a></td>';
                $form_html .= '</td>';
                $form_html .= '</tr>';
            }
        }
        $form_html .= '</table>';
        return $form_html;
    }

    function getBulkTransactionViewList($param1, $transact)
    {
        $form_html = '<div class="col-md-6 col-lg-6 col-sm-12 float-left-sm"><a class="btn btn-primary mybtn btn-submit" href="' . site_url('Admin/bulkTransaction/') . '"><i class="fa fa-archive"></i> Bulk Transaction</a></div>';
        $form_html .= '<div class="col-md-6 col-lg-6 col-sm-12 float-right-sm text-align-center sticky"><button class="btn btn-primary mybtn btn-success" onClick="addMoreBox(\'Income\')"><i class="fa fa-plus-square"></i> Income</button><button class="btn btn-primary mybtn btn-danger" onClick="addMoreBox(\'Expense\')"><i class="fa fa-minus-square"></i> Expense</button><button class="btn btn-primary mybtn btn-info" onClick="addMoreBox(\'Transfer\')"><i class="fa fa-retweet"></i> Transfer</button></div>';
        $form_html .= '<hr style="float: left;width: 100%;">';
        $form_html .= form_open('' . base_url() . '', 'class="clear-both add-transaction" id="add-transaction"');
        $form_html .= '<div class="row mx-0">';
        $ielmt = array(
            'type' => 'text',
            'name' => 'name',
            'value' => ((!empty($param1) && !empty($transact) && !empty($transact->name)) ? $transact->name : ''),
            'class' => 'name',
            'isLabel' => true,
            'label' => 'Name',
            'divClass' => 'col-md-2 col-lg-2 col-sm-2 col-8'
        );
        $form_html .= $this->Displaymodel->textBox($ielmt);

        $ielmt = array(
            'name' => 'autosave',
            'value' => '1',
            'class' => 'auto-save',
            'id' => 'auto-save',
            'isLabel' => true,
            'label' => 'Auto Save',
            'checked' => ((!empty($param1) && !empty($transact) && !empty($transact->autosave) && $transact->autosave == '1') ? true : false),
            'divClass' => 'col-md-1 col-lg-1 col-sm-1 col-4'
        );
        $form_html .= $this->Displaymodel->checkBox($ielmt);
        $ielmt = array(
            'name' => 'recalculate',
            'value' => '1',
            'class' => 'recalculate',
            'id' => 'recalculate',
            'isLabel' => true,
            'label' => 'Re calculate',
            'checked' => ((!empty($param1) && !empty($transact) && !empty($transact->recalculate) && $transact->recalculate == '1') ? true : false),
            'divClass' => 'col-md-1 col-lg-1 col-sm-1 col-4'
        );
        $form_html .= $this->Displaymodel->checkBox($ielmt);
        $form_html .= '<div class="form-group col-md-8 col-lg-8 col-sm-8 col-12 position-relative">';
        $form_html .= '<label for="search" class="form-label">Search</label>';
        $form_html .= '<input type="text" id="search_transaction" class="form-control" placeholder="Start typing..." autocomplete="off">';
        $form_html .= '<ul class="dropdown-menu w-100" id="suggestions"></ul>';
        $form_html .= '</div>';
        $form_html .= '</div>';
        $form_html .= '<hr style="float: left;width: 100%;">';
        $form_html .= '<div id="transactions-list">';
        if (!empty($param1) && !empty($transact) && !empty($transact->type)) {

            foreach ($transact->type as $key => $tran_type) {
                // echo '<pre>';print_r($key);echo '</pre>';
                $transaction = new Transaction();

                if ($tran_type == TYPE_TRANSFER) {
                    // TODO $transact->account -> $transact->accounts_name
                    $transaction->setAccountsName($this->Transaction_model->mergeAccountNameForTransfer($transact->accounts_name[$key], $transact->account_to[$key]));
                } else {
                    $transaction->setAccountsName($transact->accounts_name[$key]);
                }

                $transaction->settype($tran_type);
                $transaction->setTransDate($transact->income_expense_date[$key]);
                $transaction->setCategory($transact->income_expense_type[$key]);
                $transaction->setAmount($transact->amount[$key]);
                $transaction->setPayer($transact->payee_payers[$key]);
                $transaction->setPayee($transact->payee_payers[$key]);
                $transaction->setPMethod($transact->p_method[$key]);
                $transaction->setRef($transact->reference[$key]);
                $transaction->setNote($transact->note[$key]);
                // echo '<pre>';print_r($transaction);echo '</pre>';
                $form_html .= $this->Displaymodel->getTransactionBox($tran_type, $transaction);
            }
        }
        $form_html .= '</div>';
        $form_html .= '<div class="form-group clear-both" style="display: block;text-align: center;font-size: 26px;">Total: <span id="totalamt">0</span></div>';
        $form_html .= '<div class="form-group clear-both" style="display: block;text-align: center;font-size: 12px;">Income: <span id="totalamt-income">0</span> Expense: <span id="totalamt-expense">0</span> Transfer: <span id="totalamt-transfer">0</span></div>';
        $form_html .= '<button type="submit"  class="btn btn-primary mybtn btn-submit"><i class="fa fa-check"></i> Submit</button>';
        $form_html .= '<div class="add-button">
        <a id="validate-transaction" class="btn btn-primary mybtn btn-warning"><i class="fa fa-archive"></i> Validate</a>
        <a id="bulkbtn" onClick="saveBox(' . (!empty($param1) ? $param1 : '') . ')" class="btn btn-primary mybtn btn-success"><i class="fa fa-archive"></i> ' . (!empty($param1) ? 'Update' : 'Save') . '</a>
        </div>';
        $form_html .= form_close();

        return $form_html;
    }

    public function getVehiclesListTable($p_list)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed large-table">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th class="width-30x">Name</th>';
        $display .= '<th class="width-8x text-center">Type</th>';
        $display .= '<th class="width-15x text-center">Registration No.</th>';
        $display .= '<th class="width-15x text-center">Month & Year</th>';
        $display .= '<th class="width-8x text-center">Fuel</th>';
        $display .= '<th>Note</th>';
        $display .= '<th class="width-15x">Action</th>';
        $display .= '</tr>';
        $display .= '</thead>';
        $display .= '<tbody id="table-body">';
        foreach ($p_list as $list) {
            $display .= '<tr>';
            $display .= '<td>' . $list->name . '</td>';
            $display .= '<td class="text-center">' . $list->type . '</td>';
            $display .= '<td class="text-center">' . $list->registration_no . '</td>';
            $display .= '<td class="text-center">' . $list->month_year . '</td>';
            $display .= '<td class="text-center">' . $list->fuel_liters . '</td>';
            $display .= '<td>' . $list->note . '</td>';
            $display .= '<td>';
            $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageVehicles/addForm/' . $list->vehicle_id) . '\')">Edit</button>';
            // $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/paymentMethod/action/' . $list->vehicle_id) . '">55</a>';
            $display .= '</td>';
            $display .= ' </tr>';
        }
        $display .= '</tbody>';
        $display .= ' </table>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getTripsListTables($p_list)
    {
        $r_list = $this->Vehicle_trip_model->groupTripsByVehicleId($p_list);
        $html = '';
        foreach ($r_list as $r) {
            $html .= $this->getTripsListTable($r);
        }
        return $html;
    }

    public function getTripsListTable($p_list)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed large-table mb-5">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th rowspan="2" class="width-25x">Vehicle Name</th>';
        $display .= '<th rowspan="2" class="width-20x text-center">Trip Date</th>';
        $display .= '<th rowspan="2" class="width-15x text-center">Meter Reading</th>';
        $display .= '<th rowspan="2" class="width-8x text-center table-info">Distance</th>';
        $display .= '<th colspan="3" class="text-center table-info">Fuel</th>';
        $display .= '<th rowspan="2" class="width-8x text-center table-info">Mileage</th>';
        $display .= '<th rowspan="2">Note</th>';
        $display .= '<th rowspan="2" class="width-15x">Action</th>';
        $display .= '</tr>';

        $display .= '<tr>';
        $display .= '<th class="width-5x text-center">%</th>';
        $display .= '<th class="width-5x text-center table-info">Used</th>';
        $display .= '<th class="width-5x text-center table-info">Bal</th>';
        $display .= '</tr>';

        $display .= '</thead>';
        $display .= '<tbody id="table-body">';

        foreach ($p_list as $list) {
            $display .= '<tr>';
            $display .= '<td>' . $list->vehicle_name . '</td>';
            $display .= '<td class="text-center">' . $list->trip_date . '</td>';
            $display .= '<td class="text-center">' . $list->meter_reading . '</td>';
            $display .= '<td class="text-center table-info">' . $list->distance_km . '</td>';
            $display .= '<td class="text-center">' . $list->fuel_percentage . '</td>';
            $display .= '<td class="text-center table-info">' . $list->fuel_used . '</td>';
            $display .= '<td class="text-center table-info">' . $list->fuel_bal . '</td>';
            $display .= '<td class="text-center table-info">' . $list->mileage_kmpl . '</td>';
            $display .= '<td>' . $list->note . '</td>';
            $display .= '<td>';
            $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageTrips/addForm/' . $list->trip_id) . '\')">Edit</button>';
            $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageTrips/copy/' . $list->trip_id) . '\')">Copy</button>';
            // $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/paymentMethod/action/' . $list->trip_id) . '">55</a>';
            $display .= '</td>';
            $display .= ' </tr>';
        }
        $display .= '</tbody>';
        $display .= ' </table>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getVehiclesFuelsListTable($p_list)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed large-table">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th rowspan="2" class="width-30x">Vehicle Name</th>';
        $display .= '<th rowspan="2" class="width-20x text-center table-info">Fill Date</th>';
        $display .= '<th rowspan="2" class="width-15x text-center table-info">Meter Reading</th>';
        $display .= '<th colspan="4" class="text-center table-info">Fuel</th>';
        $display .= '<th rowspan="2" class="table-info">Note</th>';
        $display .= '<th rowspan="2" class="width-15x">Action</th>';
        $display .= '</tr>';
        $display .= '<tr>';
        $display .= '<th class="width-8x text-center">Liters</th>';
        $display .= '<th class="width-8x text-center">Price</th>';
        $display .= '<th class="width-8x text-center table-info">%</th>';
        $display .= '<th class="width-8x text-center table-info">Total</th>';
        $display .= '</tr>';
        $display .= '</thead>';
        $display .= '<tbody id="table-body">';
        foreach ($p_list as $list) {
            $display .= '<tr>';
            $display .= '<td>' . $list->vehicle_name . '</td>';
            $display .= '<td class="text-center table-info">' . $list->fill_date . '</td>';
            $display .= '<td class="text-center table-info">' . $list->meter_reading . '</td>';
            $display .= '<td class="text-center">' . $list->fuel_liters . '</td>';
            $display .= '<td class="text-center">' . $list->fuel_price . '</td>';
            $display .= '<td class="text-center table-info">' . $list->fuel_percentage . '</td>';
            $display .= '<td class="text-center table-info">' . $list->total_cost . '</td>';
            $display .= '<td class="table-info">' . $list->note . '</td>';
            $display .= '<td>';
            $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageFuels/addForm/' . $list->fuel_id) . '\')">Edit</button>';
            // $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/manageFuels/action/' . $list->fuel_id) . '">55</a>';
            $display .= '</td>';
            $display .= ' </tr>';
        }
        $display .= '</tbody>';
        $display .= ' </table>';

        // Now $display contains the full HTML for the table
        return $display;
    }

    public function getVehicleServiceListTable($p_list)
    {
        $display = '<table class="scroll-table-head table table-striped table-bordered table-condensed large-table">';
        $display .= '<thead>';
        $display .= '<tr>';
        $display .= '<th class="width-30x">Vehicle Name</th>';
        $display .= '<th class="width-15x text-center">Service Type</th>';
        $display .= '<th class="width-20x text-center table-info">Service Date</th>';
        $display .= '<th class="width-15x text-center table-info">Meter Reading</th>';
        $display .= '<th class="width-15x text-center table-info">Fuel Percentage</th>';
        $display .= '<th class="table-info">Note</th>';
        $display .= '<th class="width-15x">Action</th>';
        $display .= '</tr>';
        $display .= '</thead>';
        $display .= '<tbody id="table-body">';
        foreach ($p_list as $list) {
            $display .= '<tr>';
            $display .= '<td>' . $list->vehicle_name . '</td>';
            $display .= '<td class="text-center">' . $list->service_type . '</td>';
            $display .= '<td class="text-center table-info">' . $list->service_date . '</td>';
            $display .= '<td class="text-center table-info">' . $list->meter_reading . '</td>';
            $display .= '<td class="text-center table-info">' . $list->fuel_percentage . '</td>';
            $display .= '<td class="table-info">' . $list->note . '</td>';
            $display .= '<td>';
            $display .= '<button class="btn btn-primary mybtn" onclick="showCommonModal(\'myCommonModal\', \'' . site_url('Admin/manageServices/addForm/' . $list->service_id) . '\')">Edit</button>';
            // $display .= '<a class="btn btn-primary mybtn btn-danger btn-xs action-enabledisable-btn" href="' . site_url('Admin/paymentMethod/action/' . $list->service_id) . '">55</a>';
            $display .= '</td>';
            $display .= ' </tr>';
        }
        $display .= '</tbody>';
        $display .= ' </table>';

        // Now $display contains the full HTML for the table
        return $display;
    }
}
