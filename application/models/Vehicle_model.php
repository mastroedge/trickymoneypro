<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Model {

    private $table = 'vehicles';

    public function __construct() {
        parent::__construct();
    }

    public function get_all() {
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($vehicle_id) {
        return $this->db->get_where($this->table, ['vehicle_id' => $vehicle_id])->row();
    }

    public function insert($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update($vehicle_id, $data) {
        return $this->db->where('vehicle_id', $vehicle_id)->update($this->table, $data);
    }

    public function delete($vehicle_id) {
        return $this->db->where('vehicle_id', $vehicle_id)->delete($this->table);
    }
}
?>
