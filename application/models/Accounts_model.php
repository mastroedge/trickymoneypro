<?php
class Accounts_model extends CI_Model {
    protected $table = 'accounts';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_accounts() {
        return $this->db->get($this->table)->result();
    }

    public function get_account_by_id($account_id) {
        return $this->db->get_where($this->table, ['accounts_id' => $account_id])->row();
    }

    public function insert_account($data) {
        return $this->db->insert($this->table, $data);
    }

    public function update_account($account_id, $data) {
        return $this->db->update($this->table, $data, ['accounts_id' => $account_id]);
    }

    public function delete_account($account_id) {
        return $this->db->delete($this->table, ['accounts_id' => $account_id]);
    }
}
