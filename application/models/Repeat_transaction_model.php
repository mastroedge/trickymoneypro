<?php
class Repeat_transaction_model extends CI_Model {
    protected $table = 'repeat_transaction';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_repeat_transactions() {
        return $this->db->get($this->table)->result();
    }

    public function insert_repeat_transaction($data) {
        return $this->db->insert($this->table, $data);
    }

    // get single repeat transaction
    public function getSingleRepeatTransaction($trans_id)
    {
        $this->db->select('*');
        $this->db->from('repeat_transaction');
        $this->db->where('trans_id', $trans_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    /**
     * Get repeat transactions (Income, Expense, Transfer)
     * fetch 3 year before and after today
     */
    public function getRepeatTransaction($type, $status = '')
    {
        $this->db->select('*');
        $this->db->from('repeat_transaction');
        $this->db->where('type', $type);
        $this->db->where("user_id", $this->session->userdata('user_id'));
        if (!empty($status)) {
            if (is_array($status)) {
                $this->db->where_in('status', $status);
            } elseif (is_string($status)) {
                $this->db->where('status', $status);
            }
        }
        // Get current date
        $current_date = date('Y-m-d');
        // Set date range (3 years before and after today)
        $this->db->where("trans_date >=", "DATE_SUB('$current_date', INTERVAL 3 YEAR)", false);
        $this->db->where("trans_date <=", "DATE_ADD('$current_date', INTERVAL 3 YEAR)", false);
        $this->db->order_by('trans_date', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    /**
     * Insert logic for repeat transactions (Income, Expense, Transfer)
     */
    function insertRepeatTransaction($data, $rotation, $loop)
    {
        $this->db->trans_begin();
        for ($i = 0; $i < $loop; $i++) {
            $this->db->insert('repeat_transaction', $data);
            $data['trans_date'] = changeDate($data['trans_date'], $rotation);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
