<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datatable_model extends CI_Model
{

    var $table;
    var $column_order;
    var $column_search;
    var $order;
    var $order_by;
    var $where;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {

        $this->db->from($this->table);
        if (!empty($this->where)) $this->db->where($this->where);

        $i = 0;

        foreach ($this->column_search as $item) // loop column
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    // $this->db->like($item, $_POST['search']['value']);
                    $this->db->like('LOWER(' . $item . ')', strtolower($_POST['search']['value']));
                } else {
                    // $this->db->or_like($item, $_POST['search']['value']);
                    $this->db->or_like('LOWER(' . $item . ')', strtolower($_POST['search']['value']));
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            foreach ($order as $k => $o) {
                $this->db->order_by($k, $o);
            }
        } else if (isset($_POST['order_by'])) {
            $this->db->order_by($_POST['order_by']);
        } else if (isset($this->order_by)) {
            $this->db->order_by($this->order_by);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        if (!empty($this->where)) $this->db->where($this->where);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
