<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle_service_model extends CI_Model
{

    private $table = 'vehicle_services';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function get_by_id($service_id)
    {
        return $this->db->get_where($this->table, ['service_id' => $service_id])->row();
    }

    public function get_all_by_vehicle($vehicle_id)
    {
        return $this->db->where('vehicle_id', $vehicle_id)->order_by('service_date', 'DESC')->get($this->table)->result();
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($service_id, $data)
    {
        return $this->db->where('service_id', $service_id)->update($this->table, $data);
    }

    public function delete($id)
    {
        return $this->db->where('id', $id)->delete($this->table);
    }

    public function get_vehicle_services_with_details($services, $v_list_obj, $t_list_obj)
    {

        foreach ($services as $service) {
            // Ensure trip data exists
            if (!isset($t_list_obj[$service->trip_id])) {
                continue;
            }
            $trip = $t_list_obj[$service->trip_id];

            // Ensure vehicle data exists
            if (!isset($v_list_obj[$trip->vehicle_id])) {
                continue;
            }

            $vehicle = $v_list_obj[$trip->vehicle_id];

            $service->vehicle_name = $vehicle->name;
            $service->service_date = $trip->trip_date;
            $service->meter_reading = $trip->meter_reading;
            $service->fuel_percentage = $trip->fuel_percentage;
            $service->note = $trip->note;
        }

        return $services;
    }
}
