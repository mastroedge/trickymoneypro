<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicle_trip_model extends CI_Model
{

    private $table = 'vehicle_trips';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function get_all_limit($limit)
    {
        $sql = "
            WITH RankedTrips AS (
                SELECT *,
                    ROW_NUMBER() OVER (PARTITION BY vehicle_id ORDER BY meter_reading DESC) AS trip_rank
                FROM vehicle_trips
            )
            SELECT * 
            FROM RankedTrips
            WHERE trip_rank <= " . $limit . "
            ORDER BY vehicle_id ASC, meter_reading ASC;
        ";

        // $sql = "
        //     SELECT vt.*
        //     FROM vehicle_trips vt
        //     JOIN (
        //         SELECT vehicle_id, trip_id
        //         FROM vehicle_trips
        //         ORDER BY trip_date DESC
        //         LIMIT " . $limit . "
        //     ) AS sub ON vt.vehicle_id = sub.vehicle_id AND vt.trip_id = sub.trip_id
        //     ORDER BY vt.vehicle_id ASC, vt.trip_date ASC;
        // ";

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_by_id($trip_id)
    {
        return $this->db->get_where($this->table, ['trip_id' => $trip_id])->row();
    }

    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    public function update($trip_id, $data)
    {
        return $this->db->where('trip_id', $trip_id)->update($this->table, $data);
    }

    public function delete($trip_id)
    {
        return $this->db->where('trip_id', $trip_id)->delete($this->table);
    }

    public function get_all_by_vehicle($vehicle_id)
    {
        return $this->db->where('vehicle_id', $vehicle_id)->order_by('trip_date', 'ASC')->get($this->table)->result();
    }

    public function get_last_trip($vehicle_id)
    {
        return $this->db->where('vehicle_id', $vehicle_id)->order_by('trip_date', 'DESC')->limit(1)->get($this->table)->row();
    }

    public function getTripByTripIDs($trip_ids) {
        return $this->db->where_in('trip_id', $trip_ids)->get($this->table)->result();
    }

    // Function to group trips by vehicle_id
    public function groupTripsByVehicleId($trips)
    {
        $groupedTrips = [];

        foreach ($trips as $trip) {
            $vehicleId = $trip->vehicle_id;

            if (!isset($groupedTrips[$vehicleId])) {
                $groupedTrips[$vehicleId] = [];
            }

            $groupedTrips[$vehicleId][] = $trip;
        }

        return $groupedTrips;
    }

    public function get_vehicle_trips_with_details($trips, $v_list_obj)
    {
        $last_meter_reading = 0;
        $last_fuel_percentage = null;

        foreach ($trips as $trip) {
            // Ensure vehicle data exists
            if (!isset($v_list_obj[$trip->vehicle_id])) {
                continue;
            }

            $vehicle = $v_list_obj[$trip->vehicle_id];
            $trip->vehicle_name = $vehicle->name;

            // Calculate distance
            if ($last_meter_reading > 0) {
                $trip->distance_km = round(max(0, $trip->meter_reading - $last_meter_reading), 2);
            } else {
                $trip->distance_km = 0;
            }

            // Calculate fuel used (if last fuel % is available)
            if ($last_fuel_percentage !== null && $trip->fuel_percentage > 0 && isset($vehicle->fuel_liters)) {
                $trip->fuel_bal = round($vehicle->fuel_liters * ($trip->fuel_percentage / 100), 2);
                $trip->fuel_used = round((($last_fuel_percentage - $trip->fuel_percentage) / 100) * $vehicle->fuel_liters, 2);
            } else {
                $trip->fuel_bal = 0;
                $trip->fuel_used = 0;
            }

            // Calculate mileage (only if fuel is consumed)
            if ($trip->fuel_used > 0 && $trip->distance_km > 0) {
                $trip->mileage_kmpl = round($trip->distance_km / $trip->fuel_used, 2);
            } else {
                $trip->mileage_kmpl = 0;
                $trip->fuel_used = 0;
            }

            // Update last values
            $last_meter_reading = $trip->meter_reading;
            $last_fuel_percentage = $trip->fuel_percentage;
        }

        return $trips;
    }
}
