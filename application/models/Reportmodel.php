<?php

class ReportModel extends CI_Model
{

    public function getIncomeExpense()
    {
        // Get Current Day Income, Expense AND Current Month Income, Expense
        $date = date(DATE_ONLY_FORMAT);
        // Current Day Income
        $income_query = $this->db->query("SELECT sum(amount) as amount FROM transaction 
WHERE type='Income' AND user_id='" . $this->session->userdata('user_id') . "' AND DATE(trans_date) ='" . $date . "'");
        $income_result = $income_query->row();
        // Current Day Expense
        $expense_query = $this->db->query("SELECT sum(amount) as amount FROM transaction 
WHERE type='" . TYPE_EXPENSE . "' AND user_id='" . $this->session->userdata('user_id') . "' AND DATE(trans_date) ='" . $date . "'");
        $expense_result = $expense_query->row();

        // Current Month Income
        $query1 = $this->db->query("SELECT sum(amount) as amount FROM `transaction` 
WHERE type='Income' AND user_id='" . $this->session->userdata('user_id') . "' 
And trans_date between ADDDATE(LAST_DAY(SUBDATE('" . $date . "',
INTERVAL 1 MONTH)), 1) AND LAST_DAY('" . $date . "')");
        $curmonth_income_result = $query1->row();

        // Current Month Expense
        $query2 = $this->db->query("SELECT sum(amount) as amount FROM `transaction` 
WHERE type='" . TYPE_EXPENSE . "' AND user_id='" . $this->session->userdata('user_id') . "' And trans_date 
between ADDDATE(LAST_DAY(SUBDATE('" . $date . "',
INTERVAL 1 MONTH)), 1) AND LAST_DAY('" . $date . "')");
        $curmonth_expense_result = $query2->row();

        $transaction = array(
            "current_day_income" => isset($income_result->amount) ? $income_result->amount : 0,
            "current_day_expense" => isset($expense_result->amount) ? $expense_result->amount : 0,
            "current_month_income" => isset($curmonth_income_result->amount) ? $curmonth_income_result->amount : 0,
            "current_month_expense" => isset($curmonth_expense_result->amount) ? $curmonth_expense_result->amount : 0
        );

        return $transaction;
    }

    public function dayByDayIncomeExpense()
    {
        $income = array();
        $expense = array();

        $date = date(DATE_ONLY_FORMAT);
        $date1 = $this->db->query("SELECT ADDDATE(LAST_DAY(SUBDATE('" . $date . "',INTERVAL 1 MONTH)), 1) as d")->row()->d;
        $date2 = $this->db->query("SELECT LAST_DAY('" . $date . "') as d")->row()->d;

        $income_query = $this->db->query("SELECT DATE(trans_date) AS trans_date,sum(amount) as amount,MONTHNAME('" . $date . "') as m_name FROM transaction where
type='Income' AND user_id='" . $this->session->userdata('user_id') . "' AND DATE(trans_date) between 
ADDDATE(LAST_DAY(SUBDATE('" . $date . "',INTERVAL 1 MONTH)), 1) AND
LAST_DAY('" . $date . "') GROUP BY DATE(trans_date)")
            ->result();

        $expense_query = $this->db->query("SELECT DATE(trans_date) AS trans_date,sum(amount) as amount,MONTHNAME('" . $date . "') as m_name FROM transaction where
type='" . TYPE_EXPENSE . "' AND user_id='" . $this->session->userdata('user_id') . "' AND DATE(trans_date) between 
ADDDATE(LAST_DAY(SUBDATE('" . $date . "',INTERVAL 1 MONTH)), 1) AND
LAST_DAY('" . $date . "') GROUP BY DATE(trans_date)")
            ->result();

        // For Income
        $maxIncome = count($income_query);
        $i = 0;
        // For Expense
        $maxExpense = count($expense_query);
        $j = 0;

        $day = 1;
        while (strtotime($date1) <= strtotime($date2)) {
            // For Income
            if ($maxIncome > 0) {
                if ($date1 == $income_query[$i]->trans_date) {
                    $income[$day] = array(
                        "amount" => $income_query[$i]->amount,
                        "date" => $date1,
                        "m_name" => $income_query[$i]->m_name
                    );

                    if ($i < ($maxIncome - 1)) {
                        $i++;
                    }
                } else {
                    $income[$day] = array(
                        "amount" => 0,
                        "date" => $date1,
                        "m_name" => 'NILL'
                    );
                }
            } else {
                $income[$day] = array(
                    "amount" => 0,
                    "date" => $date1,
                    "m_name" => 'NILL'
                );
            }
            // End Income

            // For Expense
            if ($maxExpense > 0) {
                if ($date1 == $expense_query[$j]->trans_date) {
                    $expense[$day] = array(
                        "amount" => $expense_query[$j]->amount,
                        "date" => $date1,
                        "m_name" => $expense_query[$j]->m_name
                    );
                    if ($j < ($maxExpense - 1)) {
                        $j++;
                    }
                } else {
                    $expense[$day] = array(
                        "amount" => 0,
                        "date" => $date1,
                        "m_name" => 'NILL'
                    );;
                }
            }
            $day++;
            $date1 = date(DATE_ONLY_FORMAT, strtotime("+1 day", strtotime($date1)));
        }
        return array(
            $income,
            $expense
        );
    }

    //
    public function sumOfIncomeExpense()
    {
        $date = date(DATE_ONLY_FORMAT);
        $income = $this->db->query("SELECT sum(amount) as amount from transaction
WHERE type='Income' AND user_id='" . $this->session->userdata('user_id') . "' AND trans_date between 
ADDDATE(LAST_DAY(SUBDATE('" . $date . "',INTERVAL 1 MONTH)), 1) AND
LAST_DAY('" . $date . "')")
            ->row();

        $expense = $this->db->query("SELECT sum(amount) as amount from transaction
WHERE type='" . TYPE_EXPENSE . "' AND user_id='" . $this->session->userdata('user_id') . "' AND trans_date between 
ADDDATE(LAST_DAY(SUBDATE('" . $date . "',INTERVAL 1 MONTH)), 1) AND
LAST_DAY('" . $date . "')")
            ->row();

        return array(
            "income" => $income->amount,
            "expense" => $expense->amount
        );
    }

    public function financialBalance($active = -1)
    {
        switch ($active) {
            case 2:
                // Retutn Only Active
                $active = 'AND active NOT IN(' . ACT_TYPE_DIS . ', ' . ACT_TYPE_DIS_DIS . ', ' . ACT_TYPE_DEL . ')';
                break;
            case 1:
                // Return only Active accounts (with display disabled)
                $active = 'AND active IN (' . ACT_TYPE_ACT . ', ' . ACT_TYPE_DIS_DIS . ')';
                break;
            case 0:
                // Return only Active accounts (with display disabled) & Disabled accounts
                $active = 'AND active IN (' . ACT_TYPE_ACT . ', ' . ACT_TYPE_DIS . ', ' . ACT_TYPE_DIS_DIS . ')';
                break;
            default:
                $active = '';
                break;
        }
        $query = $this->db->query("SELECT a.accounts_id, a.accounts_name as account,(SELECT bal FROM transaction
WHERE accounts_name=a.accounts_name AND user_id='" . $this->session->userdata('user_id') . "' ORDER BY trans_date DESC, trans_id DESC LIMIT 1) 
as balance, a.updated FROM accounts  as a WHERE user_id='" . $this->session->userdata('user_id') . "' " . $active . " ORDER BY a.accounts_name ASC");
        $result = $query->result();
        return $result;
    }

    // Start Report Page
    public function getAccountStatement($account, $from_date, $to_date, $trans_type, $order = 'desc')
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("user_id", $this->session->userdata('user_id'));
        if($account != 'ALL') {
            $this->db->where("accounts_name", $account);
        }
        // Increase date by one day
        $next_date = date(DATE_ONLY_FORMAT, strtotime($to_date . ' +0 day'));
        $this->db->where("trans_date BETWEEN '{$from_date}' AND '{$next_date}'");
        if ($trans_type != 'All') {
            $this->db->where($trans_type . " > 0");
        }

        $this->db->order_by("trans_date", $order);
        $this->db->order_by("trans_id", $order);

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // Transfer Report
    public function getTransferReport($from_date, $to_date)
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("type", TYPE_TRANSFER);
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->where("trans_date BETWEEN '$from_date' AND '$to_date'");
        $this->db->order_by("trans_date", "DESC");
        $this->db->order_by("trans_id", "DESC");
        return $this->db->get()->result();
    }

    // Category Report
    public function getCategoryReport($from_date, $to_date, $cat)
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where_in("category", $cat);
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->where("trans_date BETWEEN '$from_date' AND '$to_date'");
        $this->db->order_by("trans_date", "DESC");
        $this->db->order_by("trans_id", "DESC");
        return $this->db->get()->result();
    }

    // Payer Report
    public function getPayerReport($from_date, $to_date, $payer)
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("payer", $payer);
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->where("trans_date BETWEEN '$from_date' AND '$to_date'");
        $this->db->order_by("trans_date", "DESC");
        $this->db->order_by("trans_id", "DESC");
        return $this->db->get()->result();
    }

    // Payee Report
    public function getPayeeReport($from_date, $to_date, $payee)
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("payee", $payee);
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->where("trans_date BETWEEN '$from_date' AND '$to_date'");
        $this->db->order_by("trans_date", "DESC");
        $this->db->order_by("trans_id", "DESC");
        return $this->db->get()->result();
    }

    public function getTransactReport($type = TYPE_EXPENSE)
    {
        // Set the appropriate columns and group by based on the transaction type
        $columns = ($type == TYPE_EXPENSE) ? 'SUM(t.dr) as amount, t.type, t.payee AS transname' : 'SUM(t.cr) as amount, t.type, t.payer AS transname';
        $group_by = ($type == TYPE_EXPENSE) ? 't.payee' : 't.payer';

        $this->db->select($columns)
            ->from('lending_transaction lt')
            ->join('transaction t', 't.trans_id = lt.trans_id', 'left')
            ->where("t.type", $type)
            ->group_by($group_by);

        return $this->db->get()->result();
    }

    public function getCommonTransactReport($transact_data1, $transact_data2)
    {
        // Identify which set is for expense and which is for income
        if (count($transact_data1) >= count($transact_data2)) {
            return $this->getTransact($transact_data1, $transact_data2, 'expense', 'income');
        } else {
            return $this->getTransact($transact_data2, $transact_data1, 'income', 'expense', false);
        }
    }

    public function getTransact($transact_data1, $transact_data2, $title1, $title2, $isExpense = true)
    {
        $common_data = [];

        // Compare matching transactions between both data sets
        foreach ($transact_data1 as $edata) {
            $single = [
                'name' => $edata->transname,
                $title1 => $edata->amount,
                $title2 => 0,
                'amount' => $edata->amount
            ];

            // Check for matching entries in transact_data2
            foreach ($transact_data2 as $idata) {
                if ($edata->transname == $idata->transname) {
                    $single[$title2] = $idata->amount;
                    $single['amount'] = $isExpense ? ($edata->amount - $idata->amount) : ($idata->amount - $edata->amount);
                    break;  // Exit loop once a match is found
                }
            }

            $common_data[] = $single;
        }

        // Add transactions from transact_data2 that do not exist in transact_data1
        $diff = array_udiff($transact_data2, $transact_data1, function ($obj_a, $obj_b) {
            return strcmp($obj_a->transname, $obj_b->transname);
        });

        foreach ($diff as $df) {
            $common_data[] = [
                'name' => $df->transname,
                $title1 => 0,
                $title2 => $df->amount,
                'amount' => $df->amount
            ];
        }

        // Sort the result by amount in descending order
        usort($common_data, function ($a, $b) {
            return $b['amount'] <=> $a['amount'];
        });

        return $common_data;
    }
}
