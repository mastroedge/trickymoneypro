<?php
class Language_model extends CI_Model {
    protected $table = 'language';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_phrases() {
        return $this->db->get($this->table)->result();
    }

    public function insert_phrase($data) {
        return $this->db->insert($this->table, $data);
    }
}
