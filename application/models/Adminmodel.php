<?php

class AdminModel extends CI_Model
{

    // get all Chart of Accounts
    public function getAllChartOfAccounts()
    {
        $this->db->select('*');
        $this->db->from('chart_of_accounts');
        $this->db->order_by("accounts_name", "asc");
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // get all Chart of Accounts
    public function getAllChartOfAccountsSplit()
    {
        $result = $this->getAllChartOfAccounts();
        $list = array();
        foreach ($result as $res) {
            if (array_key_exists($res->accounts_type, $list)) {
                array_push($list[$res->accounts_type], $res);
            } else {
                $list[$res->accounts_type] = array($res);
            }
        }
        return $list;
    }

    // get all payer and payee or by type and status
    public function getPayerAndPayee($type = '', $status = '')
    {
        $this->db->select('*');
        $this->db->from('payee_payers');

        // Apply user filter
        $this->db->where("user_id", $this->session->userdata('user_id'));

        // Apply type filter if provided
        if (!empty($type)) {
            $this->db->where("type", $type);
        }

        // Apply status filter if provided
        if (!empty($status)) {
            $this->db->where("status", $status);
        }

        // Set order
        $this->db->order_by("payee_payers", "asc");

        // Execute query
        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    // get all user
    public function getAllUsers()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->order_by("user_id", "desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // get all settings
    public function getAllSettings()
    {
        $this->db->select('*');
        $this->db->from('settings');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // get all Personal/Bank Account
    public function getAllAccounts($active = -1)
    {
        switch ($active) {
            case 2:
                // Retutn Only Active
                $this->db->where_not_in("active", array(ACT_TYPE_DIS, ACT_TYPE_DIS_DIS, ACT_TYPE_DEL));
                break;
            case 1:
                // Return only Active accounts (with display disabled)
                $this->db->where_in("active", array(ACT_TYPE_ACT, ACT_TYPE_DIS_DIS));
                break;
            case 0:
                // Return Active accounts (with display disabled) & Disabled accounts
                $this->db->where_in("active", array(ACT_TYPE_ACT, ACT_TYPE_DIS, ACT_TYPE_DIS_DIS));
                break;
            default:
                break;
        }
        $this->db->select('*');
        $this->db->from('accounts');
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->order_by("accounts_name", "asc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // get account by id
    // TODO DELETE
    public function getAccount($accounts_id)
    {
        $this->db->select('*');
        $this->db->from('accounts');
        $this->db->where('accounts_id', $accounts_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    /**
     * Method for calculating balance from account name
     */
    public function getBalance($account_name, $amount, $action)
    {
        // get last balance and do action
        return $this->calculateAmount($this->getCurrentBalanceByAccountName($account_name), $amount, $action);
    }

    public function calculateAmount($amount1, $amount2, $action)
    {
        // Ensure amounts are numbers; default to 0 if not set
        $amount1 = is_numeric($amount1) ? $amount1 : 0;
        $amount2 = is_numeric($amount2) ? $amount2 : 0;

        // Perform the calculation based on the action
        switch ($action) {
            case 'add':
                return $amount1 + $amount2;
            case 'sub':
                return $amount1 - $amount2;
            default:
                return 'Invalid action'; // Return message if action is not valid
        }
    }

    /**
     * get Current Balance By AccountName
     */
    public function getCurrentBalanceByAccountName($account_name)
    {
        $result = $this->getLastTransactionByAccountName($account_name);
        return $result->bal;
    }

    /**
     * Get Last Transaction By AccountName
     */
    public function getLastTransactionByAccountName($account_name)
    {
        $this->db->select('*')
            ->from('transaction')
            ->where('accounts_name', $account_name)
            ->where('user_id', $this->session->userdata('user_id'))
            ->order_by('trans_date', 'DESC')
            ->order_by('trans_id', 'DESC')
            ->limit(1);

        $query = $this->db->get();
        return $query->row();
    }

    /**
     * Get transactions by Type
     * Income / Expense
     */
    public function getTransaction($limit = '', $type = '')
    {
        $sqllimit = $limit;
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where("user_id", $this->session->userdata('user_id'));

        if ($type != '') {
            $this->db->where('type', $type);
        }
        $this->db->order_by("trans_date", "desc");
        $this->db->order_by("trans_id", "desc");

        if ($limit != 'all') {
            if ($type == TYPE_TRANSFER) {
                $sqllimit = $limit + 15;
            }
            $this->db->limit($sqllimit);
        }

        $query_result = $this->db->get();
        $result = $query_result->result();

        if ($type == TYPE_TRANSFER) {
            $result = $this->getTransfer($result, $limit);
        }

        return $result;
    }

    /**
     * Get transactions by Type
     * Transfer
     */
    public function getTransfer($transactions, $limit = null)
    {
        $debitTransactions = [];
        $creditTransactions = [];

        // Separate debit and credit transactions
        foreach ($transactions as $transaction) {
            if ($transaction->cr == 0) {
                $debitTransactions[] = $transaction;
            } else {
                $creditTransactions[] = $transaction;
            }
        }

        $transfers = [];

        // Process credit and debit transactions
        foreach ($creditTransactions as $creditTransaction) {
            foreach ($debitTransactions as $key => $debitTransaction) {
                if (
                    $creditTransaction->trans_date == $debitTransaction->trans_date &&
                    $creditTransaction->amount == $debitTransaction->amount &&
                    $creditTransaction->cr == $debitTransaction->dr
                ) {
                    // Merge account names and add to transfers
                    $creditTransaction->accounts_name = merge_account_by_delimiter([
                        $debitTransaction->accounts_name,
                        $creditTransaction->accounts_name
                    ]);

                    $transfers[] = $creditTransaction;

                    // Remove matched debit transaction for efficiency
                    unset($debitTransactions[$key]);
                    break;
                }
            }
        }

        // Apply limit if specified
        if ($limit !== null) {
            $transfers = array_slice($transfers, 0, $limit);
        }

        return $transfers;
    }


    /**
     * swap Transaction between
     * Income / Expense
     */
    public function swapTransaction($curr_trans_id, $account_to)
    {
        $from_transaction = $this->getSingleTransactionById($curr_trans_id);

        // If Type Transfer Return false
        if ($from_transaction->type == TYPE_TRANSFER) return;

        $this->db->trans_begin();

        /**
         * UPDATE IN TO ACCOUNT - START
         */
        // Update Account Name of TO ACCOUNT in Transaction
        $updateTrans = array();
        $updateTrans['accounts_name'] = $account_to;
        $this->db->where('trans_id', $curr_trans_id);
        $this->db->update('transaction', $updateTrans);

        /**
         * UPDATE IN FROM ACCOUNT - START
         */
        $this->updateAccountBalanceByDate($from_transaction->trans_date, $from_transaction->accounts_name);
        /**
         * UPDATE IN FROM ACCOUNT - END
         */

        // Update Current Balance of TO ACCOUNT
        $this->updateAccountBalanceByDate($from_transaction->trans_date, $account_to);
        /**
         * UPDATE IN TO ACCOUNT - END
         */

        // end transaction
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    /**
     * Update Current Balance from transaction by amount (add / sub)
     */
    private function updateLastBalanceOfAccountFromTransaction($to_accounts_name, $amount, $calculation)
    {
        $last_trans = $this->getLastTransactionByAccountName($to_accounts_name);
        $up_cur_bal = array();
        $up_cur_bal['bal'] = $this->calculateAmount($last_trans->bal, $amount, $calculation);
        $this->db->where('trans_id', $last_trans->trans_id);
        $this->db->update('transaction', $up_cur_bal);
    }

    /**
     * Helper function to update balances after a transaction is deleted
     */
    public function updateAccountBalance($transfer_trans_id, $account_name = '')
    {
        if (empty($transfer_trans_id)) return false;
        $transfer_transaction = $this->getSingleTransactionById($transfer_trans_id);
        if (empty($account_name)) {
            $account_name = $transfer_transaction->accounts_name;
        }
        $trans_date = $transfer_transaction->trans_date;
        if ($this->updateAccountBalanceByDate($trans_date, $account_name)) {
            echo "1";
        } else {
            echo "0";
        }
    }

    /**
     * Update Account Balance By Date
     */
    public function updateAccountBalanceByDate($trans_date, $account_name)
    {
        $previous_day = date(DATE_ONLY_FORMAT, strtotime($trans_date . ' -5 years'));
        $this->db->where('trans_date >=', $previous_day);
        $this->db->where('accounts_name', $account_name); // Filter by the account
        $this->db->order_by('trans_date', 'ASC');
        $this->db->order_by('trans_id', 'ASC');

        $subsequent_transactions = $this->db->get('transaction')->result();

        $updateArray = array();
        $current_bal = 0;
        // Recalculate the balance for each transaction
        foreach ($subsequent_transactions as $trans) {
            $updateData = array();
            // Recalculate balance based on debit and credit
            if ($trans_date > $trans->trans_date) {
                $current_bal = $trans->bal;
            } else if ($trans_date <= $trans->trans_date) {
                $updateData = array(
                    'trans_id' => $trans->trans_id
                );

                if ($trans->dr > 0) {
                    $current_bal -= $trans->dr;
                }
                if ($trans->cr > 0) {
                    $current_bal += $trans->cr;
                }

                $updateData['bal'] = $current_bal;
                $updateArray[] = $updateData;
            }
        }

        // Check if $updateArray has more than 0 elements
        if (count($updateArray) > 0) {
            // Update the transaction with the new balance
            $this->db->update_batch('transaction', $updateArray, 'trans_id');
            // Check if the query was successful
            if ($this->db->affected_rows() > 0) {
                // Success
                return true;
            } else {
                // No rows were updated
                return false;
            }
        } else {
            // No data to update
            return false;
        }
        // print_r($updateArray, TRUE);
        // $queries = $this->db->queries; // Array of all queries
        // // $times = $this->db->query_times; // Array of execution times for each query
        // foreach ($queries as $query) {
        //     echo $query . '<br>';
        // }
    }

    public function getLendingTransactionArray()
    {
        $this->db->select('trans_id');
        $this->db->from('lending_transaction');
        $lending_result = $this->db->get();
        $lend_array = array();
        foreach ($lending_result->result() as $key => $lending) {
            $lend_array[$key] = $lending->trans_id;
        }
        return $lend_array;
    }

    public function getTransactionAjax($type = '')
    {
        $isMobile = false;
        $deviceType = getDeviceType();
        if ($deviceType === 'Mobile') {
            $isMobile = true;
        }

        $lend_array = $this->getLendingTransactionArray();

        if ($type == TYPE_EXPENSE) {
        } else {
        }
        $this->load->model('Datatable_model');
        $this->Datatable_model->table = 'transaction';
        if ($type == TYPE_EXPENSE) {
            $this->Datatable_model->column_order = array(null, 'trans_date', 'accounts_name', 'category', 'payee'); //set column field database for datatable orderable
            $this->Datatable_model->column_search = array('trans_date', 'accounts_name', 'category', 'amount', 'payee', 'ref', 'note'); //set column field database for datatable searchable
        } else {
            $this->Datatable_model->column_order = array(null, 'trans_date', 'accounts_name', 'category', 'payer'); //set column field database for datatable orderable
            $this->Datatable_model->column_search = array('trans_date', 'accounts_name', 'category', 'amount', 'payer', 'ref', 'note'); //set column field database for datatable searchable
        }
        $this->Datatable_model->order = array('trans_date' => 'desc', 'trans_id' => 'desc'); // default order
        $this->Datatable_model->where = array('type' => $type, "user_id", $this->session->userdata('user_id'));

        $list = $this->Datatable_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            // $row[] = $no;
            $row[] = displayDateCalendar($item->trans_date, ($isMobile ? 'size0_75x' : 'size1x'));
            $row[] = $item->accounts_name;
            $row[] = displayMoney($item->amount);
            $row[] = $item->category;
            $name = '';
            if ($type == TYPE_EXPENSE) {
                $name = $item->payee;
            } else {
                $name = $item->payer;
            }
            $row[] = $name;
            $row[] = $item->ref . '</br>' . $item->note;
            // $row[] = $item->note;
            $row[] = $item->p_method;
            $l_action = 'insert';
            $l_icon = '<i class="fa fa-user-plus" aria-hidden="true"></i>';
            if (in_array($item->trans_id, $lend_array)) {
                $l_action = 'remove';
                $l_icon = '<i class="fa fa-user-times" aria-hidden="true"></i>';
            }
            $row[] = '<a class="myicon" href="' . site_url('Admin/editTransaction/' . $type . '/' . $item->trans_id) . '" data-toggle="tooltip" title="Click For Edit"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
        <a href="" onclick ="addToTransact(event, \'' . $item->trans_id . '\', \'' . site_url() . '\')" class="myicon aicon ' . $l_action . '">' . $l_icon . '</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Datatable_model->count_all(),
            "recordsFiltered" => $this->Datatable_model->count_filtered(),
            "data" => $data,
        );

        return $output;
    }

    // get single transaction
    public function getSingleTransactionById($trans_id)
    {
        $this->db->select('*');
        $this->db->from('transaction');
        $this->db->where('trans_id', $trans_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    // Process Repeating Transaction
    public function processRepeatTransaction($trans_id, $status)
    {
        $this->load->model('Transaction_model');
        $this->db->trans_begin();

        $data['status'] = $status;
        $data['pdate'] = date(DATETIME_FORMAT_DB);
        $this->db->where('trans_id', $trans_id);
        $this->db->update('repeat_transaction', $data);

        $trans = $this->Repeat_transaction_model->getSingleRepeatTransaction($trans_id);

        $transaction = $this->Transaction_model->mapTransactionFromRepeatTransaction($trans);

        // echo '<pre>';print_r($transaction);echo '</pre>';
        $this->Transaction_model->insertTransaction($transaction);

        // UPDATE BALANCE if its intermediate transaction
        if ($transaction->gettype() == TYPE_INCOME || $transaction->gettype() == TYPE_EXPENSE) {
            $last_transaction = $this->Adminmodel->getLastTransactionByAccountName($transaction->getAccountsName());
            if ($last_transaction->trans_date > $transaction->getTransDate()) {
                $this->Adminmodel->updateAccountBalanceByDate($transaction->getTransDate(), $transaction->getAccountsName());
            }
        } else {
            $accounts_name_transfer = split_account_by_delimiter($transaction->getAccountsName());
            $last_transaction = $this->Adminmodel->getLastTransactionByAccountName($accounts_name_transfer[0]);
            if ($last_transaction->trans_date > $transaction->getTransDate()) {
                $this->Adminmodel->updateAccountBalanceByDate($transaction->getTransDate(), $accounts_name_transfer[0]);
            }
            $last_transaction = $this->Adminmodel->getLastTransactionByAccountName($accounts_name_transfer[1]);
            if ($last_transaction->trans_date > $transaction->getTransDate()) {
                $this->Adminmodel->updateAccountBalanceByDate($transaction->getTransDate(), $accounts_name_transfer[1]);
            }
        }

        // end transaction
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    /**
     * Split Transactions By Date
     */
    function splitTransactionsByDate($transactions)
    {
        $beforeToday = [];
        $onOrAfterToday = [];

        $today = new DateTime(); // Current date
        $currentMonthStart = new DateTime($today->format('Y-m-01')); // Start of current month
        $nextMonthStart = (clone $currentMonthStart)->modify('+1 month'); // Start of next month

        foreach ($transactions as $transaction) {
            $transDate = new DateTime($transaction->trans_date);

            if ($transDate < $nextMonthStart) {
                $beforeToday[] = $transaction; // Transactions before this month
            } else {
                $onOrAfterToday[] = $transaction; // Transactions on or after month
            }
        }
        return [
            'before' => $beforeToday,
            'onOrAfter' => $onOrAfterToday,
        ];
    }

    /**
     * Split Transactions By Month in the current year
     */
    function splitTransactionsByCurrentYear($transactions)
    {
        $today = new DateTime(); // Current date
        $groupedTransactions = []; // Array to hold transactions by month
        $currentYear = $today->format('Y'); // Current year

        foreach ($transactions as $transaction) {
            $transDate = new DateTime($transaction->trans_date);
            $transYear = $transDate->format('Y'); // Extract year of the transaction

            if ($transYear == $currentYear) {
                // Group transactions by month for the current year
                $monthKey = $transDate->format('Y-m'); // e.g., '2024-10'

                if (!isset($groupedTransactions[$monthKey])) {
                    $groupedTransactions[$monthKey] = [];
                }

                $groupedTransactions[$monthKey][] = $transaction;
            } else {
                // Group transactions by year for previous years
                if (!isset($groupedTransactions[$transYear])) {
                    $groupedTransactions[$transYear] = [];
                }

                $groupedTransactions[$transYear][] = $transaction;
            }
        }

        // echo '<pre>';print_r($groupedTransactions);echo '</pre>';
        return $groupedTransactions;
    }

    /**
     * Split Transactions By Upcomming 6 Months
     */
    function splitTransactionsByMonths($transactions)
    {
        $today = new DateTime(); // Current date
        $groupedTransactions = []; // Array to hold transactions by month

        $currentMonthStart = (clone $today)->modify('first day of this month'); // Start of the current month
        $nextFiveMonthsEnd = (clone $currentMonthStart)->modify('+5 months')->modify('last day of this month'); // End of the next 5 months

        foreach ($transactions as $transaction) {
            $transDate = new DateTime($transaction->trans_date);

            if ($transDate >= $currentMonthStart && $transDate <= $nextFiveMonthsEnd) {
                // Group transactions by month for the current month and next 5 months
                $monthKey = $transDate->format('Y-m'); // e.g., '2024-10'

                if (!isset($groupedTransactions[$monthKey])) {
                    $groupedTransactions[$monthKey] = [];
                }

                $groupedTransactions[$monthKey][] = $transaction;
            } else {
                // Group transactions by year for older or later transactions
                $transYear = $transDate->format('Y'); // Extract year of the transaction

                if (!isset($groupedTransactions[$transYear])) {
                    $groupedTransactions[$transYear] = [];
                }

                $groupedTransactions[$transYear][] = $transaction;
            }
        }

        return $groupedTransactions;
    }

    /**
     * Convert SQL Result Object to Select Box Array
     */
    function resultsObjectToArray($result, $key, $value)
    {
        $val = array();
        foreach ($result as $a) {
            $val[$a->{$key}] = $a->{$value};
        }
        return $val;
    }

    function resultsObjectToArrayObject($result, $key)
    {
        $val = array();
        foreach ($result as $a) {
            $val[$a->{$key}] = $a;
        }
        return $val;
    }

    function resultsObjectToIdArray($result, $key)
    {
        $ids = array();
        foreach ($result as $a) {
            $ids[] = $a->{$key};
        }
        return $ids;
    }

    function resultObjectToObject($result, $key, $value)
    {
        return current(array_filter($result, function ($a) use ($key, $value) {
            return isset($a->{$key}) && $a->{$key} === $value;
        })) ?: null;
        // foreach ($result as $a) {
        //     if (isset($a->{$key}) && $a->{$key} === $value) {
        //         return $a;
        //     }
        // }
        // return null;
    }

    public function getAllTasks($limit = 10)
    {
        $this->db->select('*');
        $this->db->from('tasks');
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->limit($limit);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getSingleTask($task_id)
    {
        $this->db->select('*');
        $this->db->from('tasks');
        $this->db->where('task_id', $task_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function getTaskAjax()
    {
        $this->load->model('Datatable_model');
        $this->Datatable_model->table = 'tasks';
        $this->Datatable_model->column_order = array(null, 'startdate', 'note', 'priority', 'duedate'); //set column field database for datatable orderable
        $this->Datatable_model->column_search = array('startdate', 'note', 'priority', 'duedate'); //set column field database for datatable searchable
        $this->Datatable_model->order_by = "CASE
                                                WHEN `status` = 'Pending' && `priority` = 'High' then 1
                                                WHEN `status` = 'Pending' && `priority` = 'Medium' then 2
                                                WHEN `status` = 'Pending' && `priority` = 'Low' then 3
                                                WHEN `status` = 'Partial' && `priority` = 'High' then 4 
                                                WHEN `status` = 'Partial' && `priority` = 'Medium' then 5 
                                                WHEN `status` = 'Partial' && `priority` = 'Low' then 6
                                                WHEN `status` = 'Complete' && `priority` = 'High' then 7
                                                WHEN `status` = 'Complete' && `priority` = 'Medium' then 8
                                                WHEN `status` = 'Complete' && `priority` = 'Low' then 9
                                            END ASC";
        $this->Datatable_model->where = array("user_id", $this->session->userdata('user_id'));

        $list = $this->Datatable_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $item) {
            $no++;
            $row = array();
            // $row[] = $no;
            $row[] = $item->startdate;
            $row[] = $item->note;
            $row[] = $item->priority;
            $row[] = $item->duedate;
            $row[] = $item->datefinished;
            $row[] = $item->status;
            $row[] = '<a class="btn btn-primary mybtn btn-info btn-xs expense-manage-btn" href="' . site_url('Admin/editTask/' . $item->task_id) . '" data-toggle="tooltip" 
        title="Click For Edit">Manage</a>';
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Datatable_model->count_all(),
            "recordsFiltered" => $this->Datatable_model->count_filtered(),
            "data" => $data,
        );

        return $output;
    }

    /**
     * Calculate total amount from Transactions
     * for Income / Expense (including transfer)
     */
    public function calculateTotalFromTransactions($t_data)
    {
        $t_total = 0;
        foreach ($t_data as $t) {
            $t_total = $t_total + $t->amount;
        }
        return $t_total;
    }

    /**
     * Calculate total amount from Transactions by Type
     * for Income / Expense
     */
    public function calculateTotalFromTransactionsByType($t_data)
    {
        $totals = [
            TYPE_INCOME => 0,
            TYPE_EXPENSE => 0,
            TYPE_INCOME . AMOUNT_CR => 0,
            TYPE_EXPENSE . AMOUNT_DR => 0,
        ];

        foreach ($t_data as $t) {
            switch ($t->type) {
                case TYPE_INCOME:
                    $totals[TYPE_INCOME] += $t->amount;
                    break;
                case TYPE_EXPENSE:
                    $totals[TYPE_EXPENSE] += $t->amount;
                    break;
                case TYPE_TRANSFER:
                    $totals[TYPE_INCOME . AMOUNT_CR] += $t->cr; // Sum credit for transfers
                    $totals[TYPE_EXPENSE . AMOUNT_DR] += $t->dr; // Sum debit for transfers
                    break;
            }
        }

        return $totals;
    }

    public function add_breadcrumb($title, $top_menu = array())
    {
        $html = '';
        $html = '<div class="row mx-0 content-title">';
        $html .= '<div class="col-md-10 col-lg-10 col-sm-10 col-10">';
        $html .= '<nav aria-label="breadcrumb">';
        $html .= '<ol class="breadcrumb">';
        $html .= '<li class="breadcrumb-item"><a href="' . site_url("/") . '">Home</a></li>';
        $html .= '<li class="breadcrumb-item active" aria-current="page">' . $title . '</li>';
        $html .= '</ol>';
        $html .= '</nav>';
        $html .= '</div>';
        $html .= '<div class="col-md-2 col-lg-2 col-sm-2 col-2 text-end">';
        $html .= '<a href="javascript:void(0);" onclick="location.reload();"><i class="fa fa-refresh"></i></a>';
        $html .= '</div>';
        $html .= '</div>';
        if (!empty($top_menu)) {
            $html .= '<ul class="top-bar-menu">';
            foreach ($top_menu as $top) {
                $html .= '<li>';
                $html .= '<a href="' . $top['link'] . '" ' . (!empty($top['active']) ? 'class="active"' : '') . '><i class="' . $top['icon'] . '" aria-hidden="true"></i> ' . $top['menu'] . '</a>';
                $html .= '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}
