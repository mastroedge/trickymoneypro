<?php
class Lending_transaction_model extends CI_Model {
    protected $table = 'lending_transaction';

    public function __construct() {
        parent::__construct();
    }

    public function get_lending_transaction_by_id($lend_id) {
        return $this->db->get_where($this->table, ['lend_id' => $lend_id])->row();
    }

    public function insert_lending_transaction($data) {
        return $this->db->insert($this->table, $data);
    }
}
