<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends CI_Model
{
    protected $table = 'transaction';
    var $slno;
    var $account_balances;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_all_transactions() {
        return $this->db->get($this->table)->result();
    }

    public function insert_transaction($data) {
        return $this->db->insert($this->table, $data);
    }

    public function get_transaction_by_id($trans_id) {
        return $this->db->get_where($this->table, ['trans_id' => $trans_id])->row();
    }
    
    /**
     * get Income Expense Data including Transfer in it
     */
    public function getIncomeExpenseData($tdata)
    {
        $type = (isset($tdata['type']) ? $tdata['type'] : []);
        $from_date = (isset($tdata['from_date']) ? $tdata['from_date'] : '');
        $to_date = (isset($tdata['to_date']) ? $tdata['to_date'] : '');
        $group = (isset($tdata['group']) ? $tdata['group'] : '');
        $payee = (isset($tdata['payee']) ? $tdata['payee'] : '');
        $payer = (isset($tdata['payer']) ? $tdata['payer'] : '');
        $category = (isset($tdata['category']) ? $tdata['category'] : '');
        $accounts_name = (isset($tdata['accounts_name']) ? $tdata['accounts_name'] : '');
        $limit = (isset($tdata['limit']) ? $tdata['limit'] : '');
        $order = (isset($tdata['order']) ? $tdata['order'] : 'DESC');

        $this->db->from('transaction');
        if (!empty($accounts_name)) {
            $this->db->where("accounts_name", $accounts_name);
        }
        if (is_array($type)) {
            $this->db->where_in("type", $type);
            if (in_array(TYPE_INCOME, $type)) {
                $this->db->where("cr > 0");
            } else if (in_array(TYPE_EXPENSE, $type)) {
                $this->db->where("dr > 0");
            } else if (in_array(TYPE_TRANSFER, $type)) {
            }
        } elseif (is_string($type)) {
            $this->db->where("type", $type);
        }

        $this->db->where("user_id", $this->session->userdata('user_id'));
        if ($group == "group") {
            $this->db->select('trans_date,sum(amount) as amount');
            $this->db->group_by('trans_date');
        } else {
            $this->db->select('*');
        }
        if (!empty($payee)) {
            $this->db->where("payee", $payee);
        }
        if (!empty($payer)) {
            $this->db->where("payer", $payer);
        }
        if (!empty($category)) {
            $this->db->where_in("category", $category);
        }
        if (!empty($from_date) && !empty($to_date)) {
            $this->db->where("trans_date BETWEEN '$from_date' AND '$to_date'");
        }
        if (!empty($limit)) {
            $this->db->limit($limit);
        }
        $this->db->order_by("trans_date", $order);
        $this->db->order_by("trans_id", $order);
        return $this->db->get()->result();
    }

    public function insertTransactionInitial($account, $amount, $date)
    {
        $data = array();
        $data['accounts_name'] = $account;
        $data['trans_date'] = $date;
        $data['type'] = TYPE_TRANSFER;
        $data['category'] = '';
        $data['amount'] = $amount;
        $data['payer'] = 'System';
        $data['payee'] = '';
        $data['p_method'] = '';
        $data['ref'] = '';
        $data['note'] = 'Opening Balance';
        $data['dr'] = 0;
        $data['cr'] = $amount;
        $data['bal'] = $amount;
        $data['user_id'] = $this->session->userdata('user_id');
        $this->db->insert($this->table, $data);
    }

    /**
     * This function set value to Insert to DB
     * This method prepares the data for insertion into the database based on transaction types (income, expense, transfer)
     */
    // FIXME:: replace insertTransaction()
    function insertTransaction($transaction)
    {
        $array = $transaction->toArray();
        $array['user_id'] = $this->session->userdata('user_id');

        // Remove key from the array
        unset($array['trans_id']);

        if ($transaction->gettype() == TYPE_INCOME) {
            unset($array['payee']);
            $array['dr'] = 0;
            $array['cr'] = $transaction->getAmount();
            $array['bal'] = $this->getBalance($transaction->getAccountsName(), $transaction->getAmount(), "add");
        } else if ($transaction->gettype() == TYPE_EXPENSE) {
            unset($array['payer']);
            $array['dr'] = $transaction->getAmount();
            $array['cr'] = 0;
            $array['bal'] = $this->getBalance($transaction->getAccountsName(), $transaction->getAmount(), "sub");
        } else if ($transaction->gettype() == TYPE_TRANSFER) {
            unset($array['payee'], $array['payer'], $array['category']);

            $accounts_name_transfer = split_account_by_delimiter($transaction->getAccountsName());
            $array_list = [];

            $array['accounts_name'] = $accounts_name_transfer[0];
            $array['dr'] = $transaction->getAmount();
            $array['cr'] = 0;
            $array['bal'] = $this->getBalance($accounts_name_transfer[0], $transaction->getAmount(), "sub");
            $array_list[] = $array;

            $array['accounts_name'] = $accounts_name_transfer[1];
            $array['dr'] = 0;
            $array['cr'] = $transaction->getAmount();
            $array['bal'] = $this->getBalance($accounts_name_transfer[1], $transaction->getAmount(), "add");
            $array_list[] = $array;

            $array = $array_list;
        }

        if ($transaction->gettype() == TYPE_TRANSFER) {
            $this->db->insert_batch('transaction', $array);
        } else {
            $this->db->insert('transaction', $array);
        }
    }

    function getBalance($account_name, $amount, $math)
    {
        if (!empty($this->account_balances[$account_name])) {
            return $this->account_balances[$account_name] = $this->Adminmodel->calculateAmount($this->account_balances[$account_name], $amount, $math);
        } else {
            return $this->Adminmodel->getBalance($account_name, $amount, $math);
        }
    }

    function getCurrentBalances($account_names = array())
    {
        // Subquery 1: Get the maximum trans_date for each accounts_name
        $subquery1 = $this->db->select('accounts_name, MAX(trans_date) AS max_trans_date')
            ->from('transaction')
            ->group_by('accounts_name')
            ->get_compiled_select();

        // Subquery 2: Get the maximum trans_id for each accounts_name and trans_date
        $subquery2 = $this->db->select('accounts_name, trans_date, MAX(trans_id) AS max_trans_id')
            ->from('transaction')
            ->group_by('accounts_name, trans_date')
            ->get_compiled_select();

        // Main query
        $this->db->select('t1.accounts_name, t1.trans_date AS latest_transaction, t1.bal AS current_balance');
        $this->db->from('transaction t1');
        $this->db->join("($subquery1) t2", 't1.accounts_name = t2.accounts_name AND t1.trans_date = t2.max_trans_date', 'inner');
        $this->db->join("($subquery2) t3", 't1.accounts_name = t3.accounts_name AND t1.trans_date = t3.trans_date AND t1.trans_id = t3.max_trans_id', 'inner');
        $this->db->where('t1.user_id', $this->session->userdata('user_id'));  // Assuming user_id is from session
        if (!empty($account_names)) {
            $this->db->where_in('t1.accounts_name', $account_names);  // Filter for specific account names
        }

        // Execute the query
        $query = $this->db->get();
        $result = $query->result();

        // Convert result to associative array with account names as keys
        $balance_array = [];
        foreach ($result as $row) {
            $balance_array[$row->accounts_name] = $row->current_balance;
        }

        $this->account_balances = $balance_array;
    }

    function mergeAccountNameForTransfer($fromAccount, $toAccount)
    {
        $accounts_name_transfer = [];
        $accounts_name_transfer[] = $fromAccount;
        $accounts_name_transfer[] = $toAccount;
        return merge_account_by_delimiter($accounts_name_transfer);
    }

    function mapTransactionFromRepeatTransaction($repeatTransaction)
    {
        $transaction = new Transaction();
        $transaction->setAccountsName($repeatTransaction->accounts_name);
        $transaction->setTransDate($repeatTransaction->trans_date);
        $transaction->settype($repeatTransaction->type);
        $transaction->setCategory($repeatTransaction->category);
        $transaction->setAmount($repeatTransaction->amount);
        $transaction->setPayer($repeatTransaction->payer);
        $transaction->setPayee($repeatTransaction->payee);
        $transaction->setPMethod($repeatTransaction->p_method);
        $transaction->setRef($repeatTransaction->ref);
        $transaction->setNote($repeatTransaction->note);
        $transaction->setUserId($repeatTransaction->user_id);

        return $transaction;
    }

    function mapTransactionFromTransaction($t_transaction)
    {
        $transaction = new Transaction();
        $transaction->setTransId($t_transaction->trans_id);
        $transaction->setAccountsName($t_transaction->accounts_name);
        $transaction->setTransDate($t_transaction->trans_date);
        $transaction->settype($t_transaction->type);
        $transaction->setCategory($t_transaction->category);
        $transaction->setAmount($t_transaction->amount);
        $transaction->setPayer($t_transaction->payer);
        $transaction->setPayee($t_transaction->payee);
        $transaction->setPMethod($t_transaction->p_method);
        $transaction->setRef($t_transaction->ref);
        $transaction->setNote($t_transaction->note);
        $transaction->setDr($t_transaction->dr);
        $transaction->setCr($t_transaction->cr);
        $transaction->setBal($t_transaction->bal);
        $transaction->setUserId($t_transaction->user_id);
    }
}
