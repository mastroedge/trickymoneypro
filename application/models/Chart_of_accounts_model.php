<?php
class Chart_of_accounts_model extends CI_Model {
    protected $table = 'chart_of_accounts';

    public function __construct() {
        parent::__construct();
    }

    public function get_all_chart_of_accounts() {
        return $this->db->get($this->table)->result();
    }

    public function insert_chart_of_account($data) {
        return $this->db->insert($this->table, $data);
    }

    public function get_chart_by_id($chart_id) {
        return $this->db->get_where($this->table, ['chart_id' => $chart_id])->row();
    }

    // get Chart of Accounts by type
    public function getChartOfAccountByType($type, $status = 'ALL', $sort = 'ASC')
    {
        $this->db->select('*');
        $this->db->from('chart_of_accounts');
        $this->db->where('accounts_type', $type);
        if($status != 'ALL') {
            $this->db->where("status", $status);
        }
        $this->db->where("user_id", $this->session->userdata('user_id'));
        $this->db->order_by("accounts_name", $sort);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
}
