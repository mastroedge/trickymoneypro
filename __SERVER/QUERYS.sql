INSERT INTO `settings` (`id`, `settings`, `value`) VALUES ('10', 'info_upto_date', '');
CREATE TABLE `tasks` (
  `task_id` int(11) NOT NULL,
  `note` text CHARACTER SET utf8 NOT NULL,
  `priority` varchar(10) NOT NULL,
  `dateadded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `startdate` date DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `datefinished` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `tasks` ADD PRIMARY KEY (`task_id`);
ALTER TABLE `tasks` MODIFY `task_id` int(11) NOT NULL AUTO_INCREMENT;
CREATE TABLE `lending_transaction` (
  `lend_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type` ENUM('Income','Expense','Transfer') NOT NULL,
  `payee` VARCHAR(30) NOT NULL,
  `trans_id` INT(11) NOT NULL,
  PRIMARY KEY (`lend_id`)
) ENGINE = InnoDB;
ALTER TABLE `lending_transaction` ADD UNIQUE(`trans_id`);
ALTER TABLE `accounts` ADD `active` BOOLEAN NOT NULL DEFAULT TRUE AFTER `note`;
ALTER TABLE `repeat_transaction` CHANGE `type` `type` ENUM('Income','Expense','Transfer') CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL;
ALTER TABLE `repeat_transaction` CHANGE `account` `account` VARCHAR(70) CHARACTER SET utf8mb3 COLLATE utf8mb3_bin NOT NULL;
CREATE TABLE `bulk_transaction` ( `trans_id` INT NOT NULL AUTO_INCREMENT , `tansactions` TEXT NOT NULL , `user_id` INT NOT NULL , PRIMARY KEY (`trans_id`)) ENGINE = InnoDB;
ALTER TABLE `accounts` ADD `updated` DATE NULL DEFAULT NULL AFTER `note`;
ALTER TABLE `transaction` CHANGE `trans_date` `trans_date` DATETIME NOT NULL;
ALTER TABLE `transaction` ADD `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `bal`;
ALTER TABLE `repeat_transaction` CHANGE `date` `date` DATETIME NOT NULL;
--UPDATE transaction SET trans_date = DATE_ADD(trans_date, INTERVAL (trans_id * 10) SECOND);
--UPDATE repeat_transaction SET date = DATE_ADD(date, INTERVAL (trans_id * 10) SECOND);
ALTER TABLE `accounts` CHANGE `updated` `updated` DATETIME NULL DEFAULT NULL;
ALTER TABLE `payee_payers` ADD `status` TINYINT(1) NOT NULL DEFAULT '1' AFTER `type`;
ALTER TABLE `repeat_transaction` CHANGE `description` `note` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;
ALTER TABLE `repeat_transaction` CHANGE `status` `status` ENUM('paid','unpaid','pending','receive') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER `pdate`;
ALTER TABLE `repeat_transaction` CHANGE `account` `accounts_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;
ALTER TABLE `repeat_transaction` CHANGE `date` `trans_date` DATETIME NOT NULL AFTER `accounts_name`;
ALTER TABLE `chart_of_accounts` ADD `status` TINYINT(1) NOT NULL DEFAULT '1' AFTER `accounts_type`;
ALTER TABLE `payment_method` ADD `status` TINYINT(1) NOT NULL DEFAULT '1' AFTER `p_method_name`;
ALTER TABLE `lending_transaction` DROP `type`, DROP `payee`;
ALTER TABLE `accounts` ADD COLUMN `account_type` ENUM('Bank', 'Credit Card', 'Fixed Deposit', 'Mutual Fund', 'Gold', 'Wallet', 'Insurance') NOT NULL DEFAULT 'Bank' AFTER `accounts_name`;
-- ALTER TABLE `transaction` ADD COLUMN `quantity` DOUBLE DEFAULT NULL AFTER `amount`;
ALTER TABLE `bulk_transaction` ADD `status` TINYINT(1) NOT NULL DEFAULT '1' AFTER `tansactions`;
CREATE TABLE `repeat_template` (`template_id` INT(11) NOT NULL AUTO_INCREMENT , `accounts_name` VARCHAR(100) NOT NULL , `rotation` VARCHAR(20) NOT NULL , `no_rotation` INT NOT NULL , `type` ENUM('Income','Expense','Transfer','') NOT NULL , `amount` DOUBLE NOT NULL , `payer` VARCHAR(30) NOT NULL , `payee` VARCHAR(30) NOT NULL , `p_method` VARCHAR(20) NOT NULL , `ref` VARCHAR(60) NOT NULL , `note` TEXT NOT NULL , `user_id` INT(11) NOT NULL , PRIMARY KEY (`template_id`)) ENGINE = InnoDB;
ALTER TABLE `repeat_transaction` ADD `template_id` INT(11) NOT NULL AFTER `status`;

CREATE TABLE vehicles (
    vehicle_id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    type ENUM('Car', 'Bike') NOT NULL,
    registration_no VARCHAR(50) UNIQUE NOT NULL,
    month_year VARCHAR(10) NULL,
    fuel_liters DOUBLE NULL,
    note TEXT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE vehicle_trips (
    trip_id INT AUTO_INCREMENT PRIMARY KEY,
    vehicle_id INT NOT NULL,
    trip_date DATETIME NOT NULL,
    meter_reading DOUBLE NOT NULL, -- Odometer reading
    fuel_percentage DOUBLE DEFAULT NULL, -- Fuel used (optional)
    note TEXT NOT NULL,
    distance_km DOUBLE DEFAULT NULL, -- Distance travelled (optional)
    FOREIGN KEY (vehicle_id) REFERENCES vehicles(vehicle_id) ON DELETE CASCADE
);

CREATE TABLE vehicle_fuels (
    fuel_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT NOT NULL,
    fuel_liters DOUBLE NOT NULL,
    fuel_price DOUBLE NOT NULL,
    FOREIGN KEY (trip_id) REFERENCES vehicle_trips(trip_id) ON DELETE CASCADE
);

CREATE TABLE vehicle_services (
    service_id INT AUTO_INCREMENT PRIMARY KEY,
    trip_id INT NOT NULL,
    service_type ENUM('Oil Change', 'General Service', 'Tyre Change', 'Battery Change', 'Other') NOT NULL,
    FOREIGN KEY (trip_id) REFERENCES vehicle_trips(trip_id) ON DELETE CASCADE
);