UPDATE `transaction` SET `type`='Transfer',`category`='',`payee`='' WHERE `payee`='Life Insurance Corporation' AND `trans_id` != 2108
UPDATE `transaction` SET `payee`='' WHERE `payee`='Life Insurance Corporation' AND `trans_id` != 2108

INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2016-08-12', 'Transfer', '', 5085, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-1', 'LIC 814/16 - 786277406 - 08/16 to 08/16 - 1st Payment', 0, 5085, 5085, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2016-11-12', 'Transfer', '', 5085, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-2', 'LIC 814/16 - 786277406 - 11/16 to 11/16 - 2nd Payment', 0, 5085, 10170, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2017-03-08', 'Transfer', '', 5085, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-3', 'LIC 814/16 - 786277406 - 02/17 to 02/17 - 3rd Payment', 0, 5085, 15255, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2017-10-18', 'Transfer', '', 10416.6, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-4&5', 'LIC 814/16 - 786277406 - 05/17 to 08/17 - 4th & 5th Payment', 0, 10416.6, 25671.6, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2018-03-06', 'Transfer', '', 10181.24, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-6&7', 'LIC 814/16 - 786277406 - 11/17 to 02/18 - 6th & 7th Payment', 0, 10181.24, 35852.84, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2018-05-09', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-8', 'LIC 814/16 - 786277406 - 05/18 to 05/18 - 8th Payment', 0, 5011.28, 40864.12, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2018-10-12', 'Transfer', '', 5102.84, '', 'Life Insurance Corporation', 'Bank', 'LIC 814/16-9', 'LIC 814/16 - 786277406 - 08/18 to 08/18 - 9th Payment', 0, 5102.84, 45966.96, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2019-04-10', 'Transfer', '', 10343.02, '', 'Life Insurance Corporation', 'Bank', 'LIC814/16-10&11', 'LIC 814/16 - 786277406 - 11/18 to 02/19 - 10th and 11th Payment', 0, 10343.02, 56309.98, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2019-07-02', 'Transfer', '', 5102.84, '', 'Life Insurance Corporation', 'Bank', 'LIC814/16-12', 'LIC 814/16 - 786277406 - 05/19 to 05/19 - 12 th Payment', 0, 5102.84, 61412.82, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2020-01-02', 'Transfer', '', 10358, '', 'Life Insurance Corporation', 'Bank', 'Two Instalment in 2019', 'Two Instalment in 2019', 0, 10358, 71770.82, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2020-02-02', 'Transfer', '', 5017.41, '', 'Life Insurance Corporation', 'Credit Card', 'Insurance - 4901, Tax - 116.41 = 5017.41-50 (Phonepay Credit)', 'Insurance - 4901, Tax - 116.41 = 5017.41-50 (Phonepay Credit)', 0, 5017.41, 76788.23, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2020-05-09', 'Transfer', '', 5017.4, '', 'Life Insurance Corporation', 'Credit Card', 'LIC POLICY - 12/05/2020 - Bonus 37,800', 'LIC POLICY - 12/05/2020 - Bonus 37,800', 0, 5017.4, 81805.63, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2020-09-05', 'Transfer', '', 5017.4, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5017.4, 86823.03, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2020-12-09', 'Transfer', '', 5017.41, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5017.41, 91840.44, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2021-02-16', 'Transfer', '', 5017.41, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5017.41, 96857.85, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2021-05-08', 'Transfer', '', 5017.41, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5017.41, 101875.26, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2021-08-09', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 106886.54, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2021-11-15', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 111897.82, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2022-02-14', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 116909.1, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2022-06-06', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 121920.38, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2022-08-29', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 126931.66, 1);
INSERT INTO `transaction` (`trans_id`, `accounts_name`, `trans_date`, `type`, `category`, `amount`, `payer`, `payee`, `p_method`, `ref`, `note`, `dr`, `cr`, `bal`, `user_id`) VALUES(NULL, 'Life Insurance 7406', '2022-12-04', 'Transfer', '', 5011.28, '', 'Life Insurance Corporation', 'Bank', 'LIC POLICY 814/16', 'LIC POLICY - LIC 814/16 - 786277406', 0, 5011.28, 131942.94, 1);


UPDATE `repeat_transaction` SET `account`=concat(`account`, '||Life Insurance 7406'),`type`='Transfer',`category`='',`payee`='' WHERE `payee` = 'Life Insurance Corporation';

UPDATE `repeat_transaction` SET `account`=concat(`account`, '||SBI Life Insurance 2504'),`type`='Transfer',`category`='',`payee`='' WHERE `payee` = 'State Bank of India';

UPDATE `repeat_transaction` SET `account`=concat(`account`, '||Star Health Insurance - Amma'),`type`='Transfer',`category`='',`payee`='' WHERE `payee` = 'Star Health Insurance';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'INSURANCE - LIC - 7406' WHERE `accounts_name` = 'Life Insurance 7406';
UPDATE `transaction` SET `accounts_name` = 'INSURANCE - LIC - 7406' WHERE `accounts_name` = 'Life Insurance 7406';

UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Credit Card - 2074||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'HDFC Credit Card - 2074||Life Insurance 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'IOB Bank - 8657||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'IOB Bank - 8657||Life Insurance 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'ICICI Credit Card - 2009||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'ICICI Credit Card - 2009||Life Insurance 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'SBI Bank - 6745||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'SBI Bank - 6745||Life Insurance 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Credit Card - 6577||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'HDFC Credit Card - 6577||Life Insurance 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Bank - 6858||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'HDFC Bank - 6858||Life Insurance 7406';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'INSURANCE - SBI - 2504' WHERE `accounts_name` = 'SBI Life Insurance 2504';
UPDATE `transaction` SET `accounts_name` = 'INSURANCE - SBI - 2504' WHERE `accounts_name` = 'SBI Life Insurance 2504';

UPDATE `repeat_transaction` SET `accounts_name` = 'ICICI Credit Card - 2009||INSURANCE - SBI - 2504' WHERE `accounts_name` = 'ICICI Credit Card - 2009||SBI Life Insurance 2504';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Bank - 6858||INSURANCE - SBI - 2504' WHERE `accounts_name` = 'HDFC Bank - 6858||SBI Life Insurance 2504';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'INSURANCE - Star Health - Amma' WHERE `accounts_name` = 'Star Health Insurance - Amma';
UPDATE `transaction` SET `accounts_name` = 'INSURANCE - Star Health - Amma' WHERE `accounts_name` = 'Star Health Insurance - Amma';

UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Credit Card - 2074||INSURANCE - Star Health - Amma' WHERE `accounts_name` = 'HDFC Credit Card - 2074||Star Health Insurance - Amma';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Credit Card - 6577||INSURANCE - Star Health - Amma' WHERE `accounts_name` = 'HDFC Credit Card - 6577||Star Health Insurance - Amma';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'CREDITCARD - HDFC - 6577' WHERE `accounts_name` = 'HDFC Credit Card - 6577';
UPDATE `transaction` SET `accounts_name` = 'CREDITCARD - HDFC - 6577' WHERE `accounts_name` = 'HDFC Credit Card - 6577';

UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - HDFC - 6577||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'HDFC Credit Card - 6577||INSURANCE - LIC - 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - HDFC - 6577||INSURANCE - Star Health - Amma' WHERE `accounts_name` = 'HDFC Credit Card - 6577||INSURANCE - Star Health - Amma';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Bank - 6858||CREDITCARD - HDFC - 6577' WHERE `accounts_name` = 'HDFC Bank - 6858||HDFC Credit Card - 6577';
UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - HDFC - 6577' WHERE `accounts_name` = 'HDFC Credit Card - 6577';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'CREDITCARD - ICICI - 2009' WHERE `accounts_name` = 'ICICI Credit Card - 2009';
UPDATE `transaction` SET `accounts_name` = 'CREDITCARD - ICICI - 2009' WHERE `accounts_name` = 'ICICI Credit Card - 2009';

UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - ICICI - 2009||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'ICICI Credit Card - 2009||INSURANCE - LIC - 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - ICICI - 2009' WHERE `accounts_name` = 'ICICI Credit Card - 2009';
UPDATE `repeat_transaction` SET `accounts_name` = 'CREDITCARD - ICICI - 2009||INSURANCE - SBI - 2504' WHERE `accounts_name` = 'ICICI Credit Card - 2009||INSURANCE - SBI - 2504';
UPDATE `repeat_transaction` SET `accounts_name` = 'HDFC Bank - 6858||CREDITCARD - ICICI - 2009' WHERE `accounts_name` = 'HDFC Bank - 6858||ICICI Credit Card - 2009';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'BANK - HDFC - 5570' WHERE `accounts_name` = 'HDFC Bank - 5570';
UPDATE `transaction` SET `accounts_name` = 'BANK - HDFC - 5570' WHERE `accounts_name` = 'HDFC Bank - 5570';


-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'BANK - SBI - 6745' WHERE `accounts_name` = 'SBI Bank - 6745';
UPDATE `transaction` SET `accounts_name` = 'BANK - SBI - 6745' WHERE `accounts_name` = 'SBI Bank - 6745';

UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - SBI - 6745||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'SBI Bank - 6745||INSURANCE - LIC - 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - SBI - 6745' WHERE `accounts_name` = 'SBI Bank - 6745';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - SBI - 6745||MF - 01 ICICI Discovery Fund' WHERE `accounts_name` = 'SBI Bank - 6745||MF - 01 ICICI Discovery Fund';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'BANK - HDFC - 6858' WHERE `accounts_name` = 'HDFC Bank - 6858';
UPDATE `transaction` SET `accounts_name` = 'BANK - HDFC - 6858' WHERE `accounts_name` = 'HDFC Bank - 6858';

UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||INSURANCE - LIC - 7406' WHERE `accounts_name` = 'HDFC Bank - 6858||INSURANCE - LIC - 7406';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||INSURANCE - SBI - 2504' WHERE `accounts_name` = 'HDFC Bank - 6858||INSURANCE - SBI - 2504';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||CREDITCARD - HDFC - 6577' WHERE `accounts_name` = 'HDFC Bank - 6858||CREDITCARD - HDFC - 6577';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||MF - 02 HDFC top 100' WHERE `accounts_name` = 'HDFC Bank - 6858||MF - 02 HDFC top 100';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||MF - ICICI FMCG Fund' WHERE `accounts_name` = 'HDFC Bank - 6858||MF - ICICI FMCG Fund';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||CREDITCARD - ICICI - 2009' WHERE `accounts_name` = 'HDFC Bank - 6858||CREDITCARD - ICICI - 2009';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||MF - Motilal Oswal Midcap' WHERE `accounts_name` = 'HDFC Bank - 6858||MF - Motilal Oswal Midcap';
UPDATE `repeat_transaction` SET `accounts_name` = 'BANK - HDFC - 6858||MF - Quant ELSS Direct' WHERE `accounts_name` = 'HDFC Bank - 6858||MF - Quant ELSS Direct';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'CASH' WHERE `accounts_name` = 'Cash on Hand';
UPDATE `transaction` SET `accounts_name` = 'CASH' WHERE `accounts_name` = 'Cash on Hand';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'Cash on Hand', 'CASH') WHERE `accounts_name` LIKE '%Cash on Hand%';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'MF - ICICI Discovery Fund' WHERE `accounts_name` = 'MF - 01 ICICI Discovery Fund';
UPDATE `transaction` SET `accounts_name` = 'MF - ICICI Discovery Fund' WHERE `accounts_name` = 'MF - 01 ICICI Discovery Fund';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'MF - 01 ICICI Discovery Fund', 'MF - ICICI Discovery Fund') WHERE `accounts_name` LIKE '%MF - 01 ICICI Discovery Fund%';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'MF - HDFC top 100' WHERE `accounts_name` = 'MF - 02 HDFC top 100';
UPDATE `transaction` SET `accounts_name` = 'MF - HDFC top 100' WHERE `accounts_name` = 'MF - 02 HDFC top 100';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'MF - 02 HDFC top 100', 'MF - HDFC top 100') WHERE `accounts_name` LIKE '%MF - 02 HDFC top 100%';

-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'WALLET - Amazon Voucher' WHERE `accounts_name` = 'Amazon Voucher';
UPDATE `transaction` SET `accounts_name` = 'WALLET - Amazon Voucher' WHERE `accounts_name` = 'Amazon Voucher';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'Amazon Voucher', 'WALLET - Amazon Voucher') WHERE `accounts_name` LIKE '%Amazon Voucher%';


-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'WALLET - Amazon Pay' WHERE `accounts_name` = 'Amazon Pay';
UPDATE `transaction` SET `accounts_name` = 'WALLET - Amazon Pay' WHERE `accounts_name` = 'Amazon Pay';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'Amazon Pay', 'WALLET - Amazon Pay') WHERE `accounts_name` LIKE '%Amazon Pay%';


-- Acount name change
UPDATE `accounts` SET `accounts_name` = 'WALLET - Paytm' WHERE `accounts_name` = 'Paytm Wallet';
UPDATE `transaction` SET `accounts_name` = 'WALLET - Paytm' WHERE `accounts_name` = 'Paytm Wallet';
UPDATE `repeat_transaction` SET `accounts_name` = REPLACE(`accounts_name`, 'Paytm Wallet', 'WALLET - Paytm') WHERE `accounts_name` LIKE '%Paytm Wallet%';