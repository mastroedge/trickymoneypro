$(document).ready(function () {
    $(".select2sel").select2();

    var $ul = $('#transactions-list')

    $(document).on('click', '.up', function () {
        var $li = $(this).parent().parent();

        if ($li.is(':first-child'))
            $ul.append($li);
        else
            $li.insertBefore($li.prev());
    });

    $(document).on('click', '.down', function () {
        var $li = $(this).parent().parent();

        if ($li.is(':last-child'))
            $ul.prepend($li);
        else
            $li.insertAfter($li.next());
    });

    $(document).on('click', '.close', function () {
        $(this).parent().parent().remove();
        addAmount();
    });

    $(document).on('keyup', '.amount', function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
            (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        addAmount();
    });

    $('#add-transaction').on('submit', function (event) {
        var base_url = $(this).attr('action'); // Get the form action URL
        var data = $(this).serialize();
        swal({
            title: "Are you sure to submit ?",
            text: "You will not be able to Remove this Transaction In Future!",
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, Save it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
            function () {
                $.ajax({
                    method: "POST",
                    url: base_url + "Admin/bulkTransactionAjax/insert",
                    data: data,
                    beforeSend: function () {
                        $(".block-ui").css('display', 'block');
                    },
                    success: function (data) {
                        if (data == "true") {
                            swal("Saved!", "Your Transaction Has Been Sucessfully Saved.", "success");
                            $(".block-ui").css('display', 'none');
                        } else {
                            swal({
                                title: "Failed!",
                                text: data,
                                type: "warning",
                                html: true,
                                confirmButtonColor: "#1abc9c",
                                confirmButtonText: "Ok!",
                                closeOnConfirm: false
                            })
                            $(".block-ui").css('display', 'none');
                        }
                    }
                });
            });
        return false;
    });

    $('#validate-transaction').on('click', function (event) {
        var base_url = $('form#add-transaction').attr('action'); // Get the form action URL
        $.ajax({
            method: "POST",
            url: base_url + "Admin/bulkTransactionAjax/validate",
            data: $('form#add-transaction').serialize(),
            // data : convertFormToJSON1('form#add-transaction'),
            // data : new FormData($('#add-transaction')[0]),
            // data : JSON.stringify(Object.fromEntries($('form#add-transaction'))),
            // data : JSON.stringify($('form#add-transaction').serialize()),
            // data : 'html='+ $('form').serialize(),
            // data : 'html='+ $('form#add-transaction').serializeArray(),
            // data : j,
            // dataType: 'json',
            beforeSend: function () {
                $(".block-ui").css('display', 'block');
            },
            success: function (data) {
                if (data == "true") {
                    swal("Validated!", "Validated Sucessfully.", "success");
                    $(".block-ui").css('display', 'none');
                } else {
                    swal({
                        title: "Failed!",
                        text: data,
                        type: "warning",
                        html: true,
                        confirmButtonColor: "#1abc9c",
                        confirmButtonText: "Ok!",
                        closeOnConfirm: false
                    })
                    $(".block-ui").css('display', 'none');
                }
            }
        });
        return false;
    });

    addAmount();

    // Auto Complete for Transaction
    let debounceTimer;
    const debounceDelay = 2000; // 2 seconds

    $('#search_transaction').on('input', function () {
        clearTimeout(debounceTimer); // Clear the previous timer

        const query = $(this).val().toLowerCase();
        var base_url = $('form#add-transaction').attr('action'); // Get the form action URL
        debounceTimer = setTimeout(function () {
            $.ajax({
                method: "POST",
                url: base_url + "Admin/bulkTransactionAjax/search-transactions",
                data: { query: query },
                beforeSend: function () {
                    $(".block-ui").css('display', 'block');
                },
                success: function (response) {
                    $(".block-ui").css('display', 'none');
                    var transactions = JSON.parse(response);
                    // Handle the transactions data
                    console.log(transactions);

                    if (transactions.length > 0) {
                        // const filteredSuggestions = suggestions.filter(item => item.toLowerCase().includes(query));

                        let dropdownContent = '';
                        if (query && transactions.length > 0) {
                            transactions.forEach(item => {
                                const itemString = JSON.stringify(item).replace(/"/g, "'");
                                dropdownContent += `<li><a class="dropdown-item"
                                data-item="${itemString}"
                                data-type="${item.type}"
                                 data-trans_id="${item.trans_id}">${item.type} - ${item.ref} / ${item.note}</a></li>`;
                            });
                        }

                        $('#suggestions').html(dropdownContent).toggle(!!dropdownContent);
                    }
                },
                error: function (error) {
                    $(".block-ui").css('display', 'block');
                    console.error('Error fetching transactions:', error);
                }
            });
        }, debounceDelay);


    });

    // Hide dropdown on clicking outside
    $(document).on('click', function (e) {
        if (!$(e.target).closest('#search_transaction').length) {
            $('#suggestions').hide();
        }
    });

    // Handle click on suggestion
    $('#suggestions').on('click', '.dropdown-item', function () {
        // let params = new URLSearchParams($(this).data('item'));
        // TODO: Fix this
        const params = $(this).data('item');
        const type = $(this).data('type');
        addMoreBox(type, params, 'search');
        // $('#search_transaction').val(item);
        $('#suggestions').hide();
    });
});

function addAmount() {
    var sum = 0;
    var inc = 0;
    var exp = 0
    var tra = 0
    $(".amount").each(function (index) {
        var type = $(".type").get(index).value;
        if (type == 'Income') {
            inc += +(!isNaN(this.value) ? this.value : 0);
            sum += +(!isNaN(this.value) ? this.value : 0);
        } else if (type == 'Expense') {
            exp += +(!isNaN(this.value) ? this.value : 0);
            sum += -(!isNaN(this.value) ? this.value : 0);
        } else if (type == 'Transfer') {
            tra += +(!isNaN(this.value) ? this.value : 0);
        }

        //var val = parseInt($(this).val(), 10)
        //sum += (!isNaN(val) ? val : 0);
    });
    $("#totalamt").html(formatToIndianRupee(sum));
    $("#totalamt-income").html(formatToIndianRupee(inc));
    $("#totalamt-expense").html(formatToIndianRupee(exp));
    $("#totalamt-transfer").html(formatToIndianRupee(tra));
}

function addMoreBox(action, t, actionfor = 'clone') {
    var base_url = $('form#add-transaction').attr('action'); // Get the form action URL
    swal({
        title: "Enter amount",
        text: "Please provide amount:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: true,
        animation: "slide-from-top",
        inputPlaceholder: "Enter amount here..."
    },
        function (inputValue) {
            if (inputValue === false) return false; // If cancel is clicked

            if (inputValue === "") {
                swal.showInputError("You need to write something!");
                return false;
            }

            let params;
            if (t) {
                // var input = $("<input>").attr("type", "hidden").attr("name", "slno").val($('#transactions-list > fieldset').length);
                // $(t).parent().parent().append($(input));
                if (actionfor == 'clone') {
                    t = $(t).parent().parent().serialize();
                    params = new URLSearchParams(t);
                    params.set("amount[]", inputValue);
                } else if (actionfor == 'search') {
                    // Replace single quotes with double quotes
                    let jsonString = t.replace(/'/g, '"');

                    // Parse the JSON string into an object
                    let jsonObject = JSON.parse(jsonString);

                    const d = {
                        type: [jsonObject.type],
                        accounts_name: [jsonObject.accounts_name],
                        account_to: [],
                        payee_payers: [],
                        income_expense_date: [],
                        income_expense_type: [jsonObject.category],
                        amount: [inputValue],                      
                        p_method: [jsonObject.p_method],
                        reference: [jsonObject.ref],
                        note: [jsonObject.note]
                    };
                    if(jsonObject.type == 'Expense') {
                        d.payee_payers.push(jsonObject.payee);
                    } else if (jsonObject.type == 'Income') {
                        d.payee_payers.push(jsonObject.payer);
                    } else if (jsonObject.type == 'Transfer') {
                        d.account_to.push(jsonObject.account_to);
                    }
                    params = toQueryString(d);
                    //params = new URLSearchParams(d);
                }
            } else {
                params = new URLSearchParams();
                params.set("amount[]", inputValue);
            }
            
            //query data
            $.ajax({
                method: "POST",
                data: params.toString(),
                url: base_url + "Admin/bulkTransactionView/" + action,
                beforeSend: function () {
                    $(".block-ui").css('display', 'block');
                },
                success: function (data) {
                    $(".block-ui").css("display", "none");
                    if (data != "false") {
                        $("#transactions-list").append(data);
                        setTimeout(function () {
                            $('.select2sel').select2();
                        }, 100);
                        addAmount();
                        if ($("#auto-save").is(":checked")) {
                            console.log("auto-save is checked");
                            $('#bulkbtn').click();
                        } else {
                            console.log("auto-save is not checked");
                        }
                    } else {
                        $("#transactions-list").append("Error");
                    }
                }

            });

            // Action to perform with the input
            // swal("Nice!", "You wrote: " + inputValue, "success");
        });
    setTimeout(function () {
        const inputField = document.querySelector('.sweet-alert input');
        if (inputField) {
            inputField.setAttribute('inputmode', 'decimal'); // Suggest a numeric keyboard
            inputField.setAttribute('pattern', '[0-9]*'); // Allow only numbers and decimals
        }
    }, 100);
}

function saveBox(id) {
    var base_url = $('form#add-transaction').attr('action'); // Get the form action URL
    // console.log(new FormData($('#add-transaction')[0]));
    // console.log(new FormData(document.getElementById('add-transaction')));
    // console.log(document.getElementById('add-transaction'));
    var url = base_url + "Admin/bulkTransactionView/Insert";
    if (id) {
        url = base_url + "Admin/bulkTransactionView/Update/" + id;
    }

    $.ajax({
        method: "POST",
        // contentType: "application/json",
        url: url,
        // data : new FormData($('#add-transaction')[0]),
        // data : JSON.stringify(Object.fromEntries($('form#add-transaction'))),
        // data : JSON.stringify($('form#add-transaction').serialize()),
        data: $('form#add-transaction').serialize(),
        // data : 'html='+ $('form').serialize(),
        // data : 'html='+ $('form#add-transaction').serializeArray(),
        beforeSend: function () {
            $(".block-ui").css('display', 'block');
        },
        success: function (data) {
            if (isNumeric(data)) {
                $("#bulkbtn").attr("onclick", "saveBox(" + data + ")");
                $("#bulkbtn").html('<i class="fa fa-archive"></i> Update');
                sucessAlert("Saved Sucessfully");
                $(".block-ui").css('display', 'none');
            } else {
                failedAlert2(data);
                $(".block-ui").css('display', 'none');
            }
        }
    });
    return false;
}

function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

// TODO - Temporary function to convert form data to query string
function toQueryString(data) {
    const params = [];
    for (const key in data) {
        if (Array.isArray(data[key])) {
            data[key].forEach(value => {
                params.push(`${encodeURIComponent(key)}%5B%5D=${encodeURIComponent(value)}`);
            });
        } else {
            params.push(`${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`);
        }
    }
    return params.join("&");
}

// function convertFormToJSON(form) {
// return $(form)
//     .serializeArray()
//     .reduce(function (json, { name, value }) {
//     json[name] = value;
//     return json;
//     }, {});
// }

// function convertFormToJSON1(form) {
//     const array = $(form).serializeArray(); // Encodes the set of form elements as an array of names and values.
//     const json = {};
//     $.each(array, function () {
//         json[this.name] = this.value || "";
//     });
//     return json;
// }