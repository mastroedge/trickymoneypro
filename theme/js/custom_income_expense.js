// SAME FUNCTION IN INCOME AND EXPENSE
function addToTransact(event, trans_id, site_url) {
    event.preventDefault();
    var currentTarget = event.currentTarget;
    var faction = $(currentTarget).hasClass("insert");
    $.ajax({
        method: "POST",
        url: faction ? site_url + "/Admin/manageTransact/insert" : site_url + "/Admin/manageTransact/remove",
        data: { trans_id: trans_id },
        beforeSend: function () {
            // $(".block-ui").css('display','block'); 
        },
        success: function (data) {
            if (data == 'insert') {
                $(currentTarget).removeClass('insert');
                $(currentTarget).addClass('remove');
                currentTarget.innerHTML = '<i class="fa fa-user-times" aria-hidden="true"></i>';
            } else {
                $(currentTarget).removeClass('remove');
                $(currentTarget).addClass('insert');
                currentTarget.innerHTML = '<i class="fa fa-user-plus" aria-hidden="true"></i>';
            }
        }
    });
    return false;
}

function swapTransaction(event, trans_id) {
    event.preventDefault();
    $('#swapTransactionModal input[name="curr_trans_id"]').val(trans_id);
    $('#swapTransactionModal').modal('show');
    return false;
}

$('#swap-transaction').on('submit', function (event) {
    event.preventDefault();
    var link = $(this).attr("action");
    if ($('input[name="curr_trans_id"]').val() != "") {
        //query data
        $.ajax({
            method: "POST",
            url: link,
            data: $(this).serialize(),
            beforeSend: function () {
                $(".preloader").css("display", "block");
            }, success: function (data) {
                $(".preloader").css("display", "none");
                if (data == "1") {
                    swal("Alert", "Successfully transferred !", "info");
                    $('#swapTransactionModal').modal('hide');
                    // $("#Table-div").html(data);
                    // $(".report-heading p").html("Date From "+$("#from-date").val()+" To "+$("#to-date").val());
                } else {
                    // $("#Table-div").html("");
                    // $(".report-heading p").html("Date From "+$("#from-date").val()+" To "+$("#to-date").val());    
                    // swal("Alert","Sorry, No Data Found !", "info");
                    swal("Alert", "Failed to transfer !", "error");
                }
            }
        });
    }
});

function syncTransaction(event, trans_id, site_url) {
    event.preventDefault();
    swal({
        title: "Are you sure to submit ?",
        text: "You will not be able to Remove this Transaction In Future!",
        type: "info",
        html: true,
        showCancelButton: true,
        confirmButtonColor: "#1abc9c",
        confirmButtonText: "Yes, Save it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    },
        function () {
            $.ajax({
                method: "POST",
                url: site_url + "/Admin/syncAccount/sync",
                data: { trans_id: trans_id },
                beforeSend: function () {
                    $(".preloader").css("display", "block");
                },
                success: function (data) {
                    $(".preloader").css("display", "none");
                    if (data == "1") {
                        swal("Alert", "Successfully Sync !", "info");
                    } else {
                        swal("Alert", "Failed to Sync !", "error");
                    }
                }
            });
        });
    return false;
}

$
jQuery(document).ready(function () {
    $(document).on('click', '.check-in', function () {
        if ($(this).is(":checked")) {
            $(this).closest('tr').addClass('highlight-in');
        } else {
            $(this).closest('tr').removeClass('highlight-in');
        }

        let total = 0;
        $('input.check-in:checked').each(function () {
            total += parseFloat(this.value) || 0;
        });

        $('#total-in').html(formatToIndianRupee(total.toFixed(2)));
    });

    $(document).on('click', '.check-ex', function () {
        // Highlight row based on checkbox state
        if ($(this).is(":checked")) {
            $(this).closest('tr').addClass('highlight');
        } else {
            $(this).closest('tr').removeClass('highlight');
        }

        // Calculate total of checked items
        let total = 0;
        $('input.check-ex:checked').each(function () {
            total += parseFloat(this.value) || 0; // Ensure valid numeric value
        });

        // Round to 2 decimal places and update the total display
        $('#total-ex').html(formatToIndianRupee(total.toFixed(2)));
    });

});