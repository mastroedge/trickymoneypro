function deleteRepeatTransaction(event, trans_id, site_url) {
    event.preventDefault();
    var currentTarget = event.currentTarget;
    swal({
        title: "Are you sure Want To Delete?",
        text: "You will not be able to recover this Data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            url: site_url + 'remove/' + trans_id,
            beforeSend: function () {
                $(".block-ui").css('display', 'block');
            },
            success: function (data) {
                $(".block-ui").css('display', 'none');
                if (data == "true") {
                    currentTarget.closest("tr").remove();
                    swal("Deleted!", "Sucessfully", "success");
                } else {
                    swal("Failed!", "Try again", "error");
                }
            }
        });
    });
    return false;
}

function processRepeatTransaction(event, trans_id, site_url, $action) {
    event.preventDefault();
    var currentTarget = event.currentTarget;
    swal({
        title: "Are you sure Want To Perform?",
        text: "You will not be able to recover this Data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, do it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            url: site_url + 'action/' + trans_id + '/' + $action,
            beforeSend: function () {
                $(".block-ui").css('display', 'block');
            },
            success: function (data) {
                $(".block-ui").css('display', 'none');
                if (data == "true") {
                    currentTarget.closest("tr").remove();
                    swal("Transaction Done!", "Sucessfully", "success");
                } else {
                    swal("Failed!", "Try again", "error");
                }
            }
        });
    });
    return false;
}

// $(document).on('click', '.manage-btn', function() {

//     var link = $(this).attr("href");
//     $.ajax({
//         method: "POST",
//         url: link,
//         beforeSend: function() {
//             $(".block-ui").css('display', 'block');
//         },
//         success: function(data) {
//             //var link = location.pathname.replace(/^.*[\\\/]/, ''); //get filename only  
//             history.pushState(null, null, link);
//             $('.asyn-div').load(link + '/asyn', function() {
//                 $(".block-ui").css('display', 'none');
//             });

//         }
//     });

//     return false;
// });