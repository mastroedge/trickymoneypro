$(document).ready(function () {

    $(".datewithtime").flatpickr({
        enableTime: true,
        dateFormat: 'Y-m-d h:i K',
        disableMobile: true,  // Force Flatpickr on mobile devices
        time_24hr: false,
        formatDate: function (date, format) {
            const padZero = (number) => String(number).padStart(2, '0');
            const year = date.getFullYear();
            const month = padZero(date.getMonth() + 1); // Months are zero-based
            const day = padZero(date.getDate());
            let hours = date.getHours() % 12 || 12; // Convert to 12-hour format
            const minutes = padZero(date.getMinutes());
            const ampm = date.getHours() >= 12 ? 'PM' : 'AM';
            // Adding 07 instead of 7 in time hour
            return `${year}-${month}-${day} ${padZero(hours)}:${minutes} ${ampm}`;
        }
    });

    $(".dateonly").flatpickr({
        dateFormat: "Y-m-d",
        disableMobile: true,  // Force Flatpickr on mobile devices
    });

    $(document).on('focus', ".datewithtimebulk", function () {
        $(this).flatpickr({
            enableTime: true,
            dateFormat: 'Y-m-d h:i K', // Use H:i to ensure consistent leading zeros
            disableMobile: true, // Force Flatpickr on mobile devices
            time_24hr: false,
            formatDate: function (date, format) {
                const padZero = (number) => String(number).padStart(2, '0');
                const year = date.getFullYear();
                const month = padZero(date.getMonth() + 1); // Months are zero-based
                const day = padZero(date.getDate());
                let hours = date.getHours() % 12 || 12; // Convert to 12-hour format
                const minutes = padZero(date.getMinutes());
                const ampm = date.getHours() >= 12 ? 'PM' : 'AM';
                // Adding 07 instead of 7 in time hour
                return `${year}-${month}-${day} ${padZero(hours)}:${minutes} ${ampm}`;
            }
        });
    });

    // $('.amount').on('input', function () {
    //     // Get the current value of the input field
    //     let inputValue = $(this).val();

    //     // Remove all commas and alphabets from the input value
    //     inputValue = inputValue.replace(/[,a-zA-Z]/g, '');

    //     // Update the input field with the value without commas
    //     $(this).val(inputValue);
    // });

    $(".calculator").attr('unselectable', 'on')
        .css({
            '-moz-user-select': '-moz-none',
            '-moz-user-select': 'none',
            '-o-user-select': 'none',
            '-khtml-user-select': 'none', /* you could also put this in a class */
            '-webkit-user-select': 'none',/* and add the CSS class here instead */
            '-ms-user-select': 'none',
            'user-select': 'none'
        }).bind('selectstart', function () { return false; });

    //$("#user-type").selectmenu();

    //For Profile Dropdown Desktop And Responsive View
    $("#profile").click(function () {

        $(".dropdown-profile").fadeToggle(500);
        $('.dropdown-alert').hide();

        return false;
    });

    $(document).click(function (event) {
        if (!$(event.target).hasClass('dropdown-profile')) {
            $(".dropdown-profile").fadeOut(500);
        }
    });

    // For Alert Dropdown
    $(".my-dropdown").on('click', function () {
        if ($(".my-dropdown").css('display') == 'block' && $(this).next("ul").css('display') != 'block') {
            $('.dropdown-alert').hide();
        }
        $(this).next("ul").fadeToggle(1000);
        if ($('.dropdown-profile').css('display') == 'block') {
            $('.dropdown-profile').hide();
        }
        return false;
    });

    $(document).on('click', '.exit', function () {
        $(".alertbox-info").fadeTo(500, 0.2, function () {
            $(".alertbox-info").remove();
        });
    });

    $(document).on('click', '.action-enabledisable-btn', function () {
        var main = $(this);
        swal({
            title: "Are you sure Want To Enable/Disable?",
            text: "You will not be able to see this Data here!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, enable/disable it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function () {
            var link = $(main).attr("href");
            $.ajax({
                url: link,
                beforeSend: function () {
                    $(".block-ui").css('display', 'block');
                },
                success: function (data) {
                    $(".block-ui").css('display', 'none');
                    swal({
                        title: "Enabled / Disabled",
                        text: "Enabled / Disabled Successfully!",
                        type: "success",
                        confirmButtonColor: "#1abc9c",
                        confirmButtonText: "ok!",
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true
                    }, function () {
                        main.closest("tr").remove();
                        // location.reload(); // Reloads the current page
                    });
                }
            });
        });
        return false;
    });

});


function showIncome(content, date, url, event) {
    if (url == 1) {
        swal({
            title: "Already Received",
            text: content + " On- " + date,
            type: "success",
            html: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Ok!",
            closeOnConfirm: false
        })
    } else {
        swal({
            title: "Are you sure want to reveive ?",
            // text: content + "You will not be able to Remove this Transaction In Future!",
            text: content,
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, Receive it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
            function () {
                //Ajax Functionality
                $.ajax({
                    method: "GET",
                    url: url,
                    beforeSend: function () {
                        $(".block-ui").css('display', 'block');
                    }, success: function (data) {
                        $(".block-ui").css('display', 'none');
                        if (data == 'true') {
                            if (typeof event != 'undefined') {
                                event.url = 1;
                            }
                            swal("Received!", "Your Income has been Received.", "success");
                        }
                    }
                });

            });
    }
}

function showExpense(content, date, url, event) {
    if (url == 1) {
        swal({
            title: "Already Paid",
            text: content + " On- " + date,
            type: "success",
            html: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Ok!",
            closeOnConfirm: false
        })
    } else {
        swal({
            title: "Are you sure want to paid ?",
            // text: content + "You will not be able to Remove this Transaction In Future!",
            text: content,
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonColor: "#1abc9c",
            confirmButtonText: "Yes, Pay it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
            function () {
                //Ajax Functionality
                $.ajax({
                    method: "GET",
                    url: url,
                    beforeSend: function () {
                        $(".block-ui").css('display', 'block');
                    }, success: function (data) {
                        $(".block-ui").css('display', 'none');
                        if (data == 'true') {
                            event.url = 1;
                            swal("Paid!", "Your Transaction Has Been Sucessfully Completed.", "success");
                        }
                    }
                });
            });
    }
}


// function loadpage() {
//     $(document).on('click', '.asyn-menu li a,.slicknav_menu a', function () {
//         var link = $(this).attr("href");
//         if (link != '#' && link != '') {
//             $(".block-ui").css('display', 'block');
//             history.pushState(null, null, link);
//             link = link + "/asyn";
//             $('.asyn-div').empty();
//             $('.asyn-div').load(link, function () {
//                 $(".block-ui").css('display', 'none');


//                 //Ajdust content
//                 if ($(".sidebar").width() == "0") {
//                     $(".main-content").css("padding-left", "0px");
//                 }
//             });
//             $(".asyn-menu li a").removeClass('active-menu');
//             $(".slicknav_menu a").removeClass('active-menu');
//             $(this).addClass('active-menu');

//         }

//         return false;
//     });

// }

// function loadpage2() {
//     $(document).on('click', '.asyn-link', function () {
//         var link = $(this).attr("href");
//         if (link != '#' && link != '') {
//             $(".block-ui").css('display', 'block');
//             history.pushState(null, null, link);
//             link = link + "/asyn";
//             $('.asyn-div').empty();
//             $('.asyn-div').load(link, function () {
//                 $(".block-ui").css('display', 'none');
//                 //Ajdust content
//                 if ($(".sidebar").width() == "0") {
//                     $(".main-content").css("padding-left", "0px");
//                 }
//             });
//             $(".asyn-menu li a").removeClass('active-menu');
//             $(".slicknav_menu a").removeClass('active-menu');
//             $(this).addClass('active-menu');
//         }
//         return false;
//     });

// }

// $(window).bind('popstate', function () {
//     link = location.pathname.replace(/^.*[\\\/]/, ''); //get filename only
//     if (link != '') {
//         $('.asyn-div').empty();
//         $(".block-ui").css('display', 'block');
//         $('.asyn-div').load(link + "/asyn", function () {
//             $(".block-ui").css('display', 'none');
//         });
//     }
// });

function sucessAlert(message) {
    if (!$(".ajax-notify").length) {
        $(".system-alert-box").append("<div class='alert alert-success ajax-notify'></div>");
    }
    $('.ajax-notify').css("display", "block");
    $('.ajax-notify').addClass("alert-success");
    $('.ajax-notify').removeClass("alert-danger");
    $('.ajax-notify').html('<i class="fa fa-check"></i> ' + message + '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>');
}

function failedAlert(message) {
    if (!$(".ajax-notify").length) {
        $(".system-alert-box").append("<div class='alert alert-success ajax-notify'></div>");
    }
    $('.ajax-notify').css("display", "block");
    $('.ajax-notify').removeClass("alert-success");
    $('.ajax-notify').addClass("alert-danger");
    $('.ajax-notify').html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-times"></i> ' + message);
}

function failedAlert2(message) {
    if (!$(".ajax-notify").length) {
        $(".system-alert-box").append("<div class='alert alert-success ajax-notify'></div>");
    }
    $('.ajax-notify').css("display", "block");
    $('.ajax-notify').removeClass("alert-success");
    $('.ajax-notify').addClass("alert-danger");
    $('.ajax-notify').html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> ' + message);
}



var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};


$(document).on('click', '.pdf-btn', function () {
    var pdf = new jsPDF('l', 'pt', 'a4');
    pdf.setFont("times");
    pdf.text(40, 60, $(".report-heading h4").html());
    pdf.setFontSize(12);
    pdf.text(40, 75, $(".report-heading p").html());
    source = $('#Table-div')[0]; //table Id
    specialElementHandlers = {
        '#bypassme': function (element, renderer) {
            return true
        }
    };
    margins = { //table margins and width
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    pdf.fromHTML(
        source,
        margins.left,
        margins.top, {
        'width': margins.width,
        'elementHandlers': specialElementHandlers
    },

        function (dispose) {
            pdf.save('Report.pdf'); //Filename
        }, margins);

});


$(document).on('click', '.print-btn', function () {
    Print($("#Report-Table").html());
});

// Function to format numbers in Indian Rupee style
function formatToIndianRupee(amount) {
    let formatted = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 2, // Skip decimals if possible
        maximumFractionDigits: 2, // Include up to 2 decimals if needed
    }).format(amount);

    // Split the formatted currency into main part and decimal part
    let parts = formatted.split('.');

    if (parts.length === 2) {
        return `<span class="rupee text-success">${parts[0]}.<small>${parts[1]}</small></span>`;
    } else {
        return `<span class="rupee text-success">${formatted}.<small>00</small></span>`;
    }

    // Remove ".00" if it exists
    // if (formatted.endsWith('.00')) {
    //     formatted = formatted.slice(0, -3);
    // }
    // return formatted;
}

// Function to format numbers in Indian Rupee style
function formatToIndianNumber(amount) {
    let num = amount.toFixed(2); // Ensure 2 decimal places
    let parts = num.split(".");
    let integerPart = parts[0];
    let decimalPart = parts[1];

    // Format integer part in Indian numbering style
    let lastThree = integerPart.slice(-3);
    let otherNumbers = integerPart.slice(0, -3);
    if (otherNumbers !== '') {
        lastThree = ',' + lastThree;
    }
    let formattedNumber = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    // Append decimals only if not ".00"
    // return decimalPart === "00" ? formattedNumber : formattedNumber + "." + decimalPart;
    return formattedNumber + "." + decimalPart;
}

function Print(data) {
    var w = (screen.width);
    var h = (screen.height);
    var mywindow = window.open('', 'Print-Report', 'width=' + w + ',height=' + h);
    mywindow.document.write('<html><head><title>Print-Report</title>');
    mywindow.document.write('<link href="<?php echo base_url() ?>/theme/css/bootstrap.min.css" media="print" rel="stylesheet">');
    mywindow.document.write('<link href="<?php echo base_url() ?>/theme/css/my-style.css" media="print" rel="stylesheet">');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}

function showCommonModal(id, url) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "json", // Expect JSON response
        beforeSend: function () {
            $(".block-ui").css('display', 'block');
        },
        success: function (response) {
            if (response.success) {
                $("#" + id + "Title").text(response.title);
                $("#" + id + " .modal-body").html(response.content);
            } else {
                $("#" + id + " .modal-body").html("<p>Error: " + response.error + "</p>");
            }
            var myModal = new bootstrap.Modal(document.getElementById(id), {
                backdrop: 'static',
                keyboard: false
            });
            $(".block-ui").css("display", "none");
            myModal.show();
        },
        error: function () {
            $("#" + id + " .modal-body").html("<p>Failed to load content.</p>");
            var myModal = new bootstrap.Modal(document.getElementById(id), {
                backdrop: 'static',
                keyboard: false
            });
            $(".block-ui").css("display", "none");
            myModal.show();
        }
    });
}

function addDataToDB(id, url) {
    var elm = $('#' + id);
    var valElm = $('#ajax-message');
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: elm.serialize(),
        beforeSend: function () {
            $(".block-ui").css('display', 'block');
        },
        success: function (response) {
            if (response.status === 'success') {
                valElm.html(response.message);
            } else {
                valElm.html(response.message);
            }
            $(".block-ui").css("display", "none");
            valElm.fadeIn();
            // Hide success message after 5 seconds
            setTimeout(function () {
                valElm.fadeOut();
                valElm.html('');
                if (response.status === 'success') {
                    location.reload();
                }
            }, 3000);
        },
        error: function (xhr, status, error) {
            valElm.html(error);
        }
    });
}