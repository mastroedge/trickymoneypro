$(document).ready(function () {
    $('form').on('submit', function () {
        var actionUrl = $(this).attr('action'); // Get the action URL
        $.ajax({
            method: "POST",
            url: actionUrl,
            data: $(this).serialize(),
            beforeSend: function () {
                $(".block-ui").css('display', 'block');
            },
            success: function (data) {
                var json = JSON.parse(data);
                if (json['result'] == "true") {

                    $(".block-ui").css('display', 'none');
                    swal({
                        title: "Inserted / Updated",
                        text: "Inserted / Updated Successfully",
                        type: "success",
                        confirmButtonColor: "#1abc9c",
                        confirmButtonText: "ok!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function () {
                        location.reload(); // Reloads the current page
                    });

                } else {
                    failedAlert(json['message']);
                    $(".block-ui").css('display', 'none');
                }
            }
        });
        return false;
    });

    $('#search').on('input', function () {
        const query = $(this).val().toLowerCase();
        let hasVisibleRows = false;

        $('#table-body tr').each(function () {
            const rowText = $(this).text().toLowerCase();
            if (rowText.includes(query)) {
                $(this).show(); // Show row if it matches the query
                hasVisibleRows = true;
            } else {
                $(this).hide(); // Hide row if it doesn't match
            }
        });

        // Show or hide "no results" message
        // if (!hasVisibleRows) {
        //     $('#no-results').removeClass('d-none');
        // } else {
        //     $('#no-results').addClass('d-none');
        // }
    });
});
